//
//  AppFonts.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/5/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
struct AppFont {
    static let PTSansbold = "PTSans-bold"
    static let PTSans_Regular = "PTSans-Regular"
    static let Oswald_SemiBold = "Oswald-SemiBold"
    static let Oswald_Regular = "Oswald-Regular"
    static let Oswald_Medium = "Oswald-Medium"
}
