//
//  BaseTabBarViewController.swift
//  Boxes
//
//  Created by MacAir on 03/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import ESTabBarController_swift

class BaseTabBarViewController: ESTabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tabBar.backgroundColor = UIColor.white
        tabBar.tintColor = UIColor.init(rgb: THEME_COLOR)
        tabBar.barTintColor = UIColor.white
        tabBar.shadowImage = UIImage()
        tabBar.backgroundImage = UIImage()
        tabBar.borderWidth = 0
        tabBar.layer.borderWidth = 0
//        self.tabBar.layer.borderColor = UIColor.clear.cgColor
//        self.tabBar.shadowImage = UIImage(named: "transparent")
        self.shouldHijackHandler = { tabbarController, viewController, index in
            return false
        }
        
        
        let v1 = HomeTopBarViewController.instantiate(fromAppStoryboard: .Main)
//        let v2 = FoodTopBarViewController.instantiate(fromAppStoryboard: .Main)
        let v3 = UserProfileViewController.instantiate(fromAppStoryboard: .Main) 
        
//        let food = ESTabBarItem.init(ExampleIrregularityContentView(), title: nil, image: UIImage(named: "tab_menu"), selectedImage: UIImage(named: "tab_menu"))
//        food.contentView?.renderingMode = .alwaysOriginal
//        food.contentView?.tintColor = AppColor.primary
        
        let home = ESTabBarItem.init(ExampleIrregularityBasicContentView(), title: nil, image: UIImage(named: "icons8-a_home-1"), selectedImage: UIImage(named: "icons8-a_home"))
        home.contentView?.renderingMode = .alwaysTemplate
        home.contentView?.tintColor = AppColor.primary
        
        let profile = ESTabBarItem.init(ExampleIrregularityBasicContentView(), title: nil, image: UIImage(named: "icons8-person_male-1"), selectedImage: UIImage(named: "icons8-person_male"))
        profile.contentView?.renderingMode = .alwaysTemplate
        profile.contentView?.tintColor = AppColor.primary
        
        v1.tabBarItem = home
//        v2.tabBarItem = food
        v3.tabBarItem = profile
        
        
        //
        self.viewControllers = [
            v1,
//            v2,
            v3
        ]
        
        self.selectedIndex = SplashViewController.VCIndex
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
