//
//  BaseViewController.swift
//  Boxes
//
//  Created by MacAir on 03/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SwiftyJSON
import Material
import Motion

class BaseViewController: UIViewController {
    
    var loader : NVActivityIndicatorView?
    var loaderBGView : UIView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.isMotionEnabled = true
//        self.navigationController?.isMotionEnabled = true
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
//        CartManager.shared.itemUpdateAction = {
//            print("CartManager.shared.cartItemCount: \(CartManager.shared.cartItemCount)")
//            self.addBadgeRightBarButtonWithCartImage(itemvalue: "\(CartManager.shared.cartItemCount)")
//        }
        
    }
    
    
    //    func showVC(_ vc: UIViewController, completion: (() -> Void)? = nil){
    //        DispatchQueue.main.async {
    //            if let nav = self.navigationController {
    //                nav.pushViewController(vc, animated: true)
    //            }else{
    //                self.present(vc, animated: true, completion: completion)
    //            }
    //        }
    //    }
    
    //    @objc func back() {
    //        DispatchQueue.main.async{
    ////            Peep.play(sound: HapticFeedback.selection)
    //            //            IQKeyboardManager.shared.resignFirstResponder()
    //            if let nav = self.navigationController {
    //                if nav.viewControllers.count == 1{
    //                    nav.dismiss(animated: true, completion: nil)
    //                }else{
    //                    nav.popViewController(animated: true)
    //                }
    //            }else{
    //                self.dismiss(animated: true, completion: nil)
    //            }
    //        }
    //    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Peep.play(sound: HapticFeedback.selection)
        Peep.play(sound: KeyPress.tap)
    }
    
    func showLoader() {
        loader = nil
        loaderBGView = nil
        loaderBGView = UIView(frame: self.view.frame)
        loaderBGView!.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.75)
        loader = NVActivityIndicatorView(frame: CGRect(origin: CGPoint(x: self.view.frame.size.width  / 2 - self.view.frame.size.width / 6,
                                                                       y: self.view.frame.size.height / 2 - self.view.frame.size.height / 6), size: CGSize(width: self.view.frame.size.width / 3, height: self.view.frame.size.height / 3)), type: NVActivityIndicatorType.orbit, color: UIColor(rgb: THEME_COLOR), padding: 0)
        loaderBGView!.addSubview(loader!)
        self.view.addSubview(loaderBGView!)
        
        UIApplication.shared.keyWindow!.addSubview(loaderBGView!)
        UIApplication.shared.keyWindow!.bringSubviewToFront(loaderBGView!)
        loader?.startAnimating()
    }
    
    func showLoader(timeToDismiss: Double) {
        self.showLoader()
        Timer.scheduledTimer(withTimeInterval: timeToDismiss, repeats: false) { (_) in
            self.hideLoader()
        }
    }
    
    func hideLoader() {
        loader?.stopAnimating()
        loaderBGView?.removeFromSuperview()
    }
    
    func share(text: String, image: UIImage? = nil, url: URL? = nil) {
        var items = [
            text,
            ] as [Any]
        if let img = image {
            items.append(img)
        }
        if let URL = url {
            items.append(URL)
        }
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    //MARK:-  Remove Right Buttons
    
    func removeRightButtons() {
        if self.tabBarController == nil {
            self.navigationItem.rightBarButtonItems = nil
        } else {
            self.tabBarController?.navigationItem.rightBarButtonItems = nil
        }
    }
    
    //MARK:-  Back Button With Right Button Code
    
    func placeRightButtons(buttons: [UIBarButtonItem]) {
        
        if self.tabBarController == nil {
            self.navigationItem.hidesBackButton = true
            self.navigationItem.rightBarButtonItems = nil
        } else {
            self.tabBarController?.navigationItem.hidesBackButton = true
            self.tabBarController?.navigationItem.rightBarButtonItems = nil
        }
        
        if self.tabBarController == nil {
            self.navigationItem.rightBarButtonItems = buttons
        } else {
            self.tabBarController?.navigationItem.rightBarButtonItems = buttons
        }
    }
    
    //MARK:-  Back Button With Right Button Code
    public func addBadgeRightBarButtonWithCartImage(itemvalue: String) {
        let bagButton = BadgeButton()
        bagButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        bagButton.tintColor = UIColor.darkGray
        bagButton.setImage(#imageLiteral(resourceName: "cart"), for: .normal)
        bagButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
        bagButton.badge = itemvalue
        bagButton.addTarget(self, action: #selector(self.openCart), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: bagButton)
        self.navigationItem.rightBarButtonItem = barButton
        if let nav = self.tabBarController?.navigationItem {
            nav.rightBarButtonItem = barButton
        }else{
            self.navigationItem.rightBarButtonItem = barButton
        }
    }
    
    public func addRightBarButtonWithCartImage(_ buttonImage: UIImage = #imageLiteral(resourceName: "cart")) {
        let cartButton: UIBarButtonItem = UIBarButtonItem(image: buttonImage, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openCart))
        if let nav = self.tabBarController?.navigationItem {
            nav.rightBarButtonItem = cartButton
        }else{
            self.navigationItem.rightBarButtonItem = cartButton
        }
//        self.navigationItem.rightBarButtonItem = cartButton
    }
    
    func placeLeftButtons(buttons: [UIBarButtonItem]) {
        
        if self.tabBarController == nil {
            self.navigationItem.hidesBackButton = true
            self.navigationItem.leftBarButtonItems = nil
        } else {
            self.tabBarController?.navigationItem.hidesBackButton = true
            self.tabBarController?.navigationItem.leftBarButtonItems = nil
        }
        
        if self.tabBarController == nil {
            self.navigationItem.leftBarButtonItems = buttons
        } else {
            self.tabBarController?.navigationItem.leftBarButtonItems = buttons
        }
    }
    
    //MARK:-  Back Button Code
    
    func placeBackButton(selectorBack: Selector) {
        
        var btnBack: UIButton?
        
        if self.tabBarController == nil {
            self.navigationItem.hidesBackButton = true
            self.navigationItem.leftBarButtonItems = nil
        } else {
            self.tabBarController?.navigationItem.hidesBackButton = true
            self.tabBarController?.navigationItem.leftBarButtonItems = nil
        }
        
        btnBack = UIButton(type: .custom)
        btnBack?.setImage(UIImage(named: "back_arrow")!, for: .normal)
        btnBack?.addTarget(self, action: selectorBack, for: .touchUpInside)
        btnBack?.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        let backBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnBack!)
        
        if self.tabBarController == nil {
            self.navigationItem.leftBarButtonItems = [ backBarButton ]
        } else {
            self.tabBarController?.navigationItem.leftBarButtonItems = [ backBarButton ]
        }
    }
    
    func goToSettings(title: String, message: String) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel".localized, comment: ""), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: NSLocalizedString("Open Settings".localized, comment: ""), style: .default) { (action) in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}


extension UIViewController {
    
    @objc public func openCart() {
        guard UserManager.isUserLogin() else {
            Messages.showErrorMessage(title: "User not logged in".localized, message: "Guest user cannot access cart".localized)
            return
        }
        guard !CartManager.shared.isCartEmpty() else {
            Messages.showErrorMessage(title: "Empty Cart".localized, message: "Cannot open cart because it is empty".localized)
            return
        }
        let vc = PlaceOrderViewController.instantiate(fromAppStoryboard: .Main)
        self.showVC(vc)
    }
    
    func showVC(_ vc: UIViewController, completion: (() -> Void)? = nil){
        DispatchQueue.main.async {
            if let nav = self.navigationController {
                nav.pushViewController(vc, animated: true)
            }else{
                self.present(vc, animated: true, completion: completion)
            }
        }
    }
    
    @objc func back() {
        DispatchQueue.main.async{
            //            Peep.play(sound: HapticFeedback.selection)
            //            IQKeyboardManager.shared.resignFirstResponder()
            if let nav = self.navigationController {
                if nav.viewControllers.count == 1{
                    nav.dismiss(animated: true, completion: nil)
                }else{
                    nav.popViewController(animated: true)
                }
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
}
