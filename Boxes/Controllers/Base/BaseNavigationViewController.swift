//
//  BaseNavigationViewController.swift
//  Boxes
//
//  Created by MacAir on 03/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//


import UIKit
//import Hero

class BaseNavigationViewController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //        self.hero.navigationAnimationType = .fade
        //        self.hideNavigationBar()
        self.setNavigationBarColor(color: AppColor.primary)
        self.navigationBar.titleTextAttributes = [
//            NSAttributedString.Key.font: UIColor.white,
            NSAttributedString.Key.font : UIFont(name: AppFont.Oswald_Medium, size: 15) ?? UIFont.boldSystemFont(ofSize: 14),
            NSAttributedString.Key.foregroundColor: UIColor.white,
            
            ]
    }
    
    
    
}
