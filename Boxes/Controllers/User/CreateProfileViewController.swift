//
//  CreateProfileViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/4/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import CoreLocation //location work
import GooglePlaces
import SwiftyJSON

class CreateProfileViewController: BaseViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var uploadImageButton: UIButton!
    @IBOutlet weak var firstNameView: UIView!
    @IBOutlet weak var lastNameView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var createProfileButton: UIButton!
    @IBOutlet weak var firstNameTextfield: UITextField!
    @IBOutlet weak var lastNameTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!    
    
    var imagePicker = UIImagePickerController() //upload image step no 1
    var personalimage : UIImage?
    var isFromProfile = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setValues()
        firstNameTextfield.delegate = self
        lastNameTextfield.delegate = self
        emailTextfield.delegate = self
    }
    
    func setValues(){
        if let user = UserManager.getUserObjectFromUserDefaults() {
            self.firstNameTextfield.text = user.firstName
            self.lastNameTextfield.text = user.lastName
            self.emailTextfield.text = user.email
            guard let img = user.image, !img.isEmpty else {
                return
            }
            self.profileImageView.imageUrl = BASEIMAGEURL + user.image
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    
    func configureNavigationBar(){
        
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.transparentNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
        if isFromProfile {
            // self.placeBackButton(selectorBack: #selector(self.back))
            self.title = "update profile".localized.uppercased()
            self.createProfileButton.setTitle("update profile".localized.uppercased(), for: .normal)
        }else {
            self.placeRightButtons(buttons: configure())
            self.title = "create profile".localized.uppercased()
            self.createProfileButton.setTitle("create profile".localized.uppercased(), for: .normal)
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.resetNavigationBar()
    }
    
    func configure() -> [UIBarButtonItem] {
        
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        let b = UIBarButtonItem(title: "SKIP".localized, style: .plain, target: self, action: #selector(self.home))
        b.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        return [b]
    }
    
    @objc func home(){
        let vc = BaseLeftMenuViewController.instantiate(fromAppStoryboard: .LeftMenu)
        vc.leftViewPresentationStyle = .slideBelow
        UIWINDOW?.rootViewController = vc
    }
    
    
    @IBAction func actionCreateProfile(_ sender: UIButton) {
        
        guard isValidate() else{
            sender.shake()
            return
        }
    }
    
    func isValidate() -> Bool {
        
        guard let firstName = self.firstNameTextfield.text, !firstName.isEmpty else{
            Messages.showErrorMessage(message: "Write your first name".localized)
            return false
        }
        
        guard let lastName = self.lastNameTextfield.text, !lastName.isEmpty else{
            Messages.showErrorMessage(message: "Write your last name".localized)
            return false
        }
        
        guard let email = self.emailTextfield.text, !email.isEmpty else{
            Messages.showErrorMessage(message: "Write your email".localized)
            return false
        }
        
        guard email.isValid(regex: .email) else {
            Messages.showErrorMessage(message: "Write your email in correct form".localized)
            return false
        }
        
        serviceCall(image: self.personalimage, firstName: firstName, lastName: lastName, email: email)
        return true
        
    }
    
    func serviceCall(image: UIImage?, firstName: String, lastName: String, email: String) {
        LoadingOverlay.shared.showOverlay()
        var parameter : [String:Any] = [
            "first_name": firstName,
            "last_name": lastName,
            "email": email
        ]
        if let img = image {
            parameter["image"] = img
        }
        print(parameter)
        
        NetworkRequest.shared.storeProfile(parameters: parameter) { (response, error) in
            LoadingOverlay.shared.hideOverlayView()
            if error != nil {
                //                Messages.showErrorMessage(message: error?.message)
                Messages.showErrorMessage(title: error?.message, message: error?.userInfo["email"])
                return
            }
            let data = UserModel.init(fromJson: JSON(response))
            data.isProfileFilled = 1
            UserManager.saveUserObjectToUserDefaults(userResult: data)
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func actionSkip(_ sender: UIButton) {
        
        let vc = BaseLeftMenuViewController.instantiate(fromAppStoryboard: .LeftMenu)
        vc.leftViewPresentationStyle = .slideBelow
        UIWINDOW?.rootViewController = vc
        
    }
    
    
    @IBAction func actionUploadProfileImage(_ sender: UIButton) {
        
        print("Image func called")
        //upload image step no 2
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    
}

extension CreateProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //upload image step no 3
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage //UIImagePickerController.InfoKey
        self.profileImageView.image = chosenImage
        self.personalimage = chosenImage
        //        self.imageSave = chosenImage
        dismiss(animated: true, completion: nil)
        
    }
    
}

extension CreateProfileViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == firstNameTextfield {
            
            self.lastNameTextfield.becomeFirstResponder()
            
        }else if textField == lastNameTextfield {
            
            self.emailTextfield.becomeFirstResponder()
            
        }else {
            
            self.view.endEditing(true)
        }
        
        return true
    }
}
