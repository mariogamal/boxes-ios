//
//  ChangePasswordViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/23/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import CountryPickerView


class ChangeNumberViewController: BaseViewController {
    
    @IBOutlet weak var currentNumberTextBox: BoxesTextBox!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigationBar()
        self.configureUI()
    }
    
    func configureNavigationBar(){
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "Change number".localized.uppercased()
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
    }
    
    @IBAction func actionSubmit(_ sender: UIButton) {
        guard isValidate() else {
            return
        }
        
    }
    
    func isValidate() -> Bool {
    
        guard let oldNumber = self.currentNumberTextBox.textField.text, !oldNumber.isEmpty else{
            print("Number is empty")
            Messages.showErrorMessage(message: "Number is empty".localized)
            return false
        }
        
//        self.serviceCall(old_number: oldNumber, new_number: code + newNumber)
        
        return true
    }
    
    func serviceCall(old_number: String, new_number: String){
        
        let parameter : [String:String] = [
            "old_number":old_number,
            "new_number":new_number
        ]
        
        NetworkRequest.shared.changeNumber(parameters: parameter) { (response, error) in
            
            if error != nil {
                Messages.showErrorMessage(message: error?.message)
                return
            }
            
            DispatchQueue.main.async {
                
                let vc = CodeVerficationViewController.instantiate(fromAppStoryboard: .User)
                vc.number = new_number
                vc.isFromChangeNumber = true
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    
    // MARK: - Country Code Controller Set
    func configureUI() {
        
        self.currentNumberTextBox.textField.textColor = UIColor.gray
        
        if let user = UserManager.getUserObjectFromUserDefaults() {
            self.currentNumberTextBox.textField.text = user.phone
        }
    }

}
