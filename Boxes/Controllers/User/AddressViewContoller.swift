//
//  AddressViewContoller.swift
//  Boxes
//
//  Created by mac on 1/30/21.
//  Copyright © 2021 Apple. All rights reserved.
//
import UIKit
import SwiftyJSON

class AddressViewController: BaseViewController {
    @IBOutlet weak var cityTextBox: SurveyBox!
    @IBOutlet weak var areaTextBox: SurveyBox!
    @IBOutlet weak var streetTextBox: SurveyBox!
    @IBOutlet weak var blockTextBox: SurveyBox!
    @IBOutlet weak var buildingTextBox: SurveyBox!
    @IBOutlet weak var floorTextBox: SurveyBox!
    @IBOutlet weak var apartmentTextBox: SurveyBox!
    @IBOutlet weak var landmarksTextBox: SurveyBox!
    
    let citiesPickerView = UIPickerView()
    let areasPickerView = UIPickerView()
    private var address : AddressModel? = nil
    private var cities : [City] = []
    private var areas : [Area] = []
    private var cityId : String = ""
    private var areaId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initPickers()
        initTextFields()
        getCities()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    
    @IBAction func nextAction(_ sender: UIButton) {
        if streetTextBox.textField.text == "" {
            Messages.showErrorMessage(message: "Please enter street name")
        } else if blockTextBox.textField.text == "" {
            Messages.showErrorMessage(message: "Please enter block number")
        } else if buildingTextBox.textField.text == "" {
            Messages.showErrorMessage(message: "Please enter building number")
        } else if floorTextBox.textField.text == "" {
            Messages.showErrorMessage(message: "Please enter floor number")
        } else if apartmentTextBox.textField.text == "" {
            Messages.showErrorMessage(message: "Please enter apartment number")
        } else {
            storeAddress()
        }
    }
    
    func configureNavigationBar(){
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "Address".localized
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
//        self.placeBackButton(selectorBack: #selector(self.back))
    }
    
    private func initPickers(){
        citiesPickerView.delegate = self
        areasPickerView.delegate = self
    }
    
    private func initTextFields(){
        self.cityTextBox.textField.inputView = citiesPickerView
        self.cityTextBox.setTextFieldPlaceHolder(string: "Enter City".localized, labelTop : "City".localized)
        self.areaTextBox.textField.inputView = areasPickerView
        self.areaTextBox.setTextFieldPlaceHolder(string: "Enter Area".localized, labelTop : "Area".localized)
        self.streetTextBox.setTextFieldPlaceHolder(string: "Enter Street",labelTop: "Street")
        self.blockTextBox.setTextFieldPlaceHolder(string: "Enter Block",labelTop: "Block")
        self.buildingTextBox.setTextFieldPlaceHolder(string: "Enter Building",labelTop: "Building")
        self.floorTextBox.setTextFieldPlaceHolder(string: "Enter Floor",labelTop: "Floor")
        self.apartmentTextBox.setTextFieldPlaceHolder(string: "Enter Apartment",labelTop: "Apartment")
        self.landmarksTextBox.setTextFieldPlaceHolder(string: "Enter Landmarks",labelTop: "Landmarks")
    }
    
    private func getCities() {
        let parameter : [String:String] = ["language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"]
        NetworkRequest.shared.cities(parameters: parameter) { (response, error) in
            if let error = error {
                Messages.showErrorMessage(message: error.message)
                return
            }
            self.cities = JSON(response).arrayValue.map({ City.init(fromJson: $0) })
            if (self.areaId != "") { self.getAreas() } else { self.getAdress() }
        }
    }
    
    private func getAdress() {
        let parameter : [String:String] = ["language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"]
        NetworkRequest.shared.addressDetails(parameters: parameter) { (response, error) in
            if error != nil {
                self.cityId = self.cities.first?.id ?? ""
                self.cityTextBox.textField.text = self.cities.first?.cityName
                self.getAreas()
                return
            }
            self.address = AddressModel(fromJson: JSON(response))
            self.cityId = self.address!.cityId
            self.areaId = self.address!.areaId
            self.bindAddressDetails()
            self.getAreas()
        }
    }
    
    private func getAreas() {
        let parameter : [String:String] = ["city_id": cityId]
        NetworkRequest.shared.areas(parameters: parameter) { (response, error) in
            if let error = error {
                Messages.showErrorMessage(message: error.message)
                return
            }
            self.areas = JSON(response).arrayValue.map({ Area.init(fromJson: $0) })
            if self.areaId == "" || self.cityId != self.address?.cityId {
                self.areaId = self.areas.first?.id ?? ""
                self.areaTextBox.textField.text = self.areas.first?.areaName
            }
        }
    }
    
    private func bindAddressDetails() {
        blockTextBox.textField.text = address?.block
        streetTextBox.textField.text = address?.street
        buildingTextBox.textField.text = address?.building
        floorTextBox.textField.text = address?.floor
        apartmentTextBox.textField.text = address?.apartment
        landmarksTextBox.textField.text = address?.landmarks
        self.cityTextBox.textField.text = address?.city.cityName
        self.areaTextBox.textField.text = address?.area.areaName
    }
    
    private func storeAddress() {
        let parameter : [String:String] = [
            "city_id": cityId,
            "area_id": areaId,
            "block": blockTextBox.textField.text!,
            "street": streetTextBox.textField.text!,
            "building": buildingTextBox.textField.text!,
            "floor": floorTextBox.textField.text!,
            "apartment": apartmentTextBox.textField.text!,
            "landmarks": landmarksTextBox.textField.text ?? "",
        ]
        
        NetworkRequest.shared.storeAddress(parameters: parameter) { (response, error) in
            if let error = error {
                Messages.showErrorMessage(message: error.message)
                return
            }
            let vc = BmiViewController.instantiate(fromAppStoryboard: .User)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension AddressViewController: UIPickerViewDelegate, UIPickerViewDataSource  {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == citiesPickerView {
            return cities.count
        }else if pickerView == areasPickerView {
            return areas.count
        } else {
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == citiesPickerView {
            return cities[row].cityName
        }else if pickerView == areasPickerView {
            return areas[row].areaName
        } else {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == citiesPickerView {
            cityId = cities[row].id
            cityTextBox.textField.text = cities[row].cityName
            getAreas()
        }else if pickerView == areasPickerView {
            areaId = areas[row].id
            areaTextBox.textField.text = areas[row].areaName
        }
        self.view.endEditing(true)
    }
    
}
