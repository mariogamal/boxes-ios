//
//  DietViewController.swift
//  Boxes
//
//  Created by mac on 1/31/21.
//  Copyright © 2021 Apple. All rights reserved.
//
import UIKit
import SwiftyJSON
class DietViewController: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var dietTextBox: SurveyBox!
    @IBOutlet weak var otherTextBox: SurveyBox!
    @IBOutlet weak var ingredientsTextBox: SurveyBox!
    
    let dietPickerView = UIPickerView()
    var dietTypes = [""]
    private var dietData : [AllergiesOrDietModel] = []
    var diet_id : String?
    var survey = SurveyDetailsModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureTextFields()
        configurePickerView()
        serviceCallDiet()
        bindData()
    }
    
    @IBAction func saveSurvey(_ sender: Any) {
        survey.nonPreferredIngOther = otherTextBox.textField.text
        survey.dietId = diet_id
        saveSurveyService()
    }
    
    func configurePickerView(){
        self.dietPickerView.delegate = self
        self.dietTextBox.textField.inputView = dietPickerView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    
    func configureNavigationBar(){
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "SURVEY".localized
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
    }
    
    func configureTextFields(){
        self.dietTextBox.setTextFieldPlaceHolder(string: "PickYourType".localized, labelTop : "What is your Prefered Diet?".localized)
        self.ingredientsTextBox.setTextFieldPlaceHolder(string: "Ingredients".localized, labelTop : "Non preferred ingredients (if any)".localized)
        self.otherTextBox.setTextFieldPlaceHolder(string: "Others",labelTop: "Others")
        self.dietTextBox.textField.delegate = self
        self.ingredientsTextBox.textField.delegate = self
    }
    
    func serviceCallDiet(){
        let parameter : [String:String] = ["type": "Diet","language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"]
        NetworkRequest.shared.allergiesOrDiet(parameters: parameter) { (response, error) in
            if error != nil {
                return
            }
            let types = JSON(response).arrayValue.map({ AllergiesOrDietModel.init(fromJson: $0) })
            self.dietData = types
            DispatchQueue.main.async {
                self.dietPickerView.reloadAllComponents()
            }
        }
    }
    
    func bindData() {
        diet_id = survey.dietId
        dietTextBox.textField.text = survey.diet?.dietName
        otherTextBox.textField.text = survey.nonPreferredIngOther
        displayIngredients()
    }
    
    func displayIngredients() {
        var ingredients = ""
        survey.nonPreferredIngredients.forEach { ing in
            ingredients += "\(ing.ingName!), "
        }
        ingredientsTextBox.textField.text = String(ingredients.dropLast(2))
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == ingredientsTextBox.textField {
            let vc = IngredientsSelectionViewController.instantiate(fromAppStoryboard: .User)
            vc.selectedList = survey.nonPreferredIngredients
            vc.ingredientsDelegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func saveSurveyService() {
        let parameter : [String:String] = [
            "age":survey.age!,
            "diet_id":survey.dietId ?? "",
            "feet":survey.feet!,
            "inches":survey.inches!,
            "weight":survey.weight!,
            "bmi":survey.bmi!,
            "allergy_id": survey.allergyId ?? "",
            "gender":survey.gender!,
            "other_allergy_ingredients":survey.allergyIngOther!,
            "other_non_preferred_ingredients":survey.nonPreferredIngOther!,
            "allergy_ingredients": getSelectedIDsString(ingredients: survey.allergyIngredients),
            "non_preferred_ingredients": getSelectedIDsString(ingredients: survey.nonPreferredIngredients)
        ]
        
        NetworkRequest.shared.storeSurvey(parameters: parameter) { (response, error) in
            if error != nil {
                Messages.showErrorMessage(message: error?.message)
                return
            }
            Messages.showSuccessMessage(message: "Profile updated successfully")
            self.openTabController()
        }
        
    }
    
    func getSelectedIDsString(ingredients : [Ingredient]) -> String {
        var ingredientsString = ""
        ingredients.forEach { ing in
            ingredientsString += "\(ing.id!),"
        }
        return String(ingredientsString.dropLast(1))
    }
    
    func openTabController() {
        CartManager.shared.getCartItems()
        let vc = BaseLeftMenuViewController.instantiate(fromAppStoryboard: .LeftMenu)
        vc.leftViewPresentationStyle = .slideBelow
        UIWINDOW?.rootViewController = vc
    }
}

extension DietViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == dietPickerView {
            return dietData.count
        }else {
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == dietPickerView {
            return dietData[row].names
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == dietPickerView {
            dietTextBox.textField.text = dietData[row].names
            diet_id = dietData[row].id
        }
        self.view.endEditing(true)
    }
}

extension DietViewController : IngredientsSelectedDelegate {
    func onIngredientsSelected(ingredients: [Ingredient]) {
        otherTextBox.textField.becomeFirstResponder()
        survey.nonPreferredIngredients = ingredients
        displayIngredients()
    }
}
