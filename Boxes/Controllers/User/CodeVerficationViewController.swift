//
//  CodeVerficationViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/4/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import SwiftyJSON
import Firebase
import FirebaseAuth
import MOLH


class CodeVerficationViewController: BaseViewController {
    
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var resendLabel: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var termsConditionLabel: UILabel!
    @IBOutlet weak var termsConditionButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    
    
    var number : String?
    private var data : VerifyOTPModel?
    var isFromChangeNumber = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        otpTextField.textContentType = .oneTimeCode
        Auth.auth().languageCode = MOLHLanguage.currentAppleLanguage()
        configureCodefields()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    
    func configureNavigationBar(){
        
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "VARIFICATION CODE".localized
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.transparentNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
        // self.placeBackButton(selectorBack: #selector(self.back))
    }
    
    func configureCodefields(){
        
        self.instructionLabel.text = "Please enter the verification code sent to ".localized + (number ?? "")
        self.otpTextField.becomeFirstResponder()
        //  self.otpTextField.pinLength = 6
        sendFirebaseOTP()
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionResend(_ sender: UIButton) {
        sendFirebaseOTP()
    }
    
    @IBAction func actionSubmit(_ sender: UIButton) {
        verifyFirebaseOTP()
    }
    
    func sendFirebaseOTP() {
        
        guard let number = self.number, !number.isEmpty else {
            Messages.showErrorMessage(message: "number is empty".localized)
            print("number is empty")
            return
        }
        print(number)
        PhoneAuthProvider.provider().verifyPhoneNumber("+"+number, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print(error)
                Messages.showErrorMessage(message: error.localizedDescription)
                return
            }
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
        }
    }
    
    func verifyFirebaseOTP() {
        
        guard (otpTextField.text?.count == 6)  else{
            Messages.showErrorMessage(message: "Enter valid OTP".localized)
            return
        }
        
        LoadingOverlay.shared.showOverlay()
        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID!,
            verificationCode: otpTextField.text ?? "")
        
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if let error = error {
                Messages.showErrorMessage(message: error.localizedDescription)
                LoadingOverlay.shared.hideOverlayView()
                return
            }
            authResult?.user.getIDTokenForcingRefresh(true, completion: { idToken, error in
                if let error = error {
                    Messages.showErrorMessage(message: error.localizedDescription)
                    LoadingOverlay.shared.hideOverlayView()
                    return
                }
                self.serviceCall(idToken: idToken!)
            })
        }
    }
    
    
    func serviceCall(idToken: String) {
        guard (otpTextField.text?.count == 6)  else{
            Messages.showErrorMessage(message: "Enter valid OTP".localized)
            return
        }
        
        guard let number = self.number, !number.isEmpty else {
            Messages.showErrorMessage(message: "number is empty".localized)
            print("number is empty")
            return
        }
        
        let parameter : [String:String] = [
            "id_token": idToken,
            "phone": number,
            "device_type": "ios",
            "device_token": "123"
        ]
        
        NetworkRequest.shared.login(parameters: parameter) { (response, error) in
            
            if let err = error {
                Messages.showErrorMessage(message: err.message)
                return
            }
            let data = VerifyOTPModel(fromJson: JSON(response))
            UserManager.token = "\(data.tokenType ?? "Bearer") \(data.accessToken ?? "")"
            UserManager.saveUserObjectToUserDefaults(userResult: data.user)
            DispatchQueue.main.async {
                CartManager.shared.getCartItems()
                if self.isFromChangeNumber{
                    self.dismiss(animated: true, completion: nil)
                }else {
                    if let user = UserManager.getUserObjectFromUserDefaults() {
                        if user.isProfileFilled == 1 {
                            let vc = BaseLeftMenuViewController.instantiate(fromAppStoryboard: .LeftMenu)
                            vc.leftViewPresentationStyle = .slideBelow
                            UIWINDOW?.rootViewController = vc
                        }else {
                            
                            let vc = CreateProfileViewController.instantiate(fromAppStoryboard: .User)
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func actionTermsCondition(_ sender: UIButton) {
        
        let vc = TermsConditionPrivacyPolicyViewController.instantiate(fromAppStoryboard: .User)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
