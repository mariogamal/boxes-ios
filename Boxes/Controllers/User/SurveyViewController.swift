//
//  SurveyViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/8/19.
//  Copyright © 2019 Apple. All rights reserved.
//


import UIKit
import IQKeyboardManagerSwift
import SwiftyJSON

class SurveyViewController: BaseViewController {
    
    @IBOutlet weak var ingredientsTextBox: SurveyBox!
    @IBOutlet weak var ageTextBox: SurveyBox!
    @IBOutlet weak var dietTextBox: SurveyBox!
    @IBOutlet weak var heightFeetTextBox: SurveyBox!
    @IBOutlet weak var heightInchesTextBox: SurveyBox!
    @IBOutlet weak var weightKgsTextBox: SurveyBox!
    @IBOutlet weak var wightInchesTextBox: SurveyBox!
    @IBOutlet weak var allergiesTextBox: SurveyBox!
    @IBOutlet weak var additionalPrefTextBox: SurveyBox!
    @IBOutlet weak var proceedButton: UIButton!
    
    //pickerview
    let dietPickerView = UIPickerView()
    let allergyPickerView = UIPickerView()
    let agePickerView = UIPickerView()
    let heightFeetPickerView = UIPickerView()
    let heightInchesPickerView = UIPickerView()
    let weightKgsPickerView = UIPickerView()
    
    var dietTypes = [""]
    var allergyTypes = [""]
    var ages = [String]()
    var heightFeets = [String]()
    var heightInches = [String]()
    var weightKgs = [String]()
    private var dietData : [AllergiesOrDietModel] = []
    private var allergyData : [AllergiesOrDietModel] = []
    
    var diet_id : String?
    var allergy_id : String?
    
    var isFromSetting = false
    var ingredientsList = [Ingredient]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureTextFields()
        configurePickerView()
        serviceCallGetSurvey()
        serviceCallDiet()
        serviceCallAllergy()
        getAllIngredients()
        configurePickerViewData()
        
    }
    
    func configurePickerViewData(){
        //ages
        for i in 10...60 {
            self.ages.append(String(i))
        }
        
        //heights in feet
        for i in 1...6 {
            self.heightFeets.append(String(i))
        }
        
        //heights in inches
        for i in 1...10 {
            self.heightInches.append(String(i))
        }
        
        //weights in kg
        for i in 10...200 {
            self.weightKgs.append(String(i))
        }
    }
    
    func configurePickerView(){
        
        self.dietPickerView.delegate = self
        self.allergyPickerView.delegate = self
        self.agePickerView.delegate = self
        self.heightFeetPickerView.delegate = self
        self.heightInchesPickerView.delegate = self
        self.weightKgsPickerView.delegate = self
        self.dietTextBox.textField.inputView = dietPickerView
        self.allergiesTextBox.textField.inputView = allergyPickerView
        self.ageTextBox.textField.inputView = agePickerView
        self.heightFeetTextBox.textField.inputView = heightFeetPickerView
        self.heightInchesTextBox.textField.inputView = heightInchesPickerView
        self.weightKgsTextBox.textField.inputView = weightKgsPickerView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        configureNavigationBar()
        proceedButton.setTitle("PROCEED".localized, for: .normal)
        
        
    }
    
    func configureNavigationBar(){
        
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "SURVEY".localized
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
        
        if isFromSetting {
            // self.placeBackButton(selectorBack: #selector(self.back))
        }else {
            self.placeRightButtons(buttons: configure())
        }
        
    }
    
    func configure() -> [UIBarButtonItem] {
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        let b = UIBarButtonItem(title: "SKIP".localized, style: .plain, target: self, action: #selector(self.home))
        b.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        return [b]
    }
    
    @objc func home(){
        
        let vc = BaseLeftMenuViewController.instantiate(fromAppStoryboard: .LeftMenu)
        vc.leftViewPresentationStyle = .slideBelow
        UIWINDOW?.rootViewController = vc
    }
    
    func configureTextFields(){
        
        self.ageTextBox.setTextFieldPlaceHolder(string: "EnterAge".localized, labelTop : "What'sYourAge".localized)
        self.dietTextBox.setTextFieldPlaceHolder(string: "PickYourType".localized, labelTop : "What is your Prefered Diet?".localized)
        self.heightFeetTextBox.setTextFieldPlaceHolder(string: "Feet".localized, labelTop : "Your Height".localized)
        self.heightInchesTextBox.setTextFieldPlaceHolder(string: "Inches".localized)
        self.weightKgsTextBox.setTextFieldPlaceHolder(string: "Kgs".localized, labelTop : "Your Weight?".localized)
        self.wightInchesTextBox.setTextFieldPlaceHolder(string: "kg/m2".localized, labelTop : "Your BMI?".localized)
        self.allergiesTextBox.setTextFieldPlaceHolder(string: "Your Allergies (If any)".localized, labelTop : "PickYourType".localized)
        self.additionalPrefTextBox.setTextFieldPlaceHolder(string: "Additional Preferences".localized, labelTop : "Enter Text".localized)
        self.ingredientsTextBox.setTextFieldPlaceHolder(string: "Ingredients".localized, labelTop: "Allegries from ingredients (if any)".localized)
        
        self.ageTextBox.textField.delegate = self
        self.dietTextBox.textField.delegate = self
        self.heightFeetTextBox.textField.delegate = self
        self.heightInchesTextBox.textField.delegate = self
        self.weightKgsTextBox.textField.delegate = self
        self.wightInchesTextBox.textField.delegate = self
        self.wightInchesTextBox.textField.isUserInteractionEnabled = false
        self.allergiesTextBox.textField.delegate = self
        self.additionalPrefTextBox.textField.delegate = self
        self.ingredientsTextBox.textField.delegate = self
        
        self.ageTextBox.textField.keyboardType = .numberPad
        self.ageTextBox.textField.returnKeyType = .next
        self.dietTextBox.textField.keyboardType = .alphabet
        self.dietTextBox.textField.returnKeyType = .next
        self.heightFeetTextBox.textField.keyboardType = .numberPad
        self.heightFeetTextBox.textField.returnKeyType = .next
        self.heightInchesTextBox.textField.keyboardType = .numberPad
        self.heightInchesTextBox.textField.returnKeyType = .next
        self.weightKgsTextBox.textField.keyboardType = .numberPad
        self.weightKgsTextBox.textField.returnKeyType = .next
        self.wightInchesTextBox.textField.keyboardType = .numberPad
        self.wightInchesTextBox.textField.returnKeyType = .next
        self.allergiesTextBox.textField.keyboardType = .alphabet
        self.allergiesTextBox.textField.returnKeyType = .next
        self.ingredientsTextBox.textField.keyboardType = .alphabet
        self.ingredientsTextBox.textField.returnKeyType = .next
        self.additionalPrefTextBox.textField.keyboardType = .alphabet
        self.additionalPrefTextBox.textField.returnKeyType = .done
    }
    
    var selectedIDs  : [String] = []
    
    
    @IBAction func actionProceed(_ sender: UIButton) {
        
        guard isValidate() else {
            return
        }
        
    }
    
    func isValidate() -> Bool {
        
        guard let age = self.ageTextBox.textField.text, !age.isEmpty else{
            Messages.showErrorMessage(message: "Write your age".localized)
            return false
        }
        
        guard let dietid = self.diet_id else{
            Messages.showErrorMessage(message: "Select your diet".localized)
            return false
        }
        
        guard let heightFeet = self.heightFeetTextBox.textField.text, !heightFeet.isEmpty else{
            Messages.showErrorMessage(message: "Write your height Feet".localized)
            return false
        }
        
        guard let heightInches = self.heightInchesTextBox.textField.text, !heightInches.isEmpty else{
            Messages.showErrorMessage(message: "Write your last Height in Inches".localized)
            return false
        }
        
        guard let weightKgs = self.weightKgsTextBox.textField.text, !weightKgs.isEmpty else{
            Messages.showErrorMessage(message: "Write your last Weight in Kgs".localized)
            return false
        }
        
        guard let weightInches = self.wightInchesTextBox.textField.text, !weightInches.isEmpty else{
            Messages.showErrorMessage(message: "Write your BMI".localized)
            return false
        }
        
        guard let allergyid = self.allergy_id else{
            Messages.showErrorMessage(message: "Select your allergy".localized)
            return false
        }
        
        guard let addPref = self.additionalPrefTextBox.textField.text, !addPref.isEmpty else{
            Messages.showErrorMessage(message: "Write addtional Preferences".localized)
            return false
        }
        
        self.serviceCallPostSurvey(age: age, diet_id: dietid, feet: heightFeet, inches: heightInches, weight: weightKgs, bmi: weightKgs, allergy_id: allergyid, additional_preferences: addPref)
        
        return true
    }
    
    func serviceCallGetSurvey(){
        
        let parameter : [String:String] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"
        ]
        
        NetworkRequest.shared.survey_details(parameters: parameter) { (response, error) in
            
            if error != nil {
                return
            }
            
            DispatchQueue.main.async {
                
                let data = SurveyDetailsModel(fromJson: JSON(response))
                var names = ""
                for ingredient in data.allergyIngredients {
                    self.selectedIDs.append(ingredient.id)
                    names += ", " + ingredient.ingName
                }
                if names != "" {
                    names = names.substring(from: 1)
                }
                self.setValues(data: data, ingNames: names)
            }
        }
    }
    
    func setValues(data: SurveyDetailsModel, ingNames: String){
        self.ageTextBox.textField.text = data.age
        self.dietTextBox.textField.text = data.diet?.dietName
        self.diet_id = data.dietId
        self.heightFeetTextBox.textField.text = data.feet
        self.heightInchesTextBox.textField.text = data.inches
        self.weightKgsTextBox.textField.text = data.weight
        self.wightInchesTextBox.textField.text = data.bmi
        self.allergiesTextBox.textField.text = data.allergy?.allergyName
        self.allergy_id = data.allergyId
        self.additionalPrefTextBox.textField.text = data.additionalPreferences
        self.ingredientsTextBox.textField.text = ingNames
    }
    
    func serviceCallPostSurvey(
        age:String,
        diet_id:String,
        feet:String,
        inches:String,
        weight:String,
        bmi:String,
        allergy_id:String,
        additional_preferences:String){
        
        
        let parameter : [String:String] = [
            "age":age,
            "diet_id":diet_id,
            "feet":feet,
            "inches":inches,
            "weight":weight,
            "bmi":bmi,
            "allergy_id": allergy_id,
            "additional_preferences":additional_preferences,
            "ingredients": self.getSelectedIDsString()
        ]
        
        NetworkRequest.shared.storeSurvey(parameters: parameter) { (response, error) in
            
            if error != nil {
                Messages.showErrorMessage(message: error?.message)
                print(error?.message ?? "N/A".localized)
                return
            }
            
            //            let types = JSON(response).arrayValue.map({ AllergiesOrDietModel.init(fromJson: $0) })
            //            self.dietData = types
            
            DispatchQueue.main.async {
                
                Messages.showSuccessMessage(message: "Survey Submitted Successfully".localized)
                if let userData: UserModel = UserManager.getUserObjectFromUserDefaults() {
                    userData.isSurveyFilled = 1
                    UserManager.saveUserObjectToUserDefaults(userResult: userData)
                }

                if self.isFromSetting {
                    self.navigationController?.popViewController(animated: true)
                }else {
                    
                    let vc = BaseLeftMenuViewController.instantiate(fromAppStoryboard: .LeftMenu)
                    vc.leftViewPresentationStyle = .slideBelow
                    UIWINDOW?.rootViewController = vc
                }
                
            }
        }
    }
    
    private func getSelectedIDsString() -> String {
        var ids = ""
        for id in selectedIDs {
            ids += "," + id
        }
        if selectedIDs.count == 0 {
            return ids
        } else {
            return ids.substring(from: 1)
        }
    }
    
    func serviceCallDiet(){
        
        let parameter : [String:String] = ["type": "Diet","language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"]
        
        NetworkRequest.shared.allergiesOrDiet(parameters: parameter) { (response, error) in
            
            if error != nil {
                return
            }
            
            let types = JSON(response).arrayValue.map({ AllergiesOrDietModel.init(fromJson: $0) })
            self.dietData = types
            
            DispatchQueue.main.async {
                
                self.dietPickerView.reloadAllComponents()
            }
        }
    }
    
    
    func serviceCallAllergy(){
        
        let parameter : [String:String] = ["type": "Allergy","language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"]
        
        NetworkRequest.shared.allergiesOrDiet(parameters: parameter) { (response, error) in
            
            if error != nil {
                return
            }
            
            let types = JSON(response).arrayValue.map({ AllergiesOrDietModel.init(fromJson: $0) })
            self.allergyData = types
            
            DispatchQueue.main.async {
                
                //                Messages.showSuccessMessage(message: "Successfully get allergy data")
                self.allergyPickerView.reloadAllComponents()
            }
        }
    }
    
    func getAllIngredients() {
        
        let parameter : [String:String] = ["language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"]
        
        NetworkRequest.shared.ingredients(parameters: parameter) { (response, error) in
            
            if error != nil {
                return
            }
            
            self.ingredientsList = JSON(response).arrayValue.map({ Ingredient.init(fromJson: $0) })
        }
    }
    
    func displayIngrediantsDialog() {
        var pickerData  = [[String:String]]()
        
        for ingredient in ingredientsList {
            pickerData.append(
                [
                    "value":ingredient.id,
                    "display":ingredient.ingName
                ]
            )
        }
        
        MultiPickerDialog().show(title: "Select Ingredients".localized,doneButtonTitle:"Done".localized, cancelButtonTitle:"Cancel".localized ,options: pickerData, selected: self.selectedIDs) {
            values -> Void in
            print("callBack \(values)")
            var finalText = ""
            self.selectedIDs.removeAll()
            for (index,value) in values.enumerated(){
                self.selectedIDs.append(value["value"]!)
                finalText = finalText  + value["display"]! + (index < values.count - 1 ? ", ": "")
            }
            self.ingredientsTextBox.textField.text = finalText
        }
    }
    
    
}

extension SurveyViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.ageTextBox.textField{
            textField.resignFirstResponder()
            self.dietTextBox.textField.becomeFirstResponder()
        }else if textField == self.dietTextBox.textField{
            textField.resignFirstResponder()
            self.heightFeetTextBox.textField.becomeFirstResponder()
        }else if textField == self.heightFeetTextBox.textField{
            textField.resignFirstResponder()
            self.weightKgsTextBox.becomeFirstResponder()
        }else
            if textField == self.weightKgsTextBox.textField{
                textField.resignFirstResponder()
                self.wightInchesTextBox.textField.becomeFirstResponder()
            } else if textField == self.wightInchesTextBox.textField{
                textField.resignFirstResponder()
                self.allergiesTextBox.textField.becomeFirstResponder()
            }else if textField == self.allergiesTextBox.textField{
                textField.resignFirstResponder()
                self.additionalPrefTextBox.textField.becomeFirstResponder()
            } else if textField == self.additionalPrefTextBox.textField {
                textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == heightFeetTextBox.textField {
            if !heightFeetTextBox.textField.text!.isEmpty && !heightInchesTextBox.textField.text!.isEmpty && !weightKgsTextBox.textField.text!.isEmpty {
                let bmi = calculateBMI(heightInFeets: heightFeetTextBox.textField.text!, heightInInches: heightInchesTextBox.textField.text!, weightInKgs: weightKgsTextBox.textField.text!)
                wightInchesTextBox.textField.text = bmi
                print(bmi)
            }
        }else if textField == heightInchesTextBox.textField {
            if !heightFeetTextBox.textField.text!.isEmpty && !heightInchesTextBox.textField.text!.isEmpty && !weightKgsTextBox.textField.text!.isEmpty {
                let bmi = calculateBMI(heightInFeets: heightFeetTextBox.textField.text!, heightInInches: heightInchesTextBox.textField.text!, weightInKgs: weightKgsTextBox.textField.text!)
                wightInchesTextBox.textField.text = bmi
                print(bmi)
            }
        }else if textField == weightKgsTextBox.textField {
            if !heightFeetTextBox.textField.text!.isEmpty && !heightInchesTextBox.textField.text!.isEmpty && !weightKgsTextBox.textField.text!.isEmpty {
                let bmi = calculateBMI(heightInFeets: heightFeetTextBox.textField.text!, heightInInches: heightInchesTextBox.textField.text!, weightInKgs: weightKgsTextBox.textField.text!)
                wightInchesTextBox.textField.text = bmi
                print(bmi)
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == ingredientsTextBox.textField {
            self.displayIngrediantsDialog()
        }
    }
    
    func calculateBMI(heightInFeets:String, heightInInches:String, weightInKgs:String)-> String{
        
        let weightInDouble = Double(weightInKgs) ?? 0.0
        let heightFeetInDouble = Double(heightInFeets) ?? 0.0
        let heightFeetInInches = heightFeetInDouble * 12
        let heightInchesInDouble = Double(heightInInches) ?? 0.0
        let totalHeightInInches: Double = heightFeetInInches + heightInchesInDouble
        let totalHeightInMeter = totalHeightInInches * 0.0254
        let totalHeightInMeterSquare = totalHeightInMeter * totalHeightInMeter
        let bmi:Double = weightInDouble / totalHeightInMeterSquare
        let bmiInString = String(format:"%.2f", bmi)
        return bmiInString
    }
}

//MARK: - PickerView Delegates
extension SurveyViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == dietPickerView {
            return dietData.count
        }else if pickerView == allergyPickerView {
            return allergyData.count
        }else if pickerView == agePickerView {
            return ages.count
        }else if pickerView == heightFeetPickerView {
            return heightFeets.count
        }else if pickerView == heightInchesPickerView {
            return heightInches.count
        }else {
            return weightKgs.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == dietPickerView {
            return dietData[row].names
        }else if pickerView == allergyPickerView {
            return allergyData[row].names
        }else if pickerView == agePickerView {
            return ages[row]
        }else if pickerView == heightFeetPickerView {
            return heightFeets[row]
        }else if pickerView == heightInchesPickerView {
            return heightInches[row]
        }else {
            return weightKgs[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == dietPickerView {
            dietTextBox.textField.text = dietData[row].names
            diet_id = dietData[row].id
        }else if pickerView == allergyPickerView {
            allergiesTextBox.textField.text = allergyData[row].names
            allergy_id = allergyData[row].id
        }else if pickerView == agePickerView {
            ageTextBox.textField.text = ages[row]
        }else if pickerView == heightFeetPickerView {
            heightFeetTextBox.textField.text = heightFeets[row]
        }else if pickerView == heightInchesPickerView {
            heightInchesTextBox.textField.text = heightInches[row]
        }else {
            weightKgsTextBox.textField.text = weightKgs[row]
        }
    }
}
