//
//  BmiViewController.swift
//  Boxes
//
//  Created by mac on 1/31/21.
//  Copyright © 2021 Apple. All rights reserved.
//
import UIKit
import SwiftyJSON

class BmiViewController: BaseViewController, UITextFieldDelegate {
    @IBOutlet weak var ageTextBox: SurveyBox!
    @IBOutlet weak var genderTextBox: SurveyBox!
    @IBOutlet weak var heightFeetTextBox: SurveyBox!
    @IBOutlet weak var heightInchTextBox: SurveyBox!
    @IBOutlet weak var weightTextBox: SurveyBox!
    @IBOutlet weak var BmiTextBox: SurveyBox!
    
    let genderPickerView = UIPickerView()
    private var gender = ["Male","Female"]
    var isFromSetting = false
    var survey = SurveyDetailsModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initPickers()
        initTextFields()
        serviceCallGetSurvey()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    
    @IBAction func nextAction(_ sender: UIButton) {
        survey.age = self.ageTextBox.textField.text
        survey.feet = self.heightFeetTextBox.textField.text
        survey.inches = self.heightInchTextBox.textField.text
        survey.weight = self.weightTextBox.textField.text
        survey.bmi = self.BmiTextBox.textField.text
        survey.gender = self.genderTextBox.textField.text
        let vc = AllegryViewController.instantiate(fromAppStoryboard: .User)
        vc.survey = self.survey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func configureNavigationBar(){
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "BMI".localized
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
    }
    
    private func initPickers(){
        genderPickerView.delegate = self
    }
    
    private func initTextFields(){
        self.genderTextBox.textField.text = "Male"
        self.genderTextBox.textField.inputView = genderPickerView
        self.ageTextBox.setTextFieldPlaceHolder(string: "Enter Age".localized, labelTop : "Age".localized)
        self.genderTextBox.setTextFieldPlaceHolder(string: "Male", labelTop : "Gender".localized)
        self.heightFeetTextBox.setTextFieldPlaceHolder(string: "Feet",labelTop: "Height")
        self.heightInchTextBox.setTextFieldPlaceHolder(string: "Inch")
        self.weightTextBox.setTextFieldPlaceHolder(string: "Kg",labelTop: "Weight")
        self.BmiTextBox.setTextFieldPlaceHolder(string: "BMI = ",labelTop: "BMI")
        self.heightFeetTextBox.textField.delegate = self
        self.heightInchTextBox.textField.delegate = self
        self.weightTextBox.textField.delegate = self
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == heightFeetTextBox.textField {
            if !heightFeetTextBox.textField.text!.isEmpty && !heightInchTextBox.textField.text!.isEmpty && !weightTextBox.textField.text!.isEmpty {
                let bmi = calculateBMI(heightInFeets: heightFeetTextBox.textField.text!, heightInInches: heightInchTextBox.textField.text!, weightInKgs: weightTextBox.textField.text!)
                BmiTextBox.textField.text = bmi
                print(bmi)
            }
        }else if textField == heightInchTextBox.textField {
            if !heightFeetTextBox.textField.text!.isEmpty && !heightInchTextBox.textField.text!.isEmpty && !weightTextBox.textField.text!.isEmpty {
                let bmi = calculateBMI(heightInFeets: heightFeetTextBox.textField.text!, heightInInches: heightInchTextBox.textField.text!, weightInKgs: weightTextBox.textField.text!)
                BmiTextBox.textField.text = bmi
                print(bmi)
            }
        }else if textField == weightTextBox.textField {
            if !heightFeetTextBox.textField.text!.isEmpty && !heightInchTextBox.textField.text!.isEmpty && !weightTextBox.textField.text!.isEmpty {
                let bmi = calculateBMI(heightInFeets: heightFeetTextBox.textField.text!, heightInInches: heightInchTextBox.textField.text!, weightInKgs: weightTextBox.textField.text!)
                BmiTextBox.textField.text = bmi
                print(bmi)
            }
        }
    }
    
    func calculateBMI(heightInFeets:String, heightInInches:String, weightInKgs:String)-> String{
        let weightInDouble = Double(weightInKgs) ?? 0.0
        let heightFeetInDouble = Double(heightInFeets) ?? 0.0
        let heightFeetInInches = heightFeetInDouble * 12
        let heightInchesInDouble = Double(heightInInches) ?? 0.0
        let totalHeightInInches: Double = heightFeetInInches + heightInchesInDouble
        let totalHeightInMeter = totalHeightInInches * 0.0254
        let totalHeightInMeterSquare = totalHeightInMeter * totalHeightInMeter
        let bmi:Double = weightInDouble / totalHeightInMeterSquare
        let bmiInString = String(format:"%.2f", bmi)
        return bmiInString
    }
    
    func serviceCallGetSurvey(){
        
        let parameter : [String:String] = ["language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"]
        
        NetworkRequest.shared.survey_details(parameters: parameter) { (response, error) in
            
            if error != nil {
                return
            }
            
            DispatchQueue.main.async {
                
                self.survey = SurveyDetailsModel(fromJson: JSON(response))
                var names = ""
                for ingredient in self.survey.allergyIngredients {
                    names += ", " + ingredient.ingName
                }
                if names != "" {
                    names = names.substring(from: 1)
                }
                self.setValues()
            }
        }
    }
    
    func setValues(){
        self.ageTextBox.textField.text = survey.age
        self.genderTextBox.textField.text = survey.gender
        self.heightFeetTextBox.textField.text = survey.feet
        self.heightInchTextBox.textField.text = survey.inches
        self.weightTextBox.textField.text = survey.weight
        self.BmiTextBox.textField.text = survey.bmi
    }
    
}
extension BmiViewController: UIPickerViewDelegate, UIPickerViewDataSource  {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == genderPickerView {
            return gender.count
        }else {
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == genderPickerView {
            return gender[row]
        }else {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == genderPickerView {
            genderTextBox.textField.text = gender[row]
        }
        self.view.endEditing(true)
    }
    
}
