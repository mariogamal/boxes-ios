//
//  IngredientsSelectionViewController.swift
//  Boxes
//
//  Created by Mario Gamal on 2/7/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import SwiftyJSON

class IngredientsSelectionViewController: BaseViewController {

    @IBOutlet weak var ingredientsTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var ingredients = [Ingredient]()
    var selectedList = [Ingredient]()
    var filteredList = [Ingredient]()
    
    var ingredientsDelegate : IngredientsSelectedDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        searchBar.delegate = self
        ingredientsTableView.delegate = self
        ingredientsTableView.dataSource = self
        
        ingredientsTableView.register(UINib(nibName: IngredientCell.identifier, bundle: .main), forCellReuseIdentifier: IngredientCell.identifier)
        
        getAllIngredients()
        self.placeRightButtons(buttons: configure())
    }

    func configureNavigationBar(){
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "Ingredients".localized
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
    }
    
    func configure() -> [UIBarButtonItem] {
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        let b = UIBarButtonItem(title: "DONE".localized, style: .plain, target: self, action: #selector(self.done))
        b.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        return [b]
    }
    
    @objc func done(){
        ingredientsDelegate?.onIngredientsSelected(ingredients: selectedList)
        self.navigationController?.popViewController(animated: true)
    }
    
    private func getAllIngredients() {
        let parameter : [String:String] = ["language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"]
        NetworkRequest.shared.ingredients(parameters: parameter) { (response, error) in
            if error != nil {
                return
            }
            self.ingredients = JSON(response).arrayValue.map({ Ingredient.init(fromJson: $0) })
            self.filteredList = self.ingredients
            self.reloadTableView()
        }
    }
    
    private func reloadTableView() {
        filteredList.forEach { ingredient in
            ingredient.isSelected = selectedList.contains(where: { $0.id == ingredient.id})
        }
        ingredientsTableView.reloadData()
    }
    
}

extension IngredientsSelectionViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: IngredientCell.identifier, for: indexPath) as! IngredientCell
        cell.ingredientName.text = filteredList[indexPath.row].ingName
        cell.checkMark.image = filteredList[indexPath.row].isSelected ? #imageLiteral(resourceName: "check_box_active") : nil
        cell.selectionStyle = .none
        return cell
    }
    
}

extension IngredientsSelectionViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ingId = filteredList[indexPath.row].id
        if selectedList.contains(where: {$0.id == ingId}) {
            selectedList.removeAll(where:{$0.id == ingId})
        } else {
            selectedList.append(filteredList[indexPath.row])
        }
        print(selectedList.count)
        reloadTableView()
    }
    
}

extension IngredientsSelectionViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredList = searchText.isEmpty ? ingredients : ingredients.filter { $0.ingName.contains(searchText) }
        reloadTableView()
    }
}

protocol IngredientsSelectedDelegate {
    func onIngredientsSelected(ingredients : [Ingredient])
}
