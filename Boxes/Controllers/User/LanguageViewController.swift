//
//  LanguageViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/3/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import MOLH

class LanguageViewController: BaseViewController {
    
    
    @IBOutlet weak var greetingsLabel: UILabel!
    @IBOutlet weak var englishLanguageView: UIView!
    @IBOutlet weak var arabicLanguageView: UIView!
    
    @IBOutlet weak var englishLanguageLabel: UILabel!
    @IBOutlet weak var arabicLanguageLabel: UILabel!
    
    @IBOutlet weak var englishLanguageIcon: UIImageView!
    @IBOutlet weak var arabicLanguageIcon: UIImageView!
    
    var isFromSetting = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //        self.navigationController?.resetNavigationBar()
        //        self.englishLanguageView.makebox()
        //        self.arabicLanguageView.makebox()
        //        self.placeLeftButtons(buttons: configureButtons())
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isFromSetting {
            configureNavigationBar()
        }else {
            self.navigationController?.hideNavigationBar()
        }
        
    }
    
    func configureNavigationBar(){
        
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.transparentNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
        
        // self.placeBackButton(selectorBack: #selector(self.back))
        
    }
    
    func configureButtons() -> [UIBarButtonItem] {
        let btn1 = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(add))
        
        return [btn1]
    }
    
    @objc func add() {
        
    }
    
    func pushLoginViewController(){
        let vc = LoginViewController.instantiate(fromAppStoryboard: .User)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionEnglish(_ sender: UIButton) {
        changeLanguage(languageCode: "en", languageId: "73089eac-068f-4d4f-ae59-8fd478fefc1c")
    }
    
    @IBAction func actionArabic(_ sender: UIButton) {
        changeLanguage(languageCode: "ar", languageId: "8f50bbe7-84bb-4907-97e6-755049948c75")
    }
    
    func changeLanguage(languageCode: String, languageId: String) {
        UserDefaults.standard.set(isFromSetting, forKey: UserDefaultsKey.IsFromSetting)
        UserManager.saveUserSelectedLanguage(languageId: languageId)
        MOLH.setLanguageTo(languageCode)
        MOLH.reset()
    }
    
}
