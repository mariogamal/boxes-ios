//
//  LoginViewController.swift
//  Boxes
//
//  Created by MacAir on 03/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import CountryPickerView
import SwiftyJSON

class LoginViewController: BaseViewController {
    
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var instructionLabel: UILabel!
    
    @IBOutlet weak var phoneCodeView: UIView!
    @IBOutlet weak var countryCodeView: UIView!
    @IBOutlet weak var numberTextfield: UITextField!
    @IBOutlet weak var flagIconImageView: UIImageView!
    @IBOutlet weak var codeLabel: UILabel!
    
    @IBOutlet weak var doneButton: UIButton!
    
    
    
    var countryCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //        self.title = "Language"
        
        //        // self.placeBackButton(selectorBack: #selector(self.back))
        configureUI()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "YOUR PHONE NUMBER".localized
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.transparentNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
        self.placeRightButtons(buttons: configure())
    }
    
    func serviceCall() {
        
        guard let number = self.numberTextfield.text, !number.isEmpty, let code = self.codeLabel.text else{
            print("Number is empty")
            Messages.showErrorMessage(message: "Number is empty".localized)
            return
        }
        
        let newCode = code.replacingOccurrences(of: "+", with: "")
        
        DispatchQueue.main.async {
            let vc = CodeVerficationViewController.instantiate(fromAppStoryboard: .User)
            vc.number = newCode + number
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func configure() -> [UIBarButtonItem] {
        
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        
        let b = UIBarButtonItem(title: "SKIP".localized, style: .plain, target: self, action: #selector(self.home))
        b.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        return [b]
    }
    
    @objc func home(){
        let vc = BaseLeftMenuViewController.instantiate(fromAppStoryboard: .LeftMenu)
        vc.leftViewPresentationStyle = .slideBelow
        UIWINDOW?.rootViewController = vc
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        let vc = BaseLeftMenuViewController.instantiate(fromAppStoryboard: .LeftMenu)
        vc.leftViewPresentationStyle = .slideBelow
        UIWINDOW?.rootViewController = vc
    }
    
    @IBAction func doneAction(_ sender: UIButton) {
        
        serviceCall()
        
    }
    
    @IBAction func skipAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func configureUI() {
        
        configureCurrenCountry()
        numberTextfield.keyboardType = .numberPad
        numberTextfield.textColor = .white
        numberTextfield.placeHolderColor = UIColor.gray
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.actionCountryCodeView))
        countryCodeView.addGestureRecognizer(tap)
    }
    
    @objc func actionCountryCodeView() {
        /*  let alert = UIAlertController(style: .actionSheet, message: "Select Country".localized)
         alert.addLocalePicker(type: .phoneCode) { (info) in
         self.configureCurrenCountry(flag: info?.flag, code: info?.phoneCode)
         }
         alert.addAction(title: "CANCEL".localized, style: .cancel)
         alert.show()*/
    }
    
    func configureCurrenCountry(flag: UIImage? = nil, code: String? = nil){
        //let cp = CountryPickerView()
        //flagIconImageView.image = flag ?? cp.selectedCountry.flag
        flagIconImageView.image = UIImage(named: "Kuwait_flag")
        //codeLabel.text = code ?? cp.selectedCountry.phoneCode
        codeLabel.text = "+965"
    }
    
    // MARK: - Country Code Controller Set
    func setupCountryPickerView(){
        let cp = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 75, height: 50))
        cp.showCountryCodeInView = false
        cp.flagSpacingInView = 10
        cp.textColor = .white
        phoneCodeView.addSubview(cp)
        numberTextfield.placeholder = "000000000"
        cp.delegate = self
        cp.dataSource = self
        countryCode = cp.selectedCountry.phoneCode
    }
    
}

// MARK: - Country Code Extension
extension LoginViewController : CountryPickerViewDelegate, CountryPickerViewDataSource {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        countryCode = country.phoneCode
        
    }
}
