//
//  AllergyViewController.swift
//  Boxes
//
//  Created by mac on 1/31/21.
//  Copyright © 2021 Apple. All rights reserved.
//
import UIKit
import SwiftyJSON

class AllegryViewController: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var allergiesTextBox: SurveyBox!
    @IBOutlet weak var ingredientsTextBox: SurveyBox!
    @IBOutlet weak var otherTextBox: SurveyBox!
    
    let allergyPickerView = UIPickerView()
    var allergyTypes = [""]
    private var allergyData : [AllergiesOrDietModel] = []
    var allergy_id : String?
    var isFromSetting = false
    var survey = SurveyDetailsModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTextFields()
        configurePickerView()
        serviceCallAllergy()
        bindData()
    }
    
    @IBAction func nextAction(_ sender: UIButton) {
        survey.allergyId = allergy_id
        survey.allergyIngOther = otherTextBox.textField.text
        let vc = DietViewController.instantiate(fromAppStoryboard: .User)
        vc.survey = self.survey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func configurePickerView(){
        self.allergyPickerView.delegate = self
        self.allergiesTextBox.textField.inputView = allergyPickerView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    
    func configureNavigationBar(){
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "Allergies".localized
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationItem.rightBarButtonItems = configure()
    }
    
    func configure() -> [UIBarButtonItem] {
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        let b = UIBarButtonItem(title: "SKIP".localized, style: .plain, target: self, action: #selector(self.skip))
        b.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        return [b]
    }
    
    @objc func skip(){
        let vc = DietViewController.instantiate(fromAppStoryboard: .User)
        //vc.isFromSetting = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func configureTextFields(){
        self.allergiesTextBox.setTextFieldPlaceHolder(string: "Your Allergies (If any)".localized, labelTop : "PickYourType".localized)
        self.ingredientsTextBox.setTextFieldPlaceHolder(string: "Ingredients".localized, labelTop : "Allegries from ingredients (if any)".localized)
        self.otherTextBox.setTextFieldPlaceHolder(string: "Others",labelTop: "Others")
        self.allergiesTextBox.textField.delegate = self
        self.ingredientsTextBox.textField.delegate = self
    }
    
    func serviceCallAllergy(){
        
        let parameter : [String:String] = ["type": "Allergy","language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"]
        
        NetworkRequest.shared.allergiesOrDiet(parameters: parameter) { (response, error) in
            
            if error != nil {
                return
            }
            
            let types = JSON(response).arrayValue.map({ AllergiesOrDietModel.init(fromJson: $0) })
            self.allergyData = types
            
            DispatchQueue.main.async {
                self.allergyPickerView.reloadAllComponents()
            }
        }
    }
    
    func bindData() {
        allergy_id = survey.allergyId
        allergiesTextBox.textField.text = survey.allergy?.allergyName
        otherTextBox.textField.text = survey.allergyIngOther
        displayIngredients()
    }
    
    func displayIngredients() {
        var ingredients = ""
        survey.allergyIngredients.forEach { ing in
            ingredients += "\(ing.ingName!), "
        }
        ingredientsTextBox.textField.text = String(ingredients.dropLast(2))
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == ingredientsTextBox.textField {
            let vc = IngredientsSelectionViewController.instantiate(fromAppStoryboard: .User)
            vc.selectedList = survey.allergyIngredients
            vc.ingredientsDelegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension AllegryViewController: UIPickerViewDelegate, UIPickerViewDataSource  {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == allergyPickerView {
            return allergyData.count
        }else {
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == allergyPickerView {
            return allergyData[row].names
        } else {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == allergyPickerView {
            allergiesTextBox.textField.text = allergyData[row].names
            allergy_id = allergyData[row].id
        }
        self.view.endEditing(true)
    }
    
}

extension AllegryViewController : IngredientsSelectedDelegate {
    func onIngredientsSelected(ingredients: [Ingredient]) {
        otherTextBox.textField.becomeFirstResponder()
        survey.allergyIngredients = ingredients
        displayIngredients()
    }
}
