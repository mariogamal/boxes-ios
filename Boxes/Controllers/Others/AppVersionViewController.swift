//
//  AppVersionViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/5/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class AppVersionViewController: BaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var title2Label: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func actionClose(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
