//
//  OrderHistoryTabsViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/5/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import CarbonKit

class OrderHistoryTabsViewController: BaseViewController, CarbonTabSwipeNavigationDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let items = ["FOOD".localized]
        // self.getCategories()
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(self.view.frame.width, forSegmentAt: 0)
        //carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(self.view.frame.width/2, forSegmentAt: 1)
        
        let fontForCategories =  UIFont.init(name: AppFont.Oswald_SemiBold,
                                             size: 12) ?? UIFont.systemFont(ofSize: 10.0)
        
        carbonTabSwipeNavigation.setNormalColor(THEME_COLOR_BLACK_HEX,font: fontForCategories)
        carbonTabSwipeNavigation.setSelectedColor(THEME_COLOR_DARK_GREEN ,font: fontForCategories)
        carbonTabSwipeNavigation.setIndicatorColor(THEME_COLOR_DARK_GREEN)
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        // return viewController at index
        
        
        if index == 0{
            let vc = FoodOrderHistoryViewController.instantiate(fromAppStoryboard: .User)
            return vc
        }else {
            let vc = ECommerceOrderHistoryViewController.instantiate(fromAppStoryboard: .User)
            return vc
        }
        
    }
    
    
}
