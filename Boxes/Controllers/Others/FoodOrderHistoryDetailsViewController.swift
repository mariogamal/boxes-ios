//
//  FoodOrderHistoryViewController.swift
//  Boxes
//
//  Created by macmin on 26/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import FSCalendar
import SwiftyJSON

class FoodOrderHistoryDetailsViewController: BaseViewController {

    @IBOutlet weak var customCalendarView: CustomCardView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var restaurantTitleLabel: UILabel!
    
    var selectedDate : Date?
    var item : MealsHistoryModel?
    var colors : [String : (UIColor,Bool,PackageType,String)] = [:]
    var menu : [String : DeliveryDateDetailsModel] = [:]
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "E, MMM dd, yyyy"
        return formatter
    }()
    
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendar, action: #selector(self.calendar.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
        }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Calendar Details".localized
        calendar.dataSource = self
        calendar.delegate = self
        calendar.select(self.selectedDate)
        calendar.scope = .month
        calendar.today = nil
        calendar.appearance.caseOptions = [.headerUsesUpperCase,.weekdayUsesSingleUpperCase]
        calendar.appearance.headerTitleFont = UIFont(name: AppFont.Oswald_Regular, size: 18) ?? UIFont.systemFont(ofSize: 18)
        calendar.appearance.headerTitleColor = UIColor.darkText
        calendar.appearance.weekdayFont = UIFont(name: AppFont.PTSans_Regular, size: 14) ?? UIFont.systemFont(ofSize: 18)
        calendar.appearance.weekdayTextColor = UIColor.lightGray
        calendar.calendarHeaderView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        calendar.calendarWeekdayView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        calendar.today = nil
        self.view.addGestureRecognizer(scopeGesture)
        self.tableView.panGestureRecognizer.require(toFail: scopeGesture)
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: FoodHistioryDetailsTableViewCell.identifier, bundle: .main), forCellReuseIdentifier: FoodHistioryDetailsTableViewCell.identifier)
        
        self.menuServiceCall()
        
        if let date = self.selectedDate {self.dateLabel.text = self.dateFormatter2.string(from: date)}
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        calendar.reloadData()
    }
    
    func toggle() {
        if self.calendar.scope == .month {
            self.calendar.setScope(.week, animated: true)
        } else {
            self.calendar.setScope(.month, animated: true)
        }
    }
    
    func menuServiceCall() {
        guard let id = self.item?.id else{
            return
        }
        let parameter : [String:String] = [
                         "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
                         "subscription_id" : id
        ]
        NetworkRequest.shared.deliveryDatesBySubscription(parameters: parameter) { (response, error) in

            if error != nil {
                return
            }
            let data = JSON(response).arrayValue.map({ MealOrderHistoryModel.init(fromJson: $0) })
            self.setupData(data: data)
        }
    }
    
    func mealDetailsServiceCall(date: Date) {
        let key = self.dateFormatter.string(from: date)
        guard let id = self.colors[key]?.3 else{
            Messages.showErrorMessage(message: "Date not available".localized)
            return
        }
        let parameter : [String:String] = [
                         "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
                         "meal_order_detail_id" : id
        ]
        NetworkRequest.shared.deliveryDateDetails(parameters: parameter) { (response, error) in
            if error != nil {
                self.restaurantTitleLabel.text = "No meals, please swipe down".localized
                self.menu[key] = nil
                self.tableView.reloadData()
                return
            }
            let data = DeliveryDateDetailsModel(fromJson: JSON(response))
            self.menu[key] = data
            self.tableView.reloadData()
        }
    }
    
    func setupData(data: [MealOrderHistoryModel]){
        data.forEach { (model) in
            var color : UIColor = .yellow
            var packageType : PackageType = .booked
            if model.status == "pending" {
                color = AppColor.primary
                packageType = .booked
            }else if model.status == "pause" {
                color = AppColor.pause
                packageType = .paused
            }else{
                color = AppColor.deliver
                packageType = .delivered
            }
            self.colors[model.deliveryDate] = (color, true, packageType, model.mealOrderDetailId)
        }
        if let key = data.first?.deliveryDate, let date = self.dateFormatter.date(from: key) {
//            self.selectDate(date: date)
            self.calendar.select(date)
        }
        self.calendar.reloadData()
    }
    
    func selectDate(date: Date){
        self.dateLabel.text = self.dateFormatter2.string(from: date)
        self.selectedDate = date
        let key = self.dateFormatter.string(from: date)
        if self.menu[key] != nil {
            self.tableView.reloadData()
        }else{
            self.mealDetailsServiceCall(date: date)
        }
        self.toggle()
    }
    
}

extension FoodOrderHistoryDetailsViewController  : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let date = self.selectedDate else { return 0 }
        let key = self.dateFormatter.string(from: date)
        guard let meal = self.menu[key] else { return 0 }
        let menus = meal.menu ?? []
        self.restaurantTitleLabel.text = meal.listingName ?? "N/A".localized
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        guard let date = self.selectedDate else { return 0 }
//        let key = self.dateFormatter.string(from: date)
//        guard let meal = self.menu[key] else { return 0 }
//        let menus = meal.menu ?? []
//        return menus[section].product
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var item : DeliveryProductModel?
        if let date = self.selectedDate {
            let key = self.dateFormatter.string(from: date)
            if let meal = self.menu[key] {
                let menus = meal.menu ?? []
                item = menus[indexPath.section].product
            }
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: FoodHistioryDetailsTableViewCell.identifier, for: indexPath) as! FoodHistioryDetailsTableViewCell
        cell.titleLabel.text = item?.name ?? "N/A".localized
        cell.subTitleLabel.text = item?.caption ?? "N/A".localized
        cell.imgView.imageUrl = BASEIMAGEURL + (item?.image ?? "N/A".localized)
        cell.caloriesLabel.text = "\(item?.calories ?? 0)"
        cell.carbsLabel.text = "\(item?.carbohydrates ?? 0)"
        cell.proteinLabel.text = "\(item?.proteins ?? 0)"
        cell.ratingLabel.text = item?.rating ?? "N/A".localized
        if let id = item?.id {
            cell.addReveiwAction = {
                DispatchQueue.main.async {
                    let vc = RatingsViewController.instantiate(fromAppStoryboard: .User)
                    vc.productId = id
                    vc.productTitle = item?.name ?? ""
                    vc.productImage = item?.image ?? ""
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
        return cell
    }
    
}

extension FoodOrderHistoryDetailsViewController  : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let date = self.selectedDate else { return nil }
        let key = self.dateFormatter.string(from: date)
        guard let meal = self.menu[key] else { return nil }
        let menus = meal.menu ?? []
        return menus[section].categoryName
    }
    
}

extension FoodOrderHistoryDetailsViewController  : UIGestureRecognizerDelegate {
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let shouldBegin = self.tableView.contentOffset.y <= -self.tableView.contentInset.top
        if shouldBegin {
            let velocity = self.scopeGesture.velocity(in: self.view)
            switch self.calendar.scope {
            case .month:
                return velocity.y < 0
            default:
                return velocity.y > 0
            }
        }
        return shouldBegin
    }
    
}


extension FoodOrderHistoryDetailsViewController  : FSCalendarDataSource {
    
}


extension FoodOrderHistoryDetailsViewController  : FSCalendarDelegate {
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstraint.constant = bounds.height
        self.dateView.alpha = (self.calendar.scope == .month) ? 0.0 : 1.0
        self.titleLabel.isHidden = (self.calendar.scope == .month) ? false : true
        self.customCalendarView.isHidden = (self.calendar.scope == .month) ? false : true
        self.calendar.isHidden = (self.calendar.scope == .month) ? false : true
        self.view.layoutIfNeeded()
    }
    
    
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
        
        if self.colors[self.dateFormatter.string(from: date)] != nil {
            self.selectDate(date: date)
        }else{
            Messages.showErrorMessage(message: "You can not select any other date ".localized)
            calendar.deselect(date)
            calendar.select(self.selectedDate)
        }
        
        
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
//        print("\(self.dateFormatter.string(from: calendar.currentPage))")
    }
    
}


extension FoodOrderHistoryDetailsViewController  : FSCalendarDelegateAppearance {
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
        return UIColor.orange
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        let key = self.dateFormatter.string(from: date)
        if self.colors[key] != nil {
            return appearance.titleSelectionColor
        }
        return nil
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleSelectionColorFor date: Date) -> UIColor? {
        return appearance.titleSelectionColor
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        let key = self.dateFormatter.string(from: date)
        if let color = self.colors[key] {
            return color.0
        }
        return nil
    }
    
}
