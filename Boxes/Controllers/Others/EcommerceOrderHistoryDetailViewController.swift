//
//  EcommerceOrderHistoryDetailViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/5/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import SwiftyJSON

class EcommerceOrderHistoryDetailViewController: BaseViewController {

    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var deliveryFeeLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    
    
    var data: [OrderDetailModel] = []
    var total: TotalModel?
    var orderID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Order history detail".localized.uppercased()
        configureTableView()
        serviceCall()
    }
    
    func serviceCall() {

        let parameter : [String:Any] = [
            "order_id" : orderID,
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"
        ]

        NetworkRequest.shared.myOrderDetails(parameters: parameter) { (response, error) in
            if let err = error {
                Messages.showErrorMessage(message: err.message)
                return
            }

            DispatchQueue.main.async {
                let item = MyOrderDetailsModel.init(fromJson: JSON(response))
                self.data = item.orderDetails
                self.setupValues(data: item.total)
                self.tableView.reloadData()
            }
        }
    }
    
    func setupValues(data: TotalModel){
        subTotalLabel.text = "\(data.subTotal ?? 0)"
        deliveryFeeLabel.text = "\(data.deliveryCharges ?? 0)"
        discountLabel.text = "\(data.totalDiscount ?? 0)"
        totalLabel.text = "\(data.totalPrice ?? 0)"
    }
    
    func configureTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: OrderHistoryDetailCell1.identifier, bundle: nil), forCellReuseIdentifier: OrderHistoryDetailCell1.identifier)
    }

}

extension EcommerceOrderHistoryDetailViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OrderHistoryDetailCell1.identifier) as! OrderHistoryDetailCell1
        cell.configure(with: data[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}


