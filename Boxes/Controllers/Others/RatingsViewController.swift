//
//  RatingsViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/5/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Cosmos
import SwiftyJSON

class RatingsViewController: BaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var ratingsView: CosmosView!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var submitButton: CustomButton!
    
    var productId : String?
    var productTitle : String = ""
    var productImage : String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserRate()
        self.itemImageView.imageUrl = BASEIMAGEURL + self.productImage
        self.titleLabel.text = self.productTitle
    }
    
    func getUserRate() {
        guard let id = self.productId else{
            Messages.showErrorMessage(message: "Id not available".localized)
            return
        }
        
        let parameter : [String:Any] = ["product_id" : id]
        
        NetworkRequest.shared.rateDetails(parameters: parameter) { (response, error) in
            if error != nil {
                self.ratingsView.rating = 0.0
                return
            }
            let rating = Rating(fromJson: JSON(response))
            self.ratingsView.rating = rating.rate
        }
    }
    
    func updateUserRate() {
        
        guard let id = self.productId else{
            Messages.showErrorMessage(message: "Id not available".localized)
            return
        }
        let parameter : [String:Any] = [
                         "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
                         "product_id" : id,
                         "rate": self.ratingsView.rating
        ]
        NetworkRequest.shared.rateMeal(parameters: parameter) { (response, error) in
            self.submitButton.stopLoading()
            if error != nil {
                Messages.showSuccessMessage(message: error?.message ?? "Not Available".localized)
                self.submitButton.shake()
                return
            }
            Messages.showSuccessMessage(message: "Successfully rate the meal".localized)
            self.dismiss(animated: true, completion: nil)
        }
    }
    

    @IBAction func actionSubmit(_ sender: CustomButton) {
        if !sender.isLoading() {
            sender.showLoading()
            self.updateUserRate()
        }
    }
    
    @IBAction func actionCross(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
