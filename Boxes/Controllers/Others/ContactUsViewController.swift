//
//  ContactUsViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/5/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import CountryPickerView
import SwiftyJSON

class ContactUsViewController: BaseViewController {
    
    
    @IBOutlet weak var firstNameView: UIView!
    @IBOutlet weak var lastNameView: UIView!
    @IBOutlet weak var firstNameTextfield: UITextField!
    @IBOutlet weak var lastNameTextfield: UITextField!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTextfield: UITextField!
    
    @IBOutlet weak var contactNoView: UIView!
    @IBOutlet weak var phoneNumberTextfield: UITextField!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var messageView: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.configureUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureNavigationBar()
    }
    
    func configureNavigationBar(){
        
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "contact us".localized.uppercased()
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
        // self.placeBackButton(selectorBack: #selector(self.back))
        
    }
    
    
    func configureUI() {
        
        if let user = UserManager.getUserObjectFromUserDefaults() {
            self.phoneNumberTextfield.text = user.phone
        }
        
        firstNameTextfield.delegate = self
        lastNameTextfield.delegate = self
        emailTextfield.delegate = self
        phoneNumberTextfield.delegate = self
        messageTextView.delegate = self
        phoneNumberTextfield.isUserInteractionEnabled = false
          
    }

    
    
    @IBAction func actionSubmit(_ sender: UIButton) {
   
        guard isValidate() else{
            return
        }
    }
    
    func isValidate() -> Bool {
        
        guard let firstName = self.firstNameTextfield.text, !firstName.isEmpty else{
            Messages.showErrorMessage(message: "Write your first name".localized)
            return false
        }
        
        guard let lastName = self.lastNameTextfield.text, !lastName.isEmpty else{
            Messages.showErrorMessage(message: "Write your last name".localized)
            return false
        }
        
        guard let email = self.emailTextfield.text, !email.isEmpty else{
            Messages.showErrorMessage(message: "Write your email".localized)
            return false
        }
        
        guard let phone = self.phoneNumberTextfield.text, !email.isEmpty else{
            Messages.showErrorMessage(message: "Write your phone no".localized)
            return false
        }
        
        guard let message = self.messageTextView.text, !email.isEmpty else{
            Messages.showErrorMessage(message: "Write your message".localized)
            return false
        }
        
        serviceCall(firstName: firstName, lastName: lastName, email: email, phone: phone, message: message)
        return true
    }
    
    func serviceCall(firstName: String, lastName: String, email: String, phone: String, message: String) {

        let parameter : [String:Any] = [
            "first_name": firstName,
            "last_name": lastName,
            "email": email,
            "phone": phone,
            "message": message
        ]

        NetworkRequest.shared.submitQuery(parameters: parameter) { (response, error) in
            if error != nil {
                Messages.showErrorMessage(message: error?.message)
                return
            }
            
            DispatchQueue.main.async {
                Messages.showSuccessMessage(message: "Your query has been submitted!".localized)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
}

extension ContactUsViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == firstNameTextfield {
            
            self.lastNameTextfield.becomeFirstResponder()
            
        }else if textField == lastNameTextfield {
            
            self.emailTextfield.becomeFirstResponder()
            
        }else if textField == emailTextfield {
            
            self.phoneNumberTextfield.becomeFirstResponder()
            
        }else {
            
            self.messageTextView.becomeFirstResponder()
        }
        
        return true
    }
}

extension ContactUsViewController: UITextViewDelegate {
    
    
}
