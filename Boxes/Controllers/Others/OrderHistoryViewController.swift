//
//  OrderHistoryViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/5/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class OrderHistoryViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Order History".localized.uppercased()
    }

    override func viewWillAppear(_ animated: Bool) {
        configureNavigationBar()
    }
    
    func configureNavigationBar(){
        
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "order history".localized.uppercased()
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
        // self.placeBackButton(selectorBack: #selector(self.back))
        
    }

    
    
}
