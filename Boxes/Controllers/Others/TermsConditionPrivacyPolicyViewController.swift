//
//  TermsConditionPrivacyPolicyViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/5/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import SwiftyJSON

enum PrivacyType {
    case terms
    case privacy
    func getTitle() -> String {
        switch self {
        case .terms:
            return "terms_and_conditions"
        default:
            return "privacy_policy"
        }
    }
    func getTitleHeading() -> String {
        switch self {
        case .terms:
            return "TERMS & CONDITIONS".localized
        default:
            return "PRIVACY POLICY".localized
        }
    }
}

class TermsConditionPrivacyPolicyViewController: BaseViewController {

    @IBOutlet weak var textView: UITextView!
    
    var type : PrivacyType = .terms
    
    override func viewDidLoad() {
        super.viewDidLoad()
        serviceCall()
        // Do any additional setup after loading the view.

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.configureNavigationBar()
    }
    
    func configureNavigationBar(){
        
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = self.type.getTitleHeading()
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
        // self.placeBackButton(selectorBack: #selector(self.back))
        
    }
    
    func serviceCall() {

        var parameter : [String:Any] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"
        ]
    
        parameter["page_name"] = self.type.getTitle()

        NetworkRequest.shared.page(parameters: parameter) { (response, error) in
            if let err = error {
                Messages.showErrorMessage(message: err.message)
                return
            }
                
            DispatchQueue.main.async {
                let data = JSON(response).arrayValue.map({ TermsConditionModel(fromJson: $0) })
                self.textView.text = data.first?.pageDesc ?? ""
            }
        }
    }
    
    
    
    
}
