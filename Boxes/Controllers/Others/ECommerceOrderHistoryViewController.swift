//
//  ECommerceOrderHistoryViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/5/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import SwiftyJSON

class ECommerceOrderHistoryViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var data: [MyOrdersModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Order History".localized.uppercased()
        configureTableView()
        serviceCall()
    }
    
    func serviceCall() {
        
        let parameter : [String:Any] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"
        ]
        
        NetworkRequest.shared.myOrders(parameters: parameter) { (response, error) in
            if let err = error {
                Messages.showErrorMessage(message: err.message)
                return
            }
            
            DispatchQueue.main.async {
                self.data = JSON(response).arrayValue.map({ MyOrdersModel.init(fromJson: $0) })
                self.tableView.reloadData()
            }
        }
    }
    
    func configureTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        //self.tableView.tableFooterView = UIView()
        self.tableView.register(UINib(nibName: "OrderHistoryCell", bundle: nil), forCellReuseIdentifier: "OrderHistoryCell")
    }

}

extension ECommerceOrderHistoryViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderHistoryCell") as! OrderHistoryCell
        cell.configure(with: data[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = EcommerceOrderHistoryDetailViewController.instantiate(fromAppStoryboard: .User)
        vc.orderID = data[indexPath.row].id
        self.navigationController?.pushViewController(vc, animated: true)

       
    }
}
