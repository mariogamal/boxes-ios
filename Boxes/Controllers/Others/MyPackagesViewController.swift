//
//  MyPackagesViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/5/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import SwiftyJSON

class MyPackagesViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var data: [MyPackagesModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureTableView()
        serviceCall()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureNavigationBar()
    }
    
    func configureNavigationBar(){
        
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "my packages".localized.uppercased()
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
        // self.placeBackButton(selectorBack: #selector(self.back))
        
    }
    
    func serviceCall() {

        let parameter : [String:Any] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"
        ]

        NetworkRequest.shared.myPackages(parameters: parameter) { (response, error) in
            if let err = error {
                Messages.showErrorMessage(message: err.message)
                return
            }
            
            
            DispatchQueue.main.async {
                self.data = JSON(response).arrayValue.map({ MyPackagesModel.init(fromJson: $0) })
                self.tableView.reloadData()
            }
        }
    }
    
    func configureTableView() {
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        //self.tableView.tableFooterView = UIView()
        self.tableView.register(UINib(nibName: "MyPakegesTableViewCell", bundle: nil), forCellReuseIdentifier: "MyPakegesTableViewCell")
    }
    
    
}

extension MyPackagesViewController : UITableViewDelegate, UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyPakegesTableViewCell") as! MyPakegesTableViewCell
        cell.configure(with: data[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let vc = RatingsViewController.instantiate(fromAppStoryboard: .User)
//        present(vc, animated: true, completion: nil)

    }
}

