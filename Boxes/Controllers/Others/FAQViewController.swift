//
//  FAQViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/5/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import ExpandableCell
import SwiftyJSON

class FAQViewController: BaseViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var tableView: ExpandableTableView!
    
//    var cell: UITableViewCell {
//        return tableView.dequeueReusableCell(withIdentifier: FAQTableViewCell.ID)!
//    }
//
    var data : [FAQModel] = []
    
    
    var parentCells: [[String]] = [
        [ FAQTableViewCell.ID ,
        FAQTableViewCell.ID ,
        FAQTableViewCell.ID ]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableView.expandableDelegate = self
        tableView.animation = .automatic
        tableView.expansionStyle = .single
        
        tableView.register(UINib(nibName: "FAQTableViewCell", bundle: nil), forCellReuseIdentifier: FAQTableViewCell.ID)
        tableView.register(UINib(nibName: "FAQNormalTableViewCell", bundle: nil), forCellReuseIdentifier: FAQNormalTableViewCell.ID)
        
        serviceCall()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureNavigationBar()
    }
    
    func configureNavigationBar(){
        
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "FAQ's".localized.uppercased()
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
        // self.placeBackButton(selectorBack: #selector(self.back))
        
    }
    
    func serviceCall() {

        let parameter : [String:Any] = [
            "page_name": "faq",
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"
        ]

        NetworkRequest.shared.page(parameters: parameter) { (response, error) in
            if let err = error {
                Messages.showErrorMessage(message: err.message)
                return
            }
            
            
            DispatchQueue.main.async {
                self.data = JSON(response).arrayValue.map({ FAQModel.init(fromJson: $0) })
                self.tableView.reloadData()
            }
        }
    }

}

extension FAQViewController : ExpandableDelegate {
    
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCellsForRowAt indexPath: IndexPath) -> [UITableViewCell]? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: FAQNormalTableViewCell.ID) as! FAQNormalTableViewCell
        cell.titleLabel.text = self.data[indexPath.row].answerDesc
        return [cell]
        
    }
    
    func numberOfSections(in tableView: ExpandableTableView) -> Int {
        return self.data.count
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = expandableTableView.dequeueReusableCell(withIdentifier: FAQTableViewCell.ID) as? FAQTableViewCell else { return UITableViewCell() } //outer cell
        cell.titleLabel.text = self.data[indexPath.section].questionDesc
        cell.rightMargin = 32
        return cell
        
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightsForExpandedRowAt indexPath: IndexPath) -> [CGFloat]? {
        
        return [UITableView.automaticDimension]
        
    }
    
    
    @objc(expandableTableView:didCloseRowAt:) func expandableTableView(_ expandableTableView: UITableView, didCloseRowAt indexPath: IndexPath) {
        let cell = expandableTableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
        cell?.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
    }
    
    func expandableTableView(_ expandableTableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func expandableTableView(_ expandableTableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        //        let cell = expandableTableView.cellForRow(at: indexPath)
        //        cell?.contentView.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        //        cell?.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
    }
}
