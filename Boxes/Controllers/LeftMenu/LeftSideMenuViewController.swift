//
//  LeftSideMenuViewController.swift
//  Boxes
//
//  Created by MacAir on 03/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import MOLH
import CustomerlySDK
class LeftSideMenuViewController: BaseViewController {
    
    @IBOutlet weak var logout: UIButton!
    @IBOutlet weak var login: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        print(self.navigationDrawerController)
        self.setValues()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setValues()
    }
    
    func setValues(){
        if UserManager.isUserLogin() {
            if let user = UserManager.getUserObjectFromUserDefaults() {
                if user.image?.isEmpty ?? true {
                    self.profileImageView.image = #imageLiteral(resourceName: "placeholder")
                }else{
                    self.profileImageView.imageUrl = BASEIMAGEURL + user.image
                }
                
                self.profileNameLabel.text = user.firstName + " " + user.lastName
                self.login.isHidden = true
            }
        }else{
            self.profileImageView.image = UIImage(named: "placeholder")
            self.profileNameLabel.text = "Guest User".localized
            self.logout.isHidden = true
        }
    }

    @IBAction func profileButtonAction(_ sender: Any) {
//        let vc = CreateProfileViewController.instantiate(fromAppStoryboard: .User)
//        vc.isFromProfile = true
//        self.changeVC(vc)
    }
    
    @IBAction func oderHistoryButtonAction(_ sender: UIButton) {
        if UserManager.isUserLogin() {
            self.changeVC(OrderHistoryViewController.instantiate(fromAppStoryboard: .User))
            
        }else {
            moveToLoginVC()
        }
        
    }
    
    @IBAction func myPackagesButtonAction(_ sender: UIButton) {
        if UserManager.isUserLogin() {
            self.changeVC(AddressViewController.instantiate(fromAppStoryboard: .User))
        }else {
            moveToLoginVC()
        }
    }
    
    @IBAction func contactUsButtonAction(_ sender: UIButton) {
        self.changeVC(ContactUsViewController.instantiate(fromAppStoryboard: .User))
    }
    
    @IBAction func FaqsButtonAction(_ sender: UIButton) {
        self.changeVC(FAQViewController.instantiate(fromAppStoryboard: .User))
    }
    
    @IBAction func TermsButtonAction(_ sender: UIButton) {
        self.changeVC(TermsConditionPrivacyPolicyViewController.instantiate(fromAppStoryboard: .User))
    }
    
    @IBAction func PrivacyButtonAction(_ sender: UIButton) {
        let vc = TermsConditionPrivacyPolicyViewController.instantiate(fromAppStoryboard: .User)
        vc.type = .privacy
        self.changeVC(vc)
    }
    
    @IBAction func SettingsButtonAction(_ sender: Button) {
        if UserManager.isUserLogin() {
            self.changeVC(SettingsViewController.instantiate(fromAppStoryboard: .User))
        }else {
            moveToLoginVC()
        }
    }
    
    @IBAction func SupportButtonAction(_ sender: Button) {
        if UserManager.isUserLogin() {
            let user = UserManager.getUserObjectFromUserDefaults()
            if let email = user?.email {
                Customerly.sharedInstance.registerUser(email: email ,user_id: user?.id, name: user?.firstName)
            }
        }
        Customerly.sharedInstance.openSupport(from: self)
    }
    
    @IBAction func logoutButtonAction(_ sender: UIButton) {

        let alert = UIAlertController(title: "Do you want to logout?".localized, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes".localized, style: .default, handler: { (button) in
            self.serviceCall()
            Customerly.sharedInstance.logoutUser()
        }))
        alert.addAction(UIAlertAction(title: "No".localized, style: .cancel, handler: { (button) in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func loginButtonAction(_ sender: UIButton) {
        moveToLoginVC()
    }
    
    func serviceCall() {
        
        NetworkRequest.shared.logout(parameters: [:]) { (response, error) in
            
            if let err = error {
                Messages.showErrorMessage(message: err.message)
                return
            }
            
            DispatchQueue.main.async {
                CartManager.shared.data.removeAll()
//                CartManager.shared.cartItems.removeAll()
                UserManager.removeUserObjectFromUserDefaults()
                let vc  = SplashViewController.instantiate(fromAppStoryboard: .Main)
                (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController = vc
            }
        }
    }
    
    func moveToLoginVC() {
        let vc = BaseNavigationViewController()
        vc.viewControllers = [LoginViewController.instantiate(fromAppStoryboard: .User)]
        UIWINDOW?.rootViewController = vc
    }
    
    func changeVC (_ vc : UIViewController){
//        self.navigationDrawerController?.hideLeftViewAnimated(self)
        if MOLHLanguage.currentAppleLanguage() == "en"{
            self.hideLeftViewAnimated(true)
        }
        else{
            self.hideRightViewAnimated(true)
        }
        self.hideLeftViewAnimated(true)
        if let nav = self.sideMenuController?.rootViewController as? UINavigationController {
            nav.pushViewController(vc, animated: false)
        }
    }
    
//        func changeVC (_ vc : UIViewController){
//            self.navigationDrawerController?.hideLeftViewAnimated(self)
//            if let nav = self.navigationDrawerController?.rootViewController as? UINavigationController {
//                nav.pushViewController(vc, animated: false)
//            }
//        }

    
}
