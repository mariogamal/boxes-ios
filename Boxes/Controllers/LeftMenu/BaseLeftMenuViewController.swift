//
//  BaseLeftMenuViewController.swift
//  Boxes
//
//  Created by MacAir on 03/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import LGSideMenuController
import MOLH
class BaseLeftMenuViewController: LGSideMenuController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if MOLHLanguage.currentAppleLanguage() == "en" {
            self.leftViewSwipeGestureRange = LGSideMenuSwipeGestureRangeMake(8.0, 8.0)
            self.leftViewController = LeftSideMenuViewController.instantiate(fromAppStoryboard: .LeftMenu)
            }
        else{
            self.rightViewSwipeGestureRange = LGSideMenuSwipeGestureRangeMake(8.0, 8.0)
            self.rightViewController = LeftSideMenuViewController.instantiate(fromAppStoryboard:.RightMenu)
        }
              // Do any additional setup after loading the view.
      /*  self.leftViewSwipeGestureRange = LGSideMenuSwipeGestureRangeMake(8.0, 8.0)
        self.leftViewController = LeftSideMenuViewController.instantiate(fromAppStoryboard: .LeftMenu)
        */
        let root = BaseNavigationViewController()
        root.viewControllers = [ BaseTabBarViewController() ]
        self.rootViewController = root
        
    }
   
    
}
