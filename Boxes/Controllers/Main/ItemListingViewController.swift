//
//  ItemListingViewController.swift
//  Boxes
//
//  Created by macmin on 15/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ItemListingViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var item : ListingProductsModel?
    private var data : [ProductModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: ItemTableViewCell.identifier, bundle: .main), forCellReuseIdentifier: ItemTableViewCell.identifier)
        
        self.data = self.item?.products ?? []
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addBadgeRightBarButtonWithCartImage(itemvalue: "\(CartManager.shared.cartItemCount)")
    }

}

extension ItemListingViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.data[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: ItemTableViewCell.identifier, for: indexPath) as! ItemTableViewCell
        cell.imgView.imageUrl = BASEIMAGEURL + item.image
        cell.titleLabel.text = item.productName
        cell.subTitleLabel.text = item.captionName
        return cell
    }
}

extension ItemListingViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ItemDetailsViewController.instantiate(fromAppStoryboard: .Main)
        vc.item = self.data[indexPath.row]
        self.showVC(vc)
    }
    
}
