//
//  SplashViewController.swift
//  Boxes
//
//  Created by MacAir on 03/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import SwiftyJSON

class SplashViewController: BaseViewController {
    
    public static var VCIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigateToInitalVC()
    }

    func navigateToInitalVC() {

        if UserManager.isUserLogin() {
            self.serviceMyCurrentPackage()
        } else {
            let vc = BaseNavigationViewController()
            vc.viewControllers = [LanguageViewController.instantiate(fromAppStoryboard: .User)]
            UIWINDOW?.rootViewController = vc
        }
    }
    
    func serviceMyCurrentPackage(){
        
        let parameter : [String:String] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"
        ]
        
        NetworkRequest.shared.myCurrentPackage(parameters: parameter) { (response, error) in
            
            if error != nil {
                print(error?.message ?? "error")
                self.openTabController()
                return
            }
            let data = MyCurrentPackageModel(fromJson: JSON(response))
            if data.id != nil {
                SplashViewController.VCIndex = 1
            }
            self.openTabController()
        }
    }
    
    func openTabController() {
        CartManager.shared.getCartItems()
        let vc = BaseLeftMenuViewController.instantiate(fromAppStoryboard: .LeftMenu)
        vc.leftViewPresentationStyle = .slideBelow
        UIWINDOW?.rootViewController = vc
    }
}
