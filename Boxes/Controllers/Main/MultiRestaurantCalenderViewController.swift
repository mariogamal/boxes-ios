//
//  MultiRestaurantCalenderViewController.swift
//  Boxes
//
//  Created by macmin on 18/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import FSCalendar
import SwiftyJSON

enum PackageType {
    case delivered
    case paused
    case booked
}

class MultiRestaurantCalenderViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var calendar: FSCalendar!
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = Locale(identifier: "en_US")
        return formatter
    }()
    
    var colors : [String : (UIColor,Bool,PackageType,String)] = [:]
    var data : NewCalenderResponse?
    var selectedVendor : CalenderDatailModel?
    
    var serviceCallAction : (() -> Void)?
    var detailsVC: MultiRestaurantCalenderDetailsViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.serviceCall()
        self.setupCollectionView()
        self.setupCalendar()
        calendar.locale = Locale(identifier: "en_US")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.configureNavigationBar()
    }
    
    func configureNavigationBar(){
        
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "Calender".localized.uppercased()
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
        // self.placeBackButton(selectorBack: #selector(self.back))
        
    }
    
    func setupCollectionView(){
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: CalendarCollectionViewCell.identifier, bundle: .main), forCellWithReuseIdentifier: CalendarCollectionViewCell.identifier)
    }
    
    func setupCalendar(){
        calendar.dataSource = self
        calendar.delegate = self
        calendar.scope = .month
        calendar.swipeToChooseGesture.isEnabled = true
        calendar.appearance.caseOptions = [.headerUsesUpperCase,.weekdayUsesSingleUpperCase]
        calendar.appearance.headerTitleFont = UIFont(name: AppFont.Oswald_Regular, size: 18) ?? UIFont.systemFont(ofSize: 18)
        calendar.appearance.headerTitleColor = UIColor.darkText
        calendar.appearance.weekdayFont = UIFont(name: AppFont.PTSans_Regular, size: 14) ?? UIFont.systemFont(ofSize: 18)
        calendar.appearance.weekdayTextColor = UIColor.lightGray
        calendar.calendarHeaderView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        calendar.calendarWeekdayView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        calendar.today = nil
    }
    
    func serviceCall() {
        
        let parameter : [String:String] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
        ]
        
        NetworkRequest.shared.calendarDetail(parameters: parameter) { (response, error) in
            if error != nil {
                return
            }
            
            self.data = NewCalenderResponse.init(fromJson: JSON(response))
            self.collectionView.reloadData()
            
            if self.selectedVendor == nil {
                self.selectedVendor = self.data?.vendors.first
                self.collectionView.reloadData()
                self.collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .top)
            } else {
                if let selectedDat = self.selectedVendor {
                    for i in 0..<self.data!.vendors.count{
                        if selectedDat.id == self.data!.vendors[i].id {
                            self.selectedVendor = self.data!.vendors[i]
                            self.collectionView.reloadData()
                            self.collectionView.selectItem(at: IndexPath(item: i, section: 0), animated: true, scrollPosition: .top)
                        }
                    }
                }
            }
            self.setupData()
            self.calendar.reloadData()
            self.detailsVC?.data = self.data
            NotificationCenter.default.post(name: Notification.Name.init("serviceLoaded"), object: nil)
        }
    }
    
    func setupData(){
        self.colors.removeAll()
        self.data?.deliveryDates.forEach { (model2) in
            var color : UIColor = .yellow
            var packageType : PackageType = .booked
            if model2.status == "pending" {
                color = AppColor.primary
                packageType = .booked
            }else if model2.status == "pause" {
                color = AppColor.pause
                packageType = .paused
            }else{
                color = AppColor.deliver
                packageType = .delivered
            }
            let isSelected = !model2.userMeals.isEmpty
            self.colors[model2.deliveryDate] = (isSelected || model2.status != "pending" ? color : color.withAlphaComponent(0.4), isSelected, packageType, model2.orderDetailId)
        }
    }
    
}

extension MultiRestaurantCalenderViewController  : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data?.vendors.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CalendarCollectionViewCell.identifier, for: indexPath) as! CalendarCollectionViewCell
        
        if let item = self.data?.vendors[indexPath.row] {
            cell.setup(title: item.listingName, subTitle: "\(item.used ?? 0)/\(item.subscription ?? 0)", image: item.image)
            cell.createModels(selected: item.used, total: item.subscription)
            cell.setSelected(item == self.selectedVendor)
        }
        return cell
    }
}

extension MultiRestaurantCalenderViewController  : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? CalendarCollectionViewCell else { return }
        if let item = self.data?.vendors[indexPath.row] {
            cell.setSelected(true)
            self.selectedVendor = item
            self.setupData()
            self.calendar.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? CalendarCollectionViewCell else { return }
        cell.setSelected(false)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: collectionView.bounds.height)
    }
    
}

extension MultiRestaurantCalenderViewController  : FSCalendarDataSource {
    
}


extension MultiRestaurantCalenderViewController  : FSCalendarDelegate {
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {

        if let item = self.colors[self.dateFormatter.string(from: date)] {
            switch item.2 {
            case .booked , .paused, .delivered:
                return true
            }
        }
        return false
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        DispatchQueue.main.async {
            self.detailsVC = MultiRestaurantCalenderDetailsViewController.instantiate(fromAppStoryboard: .Main)
            self.detailsVC!.selectedDate = date
            self.detailsVC!.colors = self.colors
            self.detailsVC!.selectedVendor = self.selectedVendor
            self.detailsVC!.data = self.data
            self.detailsVC!.serviceCallAction = { [weak self] in
                self?.serviceCall()
            }
            self.showVC(self.detailsVC!)
        }
    }
    
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("\(self.dateFormatter.string(from: calendar.currentPage))")
    }
    
}


extension MultiRestaurantCalenderViewController  : FSCalendarDelegateAppearance {
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
        let key = self.dateFormatter.string(from: date)
        if let color = self.colors[key] {
            return color.0
        }
        return UIColor.white
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        let key = self.dateFormatter.string(from: date)
        if self.colors[key] != nil {
            return appearance.titleSelectionColor
        }
        return nil
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleSelectionColorFor date: Date) -> UIColor? {
        let key = self.dateFormatter.string(from: date)
        if self.colors[key] != nil {
            return appearance.titleSelectionColor
        }
        return appearance.titleDefaultColor
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        let key = self.dateFormatter.string(from: date)
        if let color = self.colors[key] {
            return color.0
        }
        return nil
    }
    
}
