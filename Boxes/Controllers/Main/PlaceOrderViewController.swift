//
//  PlaceOrderViewController.swift
//  Boxes
//
//  Created by macmin on 11/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import SwiftyJSON

class PlaceOrderViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var deliveryFeeLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var contactInfo: UILabel!
    @IBOutlet weak var deliveryDetailsLabel: UILabel!
    @IBOutlet weak var deliveryTimeLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var voucherView: UIView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var checkoutButton: CustomButton!
    @IBOutlet weak var applyVoucherButton: CustomButton!
    @IBOutlet weak var applyVouchertextfield: UITextField!
    @IBOutlet weak var checkButton: UIButton!
    
    var item : CartModel?
    var data = [CartItemModel]()
    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setValues(item: CartManager.shared.item)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        CartManager.shared.getCartItems()
        NotificationCenter.default.addObserver(self, selector: #selector(self.cartObserver), name: .cartLoaded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.addToCartObserver), name: .cartItemAdded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.orderPlaced), name: .orderPlaced, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.voucherApplied), name: .voucherApplied, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.voucherAppliedError), name: .voucherAppliedError, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.cartItemRemoved), name: .cartItemRemoved, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setContactInfo()
        configureNavigationBar()
        self.removeRightButtons()
    }
    
    func configureNavigationBar(){
        
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "cart".localized.uppercased()
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
        // self.placeBackButton(selectorBack: #selector(self.back))
        
    }
    
    func setContactInfo(){
        if let user = UserManager.getUserObjectFromUserDefaults() {
            self.contactInfo.text = CartManager.shared.contactInfo["email"] ?? user.email
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    @objc func cartObserver(){
        self.setValues(item: CartManager.shared.item)
    }
    
    @objc func addToCartObserver(){
        CartManager.shared.getCartItems()
    }
    
    @objc func orderPlaced(){
        self.checkoutButton?.stopLoading()
        let vc = OrderConfirmedViewController.instantiate(fromAppStoryboard: .Main)
//        self.showVC(vc)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true) {
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @objc func voucherApplied(){
        self.applyVoucherButton.stopLoading()
        toggleVoucherView()
    }
    
    @objc func voucherAppliedError(){
        self.applyVoucherButton.stopLoading()
    }
    
    @objc func cartItemRemoved(){
        if CartManager.shared.data.count == 0 {
            CartManager.shared.data.removeAll()
            self.back()
        }
    }
    
    func setValues(item: CartModel?){
        guard let item = item else { return }
        self.item = item
        self.data = item.cartItems
        let total = item.total
        let user = UserManager.getUserObjectFromUserDefaults()
        self.subTotalLabel.text = total?.subTotal ?? "N/A".localized
        self.deliveryFeeLabel.text = total?.deliveryCharges ?? "N/A".localized
        self.discountLabel.text = total?.totalDiscount ?? "N/A".localized
        self.totalLabel.text = total?.total ?? "N/A".localized
        self.contactInfo.text = user?.email ?? "N/A".localized
        
        self.tableViewHeightConstraint.constant = CGFloat(item.cartItems.count * 45)
        self.tableView.reloadData()
//        self.deliveryDetailsLabel.text = user?.email ?? "N/A"
//        self.deliveryTimeLabel.text = "N/A"
//        self.paymentLabel.text = "N/A"
    }
    
    @IBAction func checkAction(_ sender: UIButton) {
        checkButton.isSelected = !checkButton.isSelected
    }
    
    @IBAction func termsCondtionAction(_ sender: UIButton) {
        let vc = TermsConditionPrivacyPolicyViewController.instantiate(fromAppStoryboard: .User)
        self.showVC(vc)
    }

    @IBAction func contactInfoAction(_ sender: UITapGestureRecognizer) {
        let vc = ContactInfoViewController.instantiate(fromAppStoryboard: .Main)
        self.showVC(vc)
    }
    
    @IBAction func deliveryDetailsAction(_ sender: UITapGestureRecognizer) {
        let vc = DeliveryDetailsViewController.instantiate(fromAppStoryboard: .Main)
        self.showVC(vc)
    }
    
    @IBAction func paymentAction(_ sender: UITapGestureRecognizer) {
        let alert = UIAlertController(title: "Payment Method".localized.uppercased(), message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Cash on delivery".localized, style: .default, handler: { (button) in
            
        }))
        alert.addAction(UIAlertAction(title: "Card".localized, style: .default, handler: { (button) in
            let vc = CardInfoViewController.instantiate(fromAppStoryboard: .Main)
            self.showVC(vc)
        }))
        alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: { (button) in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func placeOrderButtonAction(_ sender: CustomButton) {
        
        if checkButton.isSelected {
            if !sender.isLoading() {
                sender.showLoading()
                CartManager.shared.placeOrder()
            }
        }else {
            Messages.showErrorMessage(message: "Select terms and condition".localized)
        }
        
        
        
    }
    
    func toggleVoucherView(){
        applyVoucherButton.isSelected = !applyVoucherButton.isSelected
        self.voucherView.isHidden = applyVoucherButton.isSelected
    }
    
    @IBAction func voucherButtonAction(_ sender: UIButton) {
        toggleVoucherView()
    }
    
    @IBAction func applyVoucherButtonAction(_ sender: CustomButton) {
        
        guard let text = applyVouchertextfield.text, !text.isEmpty else {
            sender.shake()
            Messages.showErrorMessage(message: "Voucher field is empty".localized)
            return
        }
        
        if !sender.isLoading() {
            sender.showLoading()
            CartManager.shared.applyVoucher(title: text)
        }else{
            
        }
    }
    
    
    
}

extension PlaceOrderViewController : UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.data[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: CartTableViewCell.identifier, for: indexPath) as! CartTableViewCell
        cell.minusAction = { count in
            self.timer?.invalidate()
            self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (timer) in
                print("minus from cart service".localized)
                CartManager.shared.newAddToCart(id: item.id, quantity: count)
            }
        }
        cell.plusAction = { count in
            self.timer?.invalidate()
            self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (timer) in
                print("add to cart service".localized)
                CartManager.shared.newAddToCart(id: item.id, quantity: count)
            }
        }
        cell.deleteAction = {
            let alert = UIAlertController(title: "Do you want to remove this item?".localized, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes".localized, style: .default, handler: { (button) in
                CartManager.shared.removeFromCart(id: item.id)
            }))
            alert.addAction(UIAlertAction(title: "No".localized, style: .cancel, handler: { (button) in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        cell.item = item
        return cell
    }
}

extension PlaceOrderViewController : UITableViewDelegate {
    
//    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { _, _, complete in
//            self.data.remove(at: indexPath.row)
//            self.tableView.deleteRows(at: [indexPath], with: .automatic)
//            complete(true)
//        }
//
//        deleteAction.backgroundColor = .red
//
//        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
//        configuration.performsFirstActionWithFullSwipe = true
//        return configuration
//    }
    
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return true
//    }
//    
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { _, _ in
//            let item = self.data[indexPath.row]
//            CartManager.shared.removeFromCart(id: item.id)
////            self.data.remove(at: indexPath.row)
////            self.tableView.deleteRows(at: [indexPath], with: .automatic)
//            
//        }
//        deleteAction.backgroundColor = .red
//        return [deleteAction]
//    }
    
}
