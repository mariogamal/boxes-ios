//
//  UserProfileViewController.swift
//  Boxes
//
//  Created by macmin on 11/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import SwiftyJSON

class UserProfileViewController: BaseViewController {
    
    @IBOutlet weak var topStackView: UIStackView!
    @IBOutlet weak var bottomStackView: UIStackView!
    @IBOutlet weak var topView: CustomCardView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    
    @IBOutlet weak var type_nameLabel: UILabel!
    @IBOutlet weak var validityLabel: UILabel!
    @IBOutlet weak var package_nameLabel: UILabel!
    @IBOutlet weak var subscriptionTitleLabel: UILabel!
    
    @IBOutlet weak var subscribeButton: UIButton!
    
    private var userSubscribedListing : [UserSubscribedListingModel] = []
    
    var animator : UIViewPropertyAnimator?
    var isBack: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: HomeTableViewCell.identifier, bundle: .main), forCellReuseIdentifier: HomeTableViewCell.identifier)
        setUserValues()
        serviceMyCurrentPackage()
        
        self.animator = UIViewPropertyAnimator(duration: 2, dampingRatio: 0.5, animations: {
            self.topStackView.axis = .horizontal
            self.topStackView.alignment = .center
        })
        
    }
    
    
    
    func setUserValues(){
        if let user = UserManager.getUserObjectFromUserDefaults() {
            if let img = user.image, !img.isEmpty {
                self.profileImageView.imageUrl = BASEIMAGEURL + user.image
                self.profileImageView.contentMode = .scaleAspectFill
            }
            if let fname = user.firstName, let lname = user.lastName, !fname.isEmpty, !lname.isEmpty {
                self.profileNameLabel.text = fname + " " + lname
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUserValues()
        self.tabBarController?.navigationItem.titleView = nil
        self.tabBarController?.title = "USERPROFILE" .localized
        self.tabBarController?.navigationItem.rightBarButtonItem = nil
        
        if isBack {
            self.isBack = false
            self.tabBarController?.navigationController?.resetNavigationBar()
        }
        
        
//        configureNavigationBar()
        
        if UserManager.shared.isSubscribed {
            self.serviceMyCurrentPackage()
            UserManager.shared.isSubscribed = false
        }
        
    }
    
    func configureNavigationBar(){
        let font = UIFont(name: AppFont.Oswald_Regular, size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "user profile".localized.uppercased()
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }

    @IBAction func chooseButtonAction(_ sender: UIButton) {

        if sender.titleLabel?.text == "CHOOSEYOURMEAL".localized {
            let vc  = MultiRestaurantCalenderViewController.instantiate(fromAppStoryboard: .Main)
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            self.tabBarController?.selectedIndex = 0
            if let vc = self.tabBarController?.viewControllers?.first as? HomeTopBarViewController {
                vc.scrollToPage(.first, animated: true)
            }
        }
        
    }
    
    @IBAction func editButtonAction(_ sender: UIButton) {
        self.isBack = true
        let vc  = CreateProfileViewController.instantiate(fromAppStoryboard: .User)
        vc.isFromProfile = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func serviceMyCurrentPackage(){
        
        let parameter : [String:String] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"
        ]
        
        NetworkRequest.shared.myCurrentPackage(parameters: parameter) { (response, error) in
            
            if error != nil {
                //                Messages.showErrorMessage(message: error?.message)
                print(error?.message ?? "error")
                return
            }
            
            DispatchQueue.main.async {
                
                let data = MyCurrentPackageModel(fromJson: JSON(response))
                if data.userSubscription == nil {
                    return
                }
                self.setValues(data: data)
                
            }
        }
    }
    
    func setValues(data: MyCurrentPackageModel){
        self.subscriptionTitleLabel.text = data.typeName
        if let subs = data.userSubscription.first {
            self.validityLabel.text = subs.validity
            self.package_nameLabel.text = subs.packageName
//            self.userSubscribedListing = subs.userSubscribedListing
        }
        
        self.userSubscribedListing.removeAll()
        for sub in data.userSubscription {
            for listing in sub.userSubscribedListing {
                userSubscribedListing.append(listing)
            }
        }
        self.subscribeButton.setTitle("CHOOSEYOURMEAL".localized, for: .normal)
        self.tableView.reloadData()
    }
}


extension UserProfileViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userSubscribedListing.isEmpty ? 1 : userSubscribedListing.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var item : UserSubscribedListingListingModel?
        if indexPath.row < self.userSubscribedListing.count {
            item = self.userSubscribedListing[indexPath.row].listing
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: HomeTableViewCell.identifier, for: indexPath) as! HomeTableViewCell
        cell.titleLabel.text = item?.listingName ?? ""
        cell.subTitleLabel.text = item?.captionName ?? ""
        if let img = item?.image, !img.isEmpty {
            cell.imgView.imageUrl = BASEIMAGEURL + img
            cell.imgView.contentMode = .scaleAspectFill
        }else {
            cell.imgView.image = #imageLiteral(resourceName: "logo-2")
            cell.imgView.contentMode = .center
        }
        return cell
    }
}


extension UserProfileViewController : UITableViewDelegate {
    
}

extension UserProfileViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let offsetY = scrollView.contentOffset.y
//        let height = self.view.height * 0.2
//        if self.animator?.isRunning ?? false {
//            self.animator?.fractionComplete = CGFloat(offsetY/height)
//        }else{
//            self.animator?.startAnimation()
//        }
        
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
//        let offsetY = scrollView.contentOffset.y
//        let height = self.view.height * 0.2
//        self.animator?.stopAnimation(false)
//        self.animator?.finishAnimation(at: (CGFloat(offsetY/height) < 0.5) ? .start : .end)
    }
    
}
