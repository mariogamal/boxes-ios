//
//  RestaurantTopbarViewController.swift
//  Boxes
//
//  Created by macmin on 15/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Tabman
import Pageboy

class RestaurantTopbarViewController: TabmanViewController {
    
    @IBOutlet weak var topSlidingView: SlidingView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var topSlidingViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    var typeID: String?
    var item : ListingModel?
    
    private var viewControllers : [(String, UIViewController)] = []
    let bar = TMBar.ButtonBar()
    
    var isScreenOpened : Bool = false
    
    var showSubscription = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(self.topSlidingView)
        
        let vc1 = SubscriptionViewController.instantiate(fromAppStoryboard: .Main)
        let vc2 = MenuViewController.instantiate(fromAppStoryboard: .Main)
        
        vc2.item = self.item
        vc1.item = self.item
        vc1.typeID = self.typeID
        
//        vc1.scrollViewDidScroll = { scrollView in
//            self.slideWindow(scrollView)
//        }
//
//        vc2.scrollViewDidScroll = { scrollView in
//            self.slideWindow(scrollView)
//        }
        
        if self.showSubscription {
            //            self.viewControllers = [("SUBSCRIPTION", vc1),]
            self.viewControllers.append(("SUBSCRIPTION".localized, vc1))
        }
        self.viewControllers.append(("MENU".localized, vc2))
        

        
        self.dataSource = self
        
        bar.layout.transitionStyle = .progressive
        bar.layout.contentMode = .fit
        bar.layout.contentInset = UIEdgeInsets(top: 140, left: 6.0, bottom: 0.0, right: 6.0)
        bar.backgroundView.style = .flat(color: .white)
        bar.indicator.backgroundColor = AppColor.primary
        bar.scrollMode = .interactive

        bar.buttons.customize { (button) in
            button.font = UIFont(name: AppFont.Oswald_Regular, size: 12) ?? UIFont.boldSystemFont(ofSize: 12)
            button.tintColor = AppColor.secondary
            button.selectedTintColor = AppColor.primary
            button.frame = CGRect(x: 0, y: 0, width: self.bar.bounds.width/2, height: self.bar.bounds.height)
        }
        addBar(bar, dataSource: self, at: .top)
        
        
//        addBar(bar, dataSource: self, at: .custom(view: self.topView, layout: { (tabbar) in
//
//            tabbar.backgroundColor = UIColor.clear
//            tabbar.translatesAutoresizingMaskIntoConstraints = false
//            self.topView.addSubview(tabbar)
//
//            tabbar.bottomAnchor.constraint(equalTo: self.topView.bottomAnchor).isActive = true
//            tabbar.leadingAnchor.constraint(equalTo: self.topView.leadingAnchor).isActive = true
//            tabbar.trailingAnchor.constraint(equalTo: self.topView.trailingAnchor).isActive = true
//            tabbar.topAnchor.constraint(equalTo: self.topView.topAnchor).isActive = true
//        }))
        self.setupData()
        self.setupSlidingView()
        self.applyGrad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        print("self.bar.frame")
//        print(self.bar.frame)
//        print("self.navigationController?.navigationBar.frame")
//        print(self.navigationController?.navigationBar.frame)
    }
    
    func setupData(){
        guard let item = self.item else{
            return
        }
        self.imgView.imageUrl = BASEIMAGEURL + item.image
        self.titleLabel.text = item.listingName
        self.subTitleLabel.text = item.listingCaption
    }
    
    func slideWindow(_ scrollView : UIScrollView ){
        
        let offset = scrollView.contentOffset.y + self.bar.bounds.height + (self.navigationController?.navigationBar.bounds.height ?? 44) + 20
        print("vc1 \(offset)")
        
        if offset <= 10 {
            if self.topSlidingView.currentState == .open && self.isScreenOpened {
                self.isScreenOpened = false
                DispatchQueue.main.async {
                    self.topSlidingView.closePopup()
                }
            }
        }else if offset > 50 {
            if self.topSlidingView.currentState == .closed {
                if !self.isScreenOpened {
                    self.isScreenOpened = true
                }
                DispatchQueue.main.async {
                    self.topSlidingView.openPopup()
                }
            }
        }
    }
    
    
    func applyGrad (){
        let blackColor = UIColor.black.withAlphaComponent(0.7)
        self.gradientView.applyGradient(colours: [blackColor, .clear], cornerRadius: 6, startPoint: CGPoint(x: 0, y: 1), endPoint: CGPoint(x: 1, y: 0))
    }
    
    func setupSlidingView(){
        
        self.topSlidingView.onViewUpdate = {
            self.view.layoutIfNeeded()
        }
        self.topSlidingView.onOpenAction = {
            self.topSlidingViewHeightConstraint.constant = 0
            self.bar.layout.contentInset = UIEdgeInsets(top: 0.0, left: 6.0, bottom: 0.0, right: 6.0)
            self.navigationController?.title = "Restaurant Name".localized
        }
        self.topSlidingView.onCloseAction = {
            self.topSlidingViewHeightConstraint.constant = 140
            self.bar.layout.contentInset = UIEdgeInsets(top: 140, left: 6.0, bottom: 0.0, right: 6.0)
            self.navigationController?.title = ""
        }
    }
    
}


extension RestaurantTopbarViewController : PageboyViewControllerDataSource, TMBarDataSource {
    
    func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
        let barItem = TMBarItem(title: self.viewControllers[index].0)
        return barItem
    }
    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return viewControllers.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController,
                        at index: PageboyViewController.PageIndex) -> UIViewController? {
        return viewControllers[index].1
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
    
}
