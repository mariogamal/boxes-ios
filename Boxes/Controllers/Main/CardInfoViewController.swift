//
//  CardInfoViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/26/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class CardInfoViewController: BaseViewController {

    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var cardNumberTextfield: UITextField!
    @IBOutlet weak var dateTextfield: UITextField!
    @IBOutlet weak var cvcTextfield: UITextField!
    @IBOutlet weak var applyButton: UIButton!
    
    @IBOutlet weak var packageNameLabel: UILabel!
    @IBOutlet weak var typeNameLabel: UILabel!
    
    var doneAction : (() -> Void)?
    
    var price = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        packageNameLabel.text = "Total in KWD : \(price)".localized
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        configureNavigationBar()
    }
    
    func configureNavigationBar(){
        
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "card info".localized.uppercased()
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
        // self.placeBackButton(selectorBack: #selector(self.back))
        
    }
    
    func isValidate() -> Bool {
        
        guard let name = self.nameTextfield.text, !name.isEmpty else{
            Messages.showErrorMessage(message: "Write your name".localized)
            return false
        }
        
        guard let cardNumber = self.cardNumberTextfield.text, !cardNumber.isEmpty else{
            Messages.showErrorMessage(message: "Write your card number".localized)
            return false
        }
        
        guard let date = self.dateTextfield.text, !date.isEmpty else{
            Messages.showErrorMessage(message: "Write date".localized)
            return false
        }
        
        guard let cvc = self.cvcTextfield.text, !cvc.isEmpty else{
            Messages.showErrorMessage(message: "Write cvc")
            return false
        }
        
        saveContactInfo(name: name, cardNumber: cardNumber, date: date, cvc: cvc)
        return true
    }
    
    func saveContactInfo(name: String,cardNumber: String,date: String,cvc: String){
        UserManager.shared.cardInfo = [
            "name": name,
            "cardNumber": cardNumber,
            "date": date,
            "cvc": cvc
        ]
        print(UserManager.shared.cardInfo)
    }
    
    @IBAction func applyAction(_ sender: Any) {
        if !isValidate() {
            return
        }
        self.doneAction?()
        self.navigationController?.popViewController(animated: true)
    }

}
