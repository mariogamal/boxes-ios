//
//  SubscriptionViewController.swift
//  Boxes
//
//  Created by macmin on 15/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import SwiftyJSON
import ExpandableCell

class SubscriptionViewController: BaseViewController {
    
    @IBOutlet weak var tableView: ExpandableTableView!
    
    var scrollViewDidEndScrollingAnimationAction : (() -> Void)?
    
    var openScreen : (() -> Void)?
    var closeScreen : (() -> Void)?
    var scrollViewDidScroll :((UIScrollView) -> Void)?
    
    var typeID: String?
    var item : ListingModel?
    var data : [PackageModel] = []
    
    private var lastContentOffset: CGFloat = 0
    var screenOpened : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.serviceCall()
        
        self.tableView.register(UINib(nibName: PackageDetailsCell.identifier, bundle: .main), forCellReuseIdentifier: PackageDetailsCell.identifier)
        self.tableView.register(UINib(nibName: SubscriptionTableViewCell.identifier, bundle: .main), forCellReuseIdentifier: SubscriptionTableViewCell.identifier)
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)

        tableView.expandableDelegate = self
        tableView.autoReleaseDelegate = false
        tableView.animation = .automatic
        tableView.expansionStyle = .multi
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func serviceCall() {
        guard let id = self.item!.id else{
            return
        }
        
        guard self.typeID != nil else{
            return
        }
        
        let parameter : [String:String] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
            "listing_id" : id
        ]
        NetworkRequest.shared.packages(parameters: parameter) { (response, error) in
            if error != nil {
                return
            }
            let types = JSON(response).arrayValue.map({ PackageModel.init(fromJson: $0) })
            self.data = types
            self.tableView.reloadData()
        }
    }
    
    func checkOutService(packageId: String) {
        
        guard UserManager.getUserObjectFromUserDefaults() != nil else {
            let vc = LoginViewController.instantiate(fromAppStoryboard: .User)
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        
        guard UserManager.getUserObjectFromUserDefaults()?.firstName != "" else {
            let vc = CreateProfileViewController.instantiate(fromAppStoryboard: .User)
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        guard let item = self.item, let typeId = self.typeID else{
            Messages.showErrorMessage(message: "ID not available".localized)
            return
        }
        var parameter : [String:String] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
            "listing_id" : item.id,
            "package_id" : packageId,
            "type_id" : typeId,
            "card_holder_name" : "Sample",
            "card_number" : "123456789",
            "expiry_date" : "123123",
            "cvv" : "123",
            "Is_save" : "1",
            "weekly" : "4",
            "state": "start"
        ]
        
        print(parameter)
        parameter.merge(UserManager.shared.cardInfo) { (a, b) -> String in
            return b
        }
        NetworkRequest.shared.userSubscription(parameters: parameter) { (response, error) in
            if let error = error {
                self.showError(error: error)
                return
            }
            let stringURL = response as! String
            self.openPayment(stringURL: stringURL, parameter: parameter)
        }
    }
    
    func openPayment(stringURL: String, parameter : [String:String]) {
        let vc = PaymentViewController.instantiate(fromAppStoryboard: .Main)
        vc.stringUrl = stringURL
        vc.parameter = parameter
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showCompleteProfile() {
        if let user = UserManager.getUserObjectFromUserDefaults() {
            if user.isProfileFilled == 1 {
                self.orderPlaced()
            }else {
                let vc = CreateProfileViewController.instantiate(fromAppStoryboard: .User)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func orderPlaced(){
        let vc = OrderConfirmedViewController.instantiate(fromAppStoryboard: .Main)
        self.showVC(vc)
    }
    
    func showError(error: MyError){
        let title = error.message
        for message in error.userInfo {
            if !message.value.isEmpty {
                Messages.showErrorMessage(title: title, message: message.value)
                return
            }
        }
        
        guard !error.userInfo.isEmpty else {
            Messages.showErrorMessage(message: title)
            return
        }
        
    }
    
}

extension SubscriptionViewController: ExpandableDelegate {
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightsForExpandedRowAt indexPath: IndexPath) -> [CGFloat]? {
        return [UITableView.automaticDimension]
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCell: UITableViewCell, didSelectExpandedRowAt indexPath: IndexPath) {
        let itemId = (expandedCell as! PackageDetailsCell).itemId
        self.checkOutService(packageId: itemId)
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCellsForRowAt indexPath: IndexPath) -> [UITableViewCell]? {
        let item = self.data[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: PackageDetailsCell.identifier, for: indexPath) as! PackageDetailsCell
        cell.itemId = item.id
        cell.descriptionLabel.text = item.details
        cell.caloriesLabel.text = item.calories
        cell.categoryNameLabel.text = getCategoriesNames(settings: item.settings)
        cell.categoryValueLabel.text = getCategoriesNumber(settings: item.settings)
        return [cell]
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.data[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: SubscriptionTableViewCell.identifier, for: indexPath) as! SubscriptionTableViewCell
        cell.titleLabel.text = item.packageName
        cell.priceLabel.text = item.price
        return cell
    }
    
    func numberOfSections(in tableView: ExpandableTableView) -> Int {
        return 1
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func expandableTableView(_ expandableTableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    private func getCategoriesNames(settings: [Setting]) -> String {
        var names = ""
        settings.forEach { (category) in
            if category.consumption != "0" {
                names += category.categoryName + "\n"
            }
        }
        return String(names.dropLast())
    }
    
    private func getCategoriesNumber(settings: [Setting]) -> String {
        var numbers = ""
        settings.forEach { (category) in
            if category.consumption != "0" {
                numbers += category.consumption + "\n"
            }
        }
        return String(numbers.dropLast())
    }
}
