//
//  ItemDetailsViewController.swift
//  Boxes
//
//  Created by macmin on 11/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ItemDetailsViewController: BaseViewController {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var stockButton: UIButton!
    @IBOutlet weak var discountButton: UIButton!
    @IBOutlet weak var addButton: CustomButton!
    @IBOutlet weak var countStackView: UIStackView!
    
    var maxCount = 100
    var count = 1 {
        didSet{
            self.countLabel?.text = "\(self.count)"
        }
    }
    var item : ProductModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Details".localized.uppercased()
        if let product = self.item {
            self.setup(item: product)
            self.addButton.setTitle( CartManager.shared.cartItemExists(id: product.id) ? "ADDED TO CART".localized : "ADD TO CART".localized, for: .normal)
//            self.countStackView.isHidden = true
        }
        
        self.addBadgeRightBarButtonWithCartImage(itemvalue: "\(CartManager.shared.cartItemCount)")
        NotificationCenter.default.addObserver(self, selector: #selector(self.itemAdded), name: .cartItemAdded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.itemAddedError), name: .cartItemAddedError, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.itemRemoved), name: .cartItemRemoved, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.cartObserver), name: .cartLoaded, object: nil)
    }
    
    func setup(item : ProductModel){
        self.titleLabel.text = item.productName
        self.imgView.imageUrl = BASEIMAGEURL +  item.image
        self.priceLabel.text = item.price
        self.descriptionLabel.text = item.productDescription
        self.stockButton.setTitle(item.stock, for: .normal)
        self.discountButton.setTitle(item.discount, for: .normal)
        self.maxCount = item.stockCount
//        self.count = CartManager.shared.getItemCount(id: item.id)
    }
    
    @objc func itemAdded(){
        if let product = self.item {
            self.addButton.setTitle( CartManager.shared.cartItemExists(id: product.id) ? "ADDED TO CART" .localized: "ADD TO CART".localized, for: .normal)
        }
        Messages.showSuccessMessage(message: "Successfully added to Cart".localized)
        self.addButton.stopLoading()
//        self.addBadgeRightBarButtonWithCartImage(itemvalue: "\(CartManager.shared.cartItemCount)")
    }

    @objc func itemAddedError(){
        self.addButton.stopLoading()
        self.addButton.shake()
    }
    @objc func itemRemoved(){
//        self.addBadgeRightBarButtonWithCartImage(itemvalue: "\(CartManager.shared.cartItemCount)")
    }
    
    @objc func cartObserver(){
        if let product = self.item {
            self.addButton.setTitle( CartManager.shared.cartItemExists(id: product.id) ? "ADDED TO CART".localized : "ADD TO CART".localized, for: .normal)
        }
        self.addBadgeRightBarButtonWithCartImage(itemvalue: "\(CartManager.shared.cartItemCount)")
    }
    
    @IBAction func addToCartButtonAction(_ sender: CustomButton) {
        guard let product = self.item else { return }
//        guard !CartManager.shared.cartItemExists(id: product.id) else {
////            Messages.showErrorMessage(title: "Item Existed", message: "Item already added to cart")
//            CartManager.shared.newAddToCart(id: product.id, quantity: count, opr: "add")
////            sender.shake()
//            return
//        }
//        if !sender.isLoading() {
            CartManager.shared.newAddToCart(id: product.id, quantity: CartManager.shared.getItemCount(id: product.id) + count)
//            sender.showLoading()
//        }
        
    }
    
    @IBAction func addButtonAction(_ sender: Any) {
        self.count += ( self.count < self.maxCount) ? 1 : 0
    }
    @IBAction func subtractButtonAction(_ sender: UIButton) {
        self.count -= ( self.count > 1) ? 1 : 0
    }
    
}
