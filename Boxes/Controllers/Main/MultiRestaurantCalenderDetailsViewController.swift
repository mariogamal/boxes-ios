//
//  MultiRestaurantCalenderDetailsViewController.swift
//  Boxes
//
//  Created by macmin on 18/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import FSCalendar
import SwiftyJSON

protocol MultiRestaurantDelegate {
    func updatedColors()
}

class MultiRestaurantCalenderDetailsViewController: BaseViewController {
    
    @IBOutlet weak var resturantsCollection: UICollectionView!
    @IBOutlet weak var customCalendarView: CustomCardView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var calendarButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var checkOutButton: CustomButton!
    
    var selectedItemsDict : [Int:[MenuProduct]] = [:] {
        didSet {
            self.tableView?.reloadData()
        }
    }
    
    var selectedDate : Date? {
        didSet {
            self.resturantsCollection?.reloadData()
        }
    }
    var selectedCell: CalendarCollectionViewCell?
    var colors : [String : (UIColor,Bool,PackageType,String)] = [:]
    var menuItems : [String : [String]] = [:]
    var data : NewCalenderResponse?
    var selectedVendor : CalenderDatailModel?
    var menu : [MenuModel] = []
    
    var selectedIds = [String]() {
        didSet {
            print(selectedIds)
        }
    }
    
    var serviceCallAction : (() -> Void)?
    var enableUpdate: Bool = false
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = Locale(identifier: "en_US")
        return formatter
    }()
    
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "E, MMM dd, yyyy"
        formatter.locale = Locale(identifier: "en_US")
        return formatter
    }()
    
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendar, action: #selector(self.calendar.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkOutButton.isHidden = true //by client change 16 March
        self.title = "Calendar Details".localized
        calendar.dataSource = self
        calendar.delegate = self
        calendar.select(self.selectedDate)
        dateView.alpha = 0.0 //by client change 16 March
        calendar.scope = .week //by client change 16 March
        calendar.today = nil
        calendar.appearance.caseOptions = [.headerUsesUpperCase,.weekdayUsesSingleUpperCase]
        calendar.appearance.headerTitleFont = UIFont(name: AppFont.Oswald_Regular, size: 18) ?? UIFont.systemFont(ofSize: 18)
        calendar.appearance.headerTitleColor = UIColor.darkText
        calendar.appearance.weekdayFont = UIFont(name: AppFont.PTSans_Regular, size: 14) ?? UIFont.systemFont(ofSize: 18)
        calendar.appearance.weekdayTextColor = UIColor.lightGray
        calendar.calendarHeaderView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        calendar.calendarWeekdayView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        calendar.today = nil
        calendar.locale = Locale(identifier: "en_US")
        //by client change 16 March
        //        self.view.addGestureRecognizer(scopeGesture)
        //        self.tableView.panGestureRecognizer.require(toFail: scopeGesture)
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: FoodTableViewCell.identifier, bundle: .main), forCellReuseIdentifier: FoodTableViewCell.identifier)
        
        self.setupMenuItems()
        self.menuServiceCall()
        
        if let date = self.selectedDate {self.dateLabel.text = self.dateFormatter2.string(from: date)}
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateData), name: Notification.Name.init("serviceLoaded"), object: nil)
        self.setupCollectionView()
    }
    
    func setupCollectionView(){
        resturantsCollection.dataSource = self
        resturantsCollection.delegate = self
        resturantsCollection.register(UINib(nibName: CalendarCollectionViewCell.identifier, bundle: .main), forCellWithReuseIdentifier: CalendarCollectionViewCell.identifier)
    }
    
    @objc func updateData(){
        let count = self.navigationController?.viewControllers.count ?? 2
        if count > 1 {
            if let vc = self.navigationController?.viewControllers[count - 2] as? MultiRestaurantCalenderViewController {
                //                self.selectedDate = vc.
                self.colors = vc.colors
                self.selectedVendor = vc.selectedVendor
                //                self.data = vc.data
                self.setupMenuItems()
                self.menuServiceCall()
                if let date = self.selectedDate {self.dateLabel.text = self.dateFormatter2.string(from: date)}
                self.calendar.reloadData()
                self.tableView.reloadData()
                self.resturantsCollection.reloadData()
            }
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        calendar.reloadData()
    }
    @IBAction func pauseButtonAction(_ sender: UIButton) {
        guard let date = self.selectedDate else { return }
        if let item = self.colors[self.dateFormatter.string(from: date)] {
            switch item.2 {
            case .booked:
                self.serviceCallPostPauseMeal(order_detail_id: item.3, status: "pause")
            case .paused:
                self.serviceCallPostPauseMeal(order_detail_id: item.3, status: "pending")
            default:
                break
            }
        }
    }
    
    @IBAction func checkOutButtonAction(_ sender: CustomButton) {
        
        if sender.isLoading() {
            return
        }
        
        if selectedVendor!.used >= selectedVendor!.subscription && !enableUpdate{
            Messages.showErrorMessage(message: "No available days remaining".localized)
            return
        }
        
        guard let date = self.selectedDate else { return }

        if let item = self.colors[self.dateFormatter.string(from: date)] {
            if item.2 == .delivered {
                Messages.showErrorMessage(message: "You can not update a delivered date ".localized)
                return
            }
        }
        
        guard date >= Date().addDay(days: 2) else {
            Messages.showErrorMessage(message: "You can not update a previous date or two days from today".localized)
            return
        }
        
        for index in 0..<self.menu.count {
            let count = self.selectedItemsDict[index]?.count ?? 0
            let mealConsumption = self.menu[index].mealConsumption
            if count != mealConsumption {
                Messages.showErrorMessage(message: "Please select all required item in \(self.menu[index].menuCatName ?? "".localized)")
                return
            }
        }
        
        sender.showLoading()
        let key = self.dateFormatter.string(from: date)
        guard self.menuItems[key] != nil else {
            Messages.showErrorMessage(message: "Please select an item".localized)
            return
        }
        
        if let item = self.colors[key] {
            self.updateService(order_detail_id: item.3)
        }else{
            self.storeService()
        }
    }
    
    func toggle() {
        if self.calendar.scope == .month {
            self.calendar.setScope(.week, animated: true)
        } else {
            self.calendar.setScope(.month, animated: true)
        }
    }
    
    func menuServiceCall() {
        
        enableUpdate = false
        
        guard let id = self.selectedVendor?.id else{
            return
        }
        
        var parameter : [String:String] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
            "listing_id" : id
        ]
        
        if let userId = UserManager.getUserObjectFromUserDefaults()?.id {
            parameter["user_id"] = userId
        }
        
        if let date =  self.calendar.selectedDate {
            parameter["date"] = dateFormatter.string(from: date)
        }
        
        NetworkRequest.shared.menu(parameters: parameter) { (response, error) in
            
            if error != nil {
                return
            }
            let data = JSON(response).arrayValue.map({ MenuModel.init(fromJson: $0) })
            
            self.menu = data
            self.selectedItemsDict.removeAll()
            if let date = self.calendar.selectedDate, let array = self.menuItems[self.dateFormatter.string(from: date)], !array.isEmpty {
                self.menu.enumerated().forEach { (index, item) in
                    item.products.forEach { menuProductModel in
                        if array.contains(menuProductModel.id) {
                            if var selectItemsArray = self.selectedItemsDict[index], !selectItemsArray.isEmpty {
                                selectItemsArray.append(menuProductModel)
                                self.selectedItemsDict[index] = selectItemsArray
                            }else{
                                self.enableUpdate = true
                                self.selectedIds.append(menuProductModel.id)
                                self.selectedItemsDict[index] = [menuProductModel]
                            }
                        }
                    }
                }
            }
            self.tableView.reloadData()
        }
    }
    
    func serviceCallPostPauseMeal(order_detail_id: String, status: String){
        let parameter : [String:String] = [
            "order_detail_id": order_detail_id,
            "status" : status
        ]
        NetworkRequest.shared.pauseMeal(parameters: parameter) { (response, error) in
            if error != nil {
                Messages.showErrorMessage(message: error?.message ?? "")
                return
            }
            Messages.showSuccessMessage(message: "Successfully updated".localized)
            guard let date = self.selectedDate else { return }
            if var item = self.colors[self.dateFormatter.string(from: date)] {
                if status == "pause" {
                    self.calendarButton.setTitle("Resume".localized, for: .normal)
                    self.calendarButton.backgroundColor = AppColor.primary
                    self.calendarButton.isHidden = false
                    item.0 = AppColor.pause
                    item.2 = .paused
                    
                } else if status == "pending" {
                    self.calendarButton.setTitle("Pause".localized, for: .normal)
                    self.calendarButton.backgroundColor = AppColor.pause
                    self.calendarButton.isHidden = false
                    item.0 = AppColor.primary
                    item.2 = .booked
                }
                self.colors[self.dateFormatter.string(from: date)] = item
                self.calendar.reloadData()
                self.serviceCallAction?()
            }
            //            let data = JSON(response).arrayValue.map({ MenuModel.init(fromJson: $0) })
            //            self.menu = data
            //            self.tableView.reloadData()
            
        }
    }
    
    func storeService(){
        guard let date = self.selectedDate else { return }
        guard let array = self.menuItems[self.dateFormatter.string(from: date)] else { return }
        let ids = array.reduce("") { (result, id) -> String in
            return "\(result),\(id)"
        }
        
        let parameter : [String:String] = [
            "product_ids": String(ids.dropFirst(1)),
            "delivery_date": self.dateFormatter.string(from: date),
            "listing_id": self.selectedVendor?.id ?? ""
        ]
        NetworkRequest.shared.storeUserMeal(parameters: parameter) { (response, error) in
            self.checkOutButton.stopLoading()
            if error != nil {
                Messages.showErrorMessage(message: error?.message ?? "")
                return
            }
            Messages.showSuccessMessage(message: "Successfully Stored Meals".localized)
            //            self.navigationController?.popToRootViewController(animated: true)
            self.serviceCallAction?()
            if let nextDate = self.getDateForNextOrder() {
                self.calendar(self.calendar, didSelect: nextDate, at: .next)
                self.calendar.select(nextDate)
                
            }
            
        }
        
    }
    
    func updateService(order_detail_id: String){
//        let ids = self.selectedIds.reduce("") { (result, id) -> String in
//            return "\(result),\(id)"
//        }
        
        var ids = ""
        selectedIds.forEach{ id in
            ids += "\(id),"
        }
        
        let parameter : [String:String] = [
            "order_detail_id": order_detail_id,
            "product_ids": String(ids.dropLast(1))
        ]
        NetworkRequest.shared.updateUserMeal(parameters: parameter) { (response, error) in
            self.checkOutButton.stopLoading()
            if error != nil {
                Messages.showErrorMessage(message: error?.message ?? "")
                return
            }
            Messages.showSuccessMessage(message: "Successfully Updated Meals".localized)
            //            self.navigationController?.popToRootViewController(animated: true)
            self.updateSelectedVendor()
            self.serviceCallAction?()
            if let nextDate = self.getDateForNextOrder() {
                self.selectedIds.removeAll()
                self.calendar(self.calendar, didSelect: nextDate, at: .next)
                self.calendar.select(nextDate)
            }
        }
    }
    
    private func updateSelectedVendor() {
        let vcs = self.navigationController?.viewControllers
        if let vc = vcs?[vcs!.count - 2] as? MultiRestaurantCalenderViewController {
            vc.selectedVendor = selectedVendor
        }
    }
    
    func deleteService(order_detail_id: String){
        let parameter : [String:String] = [
            "order_detail_id": order_detail_id,
        ]
        NetworkRequest.shared.deleteUserMeal(parameters: parameter) { (response, error) in
            self.checkOutButton.stopLoading()
            if error != nil {
                Messages.showErrorMessage(message: error?.message ?? "")
                return
            }
            Messages.showSuccessMessage(message: "Successfully Deleted Meals".localized)
            guard let date = self.selectedDate else { return }
            let key = self.dateFormatter.string(from: date)
            self.colors.removeValue(forKey: key)
            self.menuItems.removeValue(forKey: key)
            //            self.selectedData?.deliveryDates.removeAll(where: { (deliveryModel) -> Bool in
            //                if deliveryModel.deliveryDate == key {
            //                    return true
            //                }
            //                return false
            //            })
            self.calendar.reloadData()
            self.serviceCallAction?()
            if let nextDate = self.getDateForNextOrder() {
                self.calendar(self.calendar, didSelect: nextDate, at: .next)
                self.calendar.select(nextDate)
            }
        }
    }
    
    
    func setupMenuItems(){
        
        self.data?.deliveryDates.forEach({ (model) in
            if let meals = model.userMeals {
                var array : [String] = []
                meals.forEach { (mealModel) in
                    if let mealId = mealModel.id, !mealId.isEmpty {
                        array.append(mealId)
                    }
                }
                self.menuItems[model.deliveryDate] = array
            }
        })
        guard let date = self.selectedDate else { return }
        guard let item = self.colors[self.dateFormatter.string(from: date)] else{ return }
        if item.2 == .booked {
            self.calendarButton.setTitle("Pause".localized, for: .normal)
            self.calendarButton.backgroundColor = AppColor.pause
            self.calendarButton.isHidden = false
            self.checkOutButton.setTitle("UPDATE".localized, for: .normal)
        } else if item.2 == .paused {
            self.calendarButton.setTitle("Resume".localized, for: .normal)
            self.calendarButton.backgroundColor = AppColor.primary
            self.calendarButton.isHidden = false
            self.checkOutButton.setTitle("UPDATE".localized, for: .normal)
        }
        
        
    }
    
    func getDateForNextOrder() -> Date?{
        guard let date = self.calendar.selectedDate else{ return nil }
        return date.addDay()
    }
    
    func addTrashIcon(){
        guard let date = self.selectedDate else { return }
        let key = self.dateFormatter.string(from: date)
        if let item = self.colors[key] {
            if item.2 == .booked {
                let trashIcon = UIBarButtonItem(image: #imageLiteral(resourceName: "icons8-trash_can"), style: .plain, target: self, action: #selector(deleteAction))
                self.navigationItem.rightBarButtonItems = [trashIcon]
            }else{
                self.removeTrashIcon()
            }
        }else{
            self.removeTrashIcon()
        }
    }
    
    func removeTrashIcon(){
        self.navigationItem.rightBarButtonItem = nil
    }
    
    @objc func deleteAction(){
        let alert = UIAlertController.init(style: .alert, source: nil, title: "Are you sure you want to delete this order".localized, message: nil, tintColor: AppColor.primary)
        alert.addAction(UIAlertAction.init(title: "Okay".localized, style: .default, handler: { (action) in
            guard let date = self.selectedDate else { return }
            let key = self.dateFormatter.string(from: date)
            if let item = self.colors[key] {
                self.deleteService(order_detail_id: item.3)
            }
        }))
        alert.addAction(title: "Cancel".localized)
        self.present(alert, animated: true, completion: nil)
    }
    
    func setupData(){
        self.colors.removeAll()
        self.data?.deliveryDates.forEach { (model2) in
            var color : UIColor = .yellow
            var packageType : PackageType = .booked
            if model2.status == "pending" {
                color = AppColor.primary
                packageType = .booked
            }else if model2.status == "pause" {
                color = AppColor.pause
                packageType = .paused
            }else{
                color = AppColor.deliver
                packageType = .delivered
            }
            let isSelected = !model2.userMeals.isEmpty
            self.colors[model2.deliveryDate] = (isSelected ? color : color.withAlphaComponent(0.4), isSelected, packageType, model2.orderDetailId)
        }
    }
    
}

extension MultiRestaurantCalenderDetailsViewController  : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.menu.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menu[section].products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.menu[indexPath.section].products[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: FoodTableViewCell.identifier, for: indexPath) as! FoodTableViewCell
        cell.enableSelection()
        cell.titleLabel.text = item.productName
        cell.subTitleLabel.text = item.captionName
        cell.imgView.imageUrl = BASEIMAGEURL + item.image
        cell.caloriesLabel.text = "\(item.calories ?? 0)"
        cell.proteinLabel.text = "\(item.proteins ?? 0)"
        cell.carbsLabel.text = "\(item.carbohydrates ?? 0)"
        cell.ratingLabel.text = item.rating
        if let date = self.selectedDate {
            if let items = self.menuItems[self.dateFormatter.string(from: date)] {
                cell.selectItem(selected: items.contains(item.id))
            } else{
                cell.selectItem(selected: false)
            }
        }
        return cell
    }
    
    func isCellSelected(cell: FoodTableViewCell, id: String){
        
    }
    
}

extension MultiRestaurantCalenderDetailsViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.checkOutButton.isHidden = false //by client change 16 March
        tableView.deselectRow(at: indexPath, animated: true)
        
        let sectionItem = self.menu[indexPath.section]
        let item = sectionItem.products[indexPath.row]
        guard let cell = tableView.cellForRow(at: indexPath) as? FoodTableViewCell else{ return }
        guard let date = self.selectedDate else { return }
        
        if date <= Date() {
            DispatchQueue.main.async {
                let vc = RatingsViewController.instantiate(fromAppStoryboard: .User)
                vc.productId = item.id
                vc.productTitle = item.productName
                vc.productImage = item.image
                self.present(vc, animated: true, completion: nil)
            }
            return
        }
        
        guard date >= Date().addDay(days: 2) else {
            Messages.showErrorMessage(message: "You can't edit this day")
            return
        }

        guard var items = self.menuItems[self.dateFormatter.string(from: date)], !items.isEmpty else {
            if (sectionItem.mealConsumption == 0) {
                return
            }
            self.selectedItemsDict[indexPath.section] = [item]
            self.menuItems[self.dateFormatter.string(from: date)] = [item.id]
            cell.selectItem(selected: true)
            selectedIds.append(item.id)
            tableView.reloadData()
            //            self.toggleService(indexPath: indexPath)
            
            return
        }
        guard items.contains(item.id) else {
            if (sectionItem.mealConsumption == 0) {
                return
            }
            if var selectedItemsFromDictionary = self.selectedItemsDict[indexPath.section], !selectedItemsFromDictionary.isEmpty {
                if !(selectedItemsFromDictionary.count < sectionItem.mealConsumption) {
                    return
                }
                selectedItemsFromDictionary.append(item)
                self.selectedItemsDict[indexPath.section] = selectedItemsFromDictionary
            }else{
                self.selectedItemsDict[indexPath.section] = [item]
            }
            items.append(item.id)
            selectedIds.append(item.id)
            self.menuItems[self.dateFormatter.string(from: date)] = items
            cell.selectItem(selected: true)
            tableView.reloadData()
            //            self.toggleService(indexPath: indexPath)
            return
        }
        if var selectedItemsFromDictionary = self.selectedItemsDict[indexPath.section], !selectedItemsFromDictionary.isEmpty {
            selectedItemsFromDictionary.remove(item)
            self.selectedItemsDict[indexPath.section] = selectedItemsFromDictionary
        }
        items.removeAll(item.id)
        selectedIds.removeAll(item.id)
        self.menuItems[self.dateFormatter.string(from: date)] = items
        cell.selectItem(selected: false)
        tableView.reloadData()
        //        self.toggleService(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.menu[section].products.isEmpty ? 0 : 40
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.menu[section].products.isEmpty {
            return nil
        }else{
            let count = self.selectedItemsDict[section]?.count ?? 0
            let menuName = self.menu[section].menuCatName ?? "N/A".localized
            let mealCosumption = self.menu[section].mealConsumption ?? 0
            return "\(menuName)     \(count)/\(mealCosumption)"
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard !self.menu[section].products.isEmpty else{return nil}
        
        let myLabel = UILabel()
        myLabel.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40)
        myLabel.font = UIFont.init(name: AppFont.Oswald_Regular, size: 15) ?? UIFont.boldSystemFont(ofSize: 15)
        myLabel.text = self.tableView(tableView, titleForHeaderInSection: section)
        myLabel.textAlignment = .center
        myLabel.backgroundColor = UIColor.groupTableViewBackground
        
        
        let headerView = UIView()
        headerView.addSubview(myLabel)
        
        return headerView
    }
    
    func toggleService(indexPath: IndexPath){
        guard self.menu[indexPath.section].products.count > 1 else { return }
        guard let date = self.selectedDate else { return }
        let itemT = self.menu[indexPath.section].products[indexPath.row]
        guard var array = self.menuItems[self.dateFormatter.string(from: date)] else { return }
        for item in self.menu[indexPath.section].products {
            array.removeAll(item.id)
        }
        for i in 0 ..< self.menu[indexPath.section].products.count {
            if let cell = tableView.cellForRow(at: IndexPath(row: i, section: indexPath.section)) as? FoodTableViewCell {
                cell.selectItem(selected: false)
            }
        }
        if let cell = tableView.cellForRow(at: indexPath) as? FoodTableViewCell {
            cell.selectItem(selected: true)
        }
        array.append(itemT.id)
        self.menuItems[self.dateFormatter.string(from: date)] = array
        self.tableView.reloadData()
        
    }
    
}

extension MultiRestaurantCalenderDetailsViewController  : UIGestureRecognizerDelegate {
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let shouldBegin = self.tableView.contentOffset.y <= -self.tableView.contentInset.top
        if shouldBegin {
            let velocity = self.scopeGesture.velocity(in: self.view)
            switch self.calendar.scope {
            case .month:
                return velocity.y < 0
            default:
                return velocity.y > 0
            }
        }
        return shouldBegin
    }
    
}


extension MultiRestaurantCalenderDetailsViewController  : FSCalendarDataSource {
    
}


extension MultiRestaurantCalenderDetailsViewController  : FSCalendarDelegate {
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        //by client change 16 March
        self.calendarHeightConstraint.constant = bounds.height
        self.dateView.alpha = (self.calendar.scope == .month) ? 0.0 : 0.0
        self.titleLabel.isHidden = (self.calendar.scope == .month) ? true : true
        self.customCalendarView.isHidden = (self.calendar.scope == .month) ? false : false
        self.calendar.isHidden = (self.calendar.scope == .month) ? false : false
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {

        if let item = self.colors[self.dateFormatter.string(from: date)] {
            switch item.2 {
            case .booked , .paused, .delivered:
                return true
            }
        }
        return false
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
        
        if let item = self.colors[self.dateFormatter.string(from: date)] {
            if item.2 == .booked {
                self.calendarButton.setTitle("Pause".localized, for: .normal)
                self.calendarButton.backgroundColor = AppColor.pause
                self.calendarButton.isHidden = false
            } else if item.2 == .booked {
                self.calendarButton.setTitle("Resume".localized, for: .normal)
                self.calendarButton.backgroundColor = AppColor.primary
                self.calendarButton.isHidden = false
            }
        }else{
            self.calendarButton.isHidden = true
            self.checkOutButton.setTitle("CHECKOUT".localized, for: .normal)
        }
        self.selectedItemsDict.removeAll()
        self.dateLabel.text = self.dateFormatter2.string(from: date)
        self.selectedDate = date
        self.setupMenuItems()
        self.menuServiceCall()
        self.tableView.reloadData()
    }
    
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        //        print("\(self.dateFormatter.string(from: calendar.currentPage))")
    }
    
}


extension MultiRestaurantCalenderDetailsViewController  : FSCalendarDelegateAppearance {
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
        return UIColor.orange
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        let key = self.dateFormatter.string(from: date)
        if self.colors[key] != nil {
            return appearance.titleSelectionColor
        }
        return nil
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleSelectionColorFor date: Date) -> UIColor? {
        return appearance.titleSelectionColor
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        let key = self.dateFormatter.string(from: date)
        if let color = self.colors[key] {
            return color.0
        }
        return nil
    }
    
}

extension MultiRestaurantCalenderDetailsViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data?.vendors.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CalendarCollectionViewCell.identifier, for: indexPath) as! CalendarCollectionViewCell
        
        if let item = self.data?.vendors[indexPath.row] {
            cell.setup(title: item.listingName, subTitle: "\(item.used ?? 0)/\(item.subscription ?? 0)", image: item.image)
            cell.createModels(selected: item.used, total: item.subscription)
            cell.setSelected(item == self.selectedVendor)
            if item == self.selectedVendor {
                self.selectedCell = cell
            }
        }
        
        return cell
    }
    
}

extension MultiRestaurantCalenderDetailsViewController : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? CalendarCollectionViewCell else { return }
        
        if let oldCell = self.selectedCell {
            oldCell.setSelected(false)
        }
        if let item = self.data?.vendors[indexPath.row] {
            cell.setSelected(true)
            self.selectedVendor = item
            self.setupData()
            self.calendar.reloadData()
            self.setupMenuItems()
            self.menuServiceCall()
            self.selectedIds.removeAll()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? CalendarCollectionViewCell else { return }
        cell.setSelected(false)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: collectionView.bounds.height)
    }
}
