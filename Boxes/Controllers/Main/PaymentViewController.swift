//
//  PaymentViewController.swift
//  Boxes
//
//  Created by Mario Gamal on 11/14/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import WebKit

class PaymentViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var wMyWebView: WKWebView!
    
    var stringUrl = ""
    var parameter = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        wMyWebView.navigationDelegate = self
        let url = URL(string: stringUrl)
        let request = URLRequest(url: url!)
        wMyWebView.load(request as URLRequest)
    }
    
    
    func showCompleteProfile() {
        if let user = UserManager.getUserObjectFromUserDefaults() {
            if user.isProfileFilled == 1 {
                self.orderPlaced()
            }else {
                let vc = CreateProfileViewController.instantiate(fromAppStoryboard: .User)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func orderPlaced(){
        let vc = OrderConfirmedViewController.instantiate(fromAppStoryboard: .Main)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true) {
            self.navigationController?.popViewController(animated: false)
        }
    }

    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.getElementById('is_captured').value") { (result, error) in
            if let result = result as? String {
                self.wMyWebView.isHidden = true
                if result == "1" {
                    self.checkOutService()
                } else {
                    Messages.showErrorMessage(message: "Payment transaction failed".localized)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func checkOutService() {
        
        parameter.merge(UserManager.shared.cardInfo) { (a, b) -> String in
            return b
        }
        
        parameter["state"] = "success"
        NetworkRequest.shared.userSubscription(parameters: parameter) { (response, error) in
            if let error = error {
                Messages.showErrorMessage(message: error.message)
                return
            }
            self.showCompleteProfile()
            Messages.showSuccessMessage(message: "Order Placed Successfully".localized)
            self.navigationController?.popToRootViewController(animated: true)
            UserManager.shared.isSubscribed = true
            UserManager.saveIsUserSubscribed(isSubscribed: true)
        }
    }
}
