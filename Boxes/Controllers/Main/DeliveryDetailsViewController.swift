//
//  DeliveryDetailsViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/25/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class DeliveryDetailsViewController: BaseViewController {

    @IBOutlet weak var buildingNumberTextBox: SurveyBox!
    @IBOutlet weak var streetTextBox: SurveyBox!
    @IBOutlet weak var areaTextBox: SurveyBox!
    @IBOutlet weak var floorunitTextBox: SurveyBox!
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureTextFields()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    
    func configureNavigationBar(){
        
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "delivery details".localized.uppercased()
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
        // self.placeBackButton(selectorBack: #selector(self.back))
        
    }

    func configureTextFields(){
        
        self.buildingNumberTextBox.setTextFieldPlaceHolder(string: "Enter Your Building Number".localized, labelTop : "Building Number".localized)
        self.streetTextBox.setTextFieldPlaceHolder(string: "Enter Your Street Number".localized, labelTop : "Street".localized)
        self.areaTextBox.setTextFieldPlaceHolder(string: "Enter Area".localized, labelTop : "Area".localized)
        self.floorunitTextBox.setTextFieldPlaceHolder(string: "Floor/Unit".localized, labelTop : "Floor/Unit".localized)
        
        self.buildingNumberTextBox.textField.delegate = self
        self.streetTextBox.textField.delegate = self
        self.areaTextBox.textField.delegate = self
        self.floorunitTextBox.textField.delegate = self
 
        
        self.buildingNumberTextBox.textField.keyboardType = .numberPad
        self.streetTextBox.textField.returnKeyType = .next
        self.areaTextBox.textField.keyboardType = .alphabet
        self.floorunitTextBox.textField.returnKeyType = .next
    
        
    }
    
    @IBAction func applyDetails(_ sender: UIButton){
        
        CartManager.shared.deliveryInfo = [
            "building": self.buildingNumberTextBox.textField.text ?? "",
            "street": self.streetTextBox.textField.text ?? "",
            "area": self.areaTextBox.textField.text ?? "",
            "floor": self.floorunitTextBox.textField.text ?? "",
            "additional_instruction": self.textView.text ?? ""
        ]
        
//        Messages.showSuccessMessage(message: "Added")
        print(CartManager.shared.deliveryInfo)
    }
    
}

extension DeliveryDetailsViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.buildingNumberTextBox.textField{
            textField.resignFirstResponder()
            self.streetTextBox.textField.becomeFirstResponder()
        }else if textField == self.streetTextBox.textField{
            textField.resignFirstResponder()
            self.areaTextBox.textField.becomeFirstResponder()
        }else if textField == self.areaTextBox.textField{
            textField.resignFirstResponder()
            self.floorunitTextBox.becomeFirstResponder()
        }
        return true
    }
    
}
