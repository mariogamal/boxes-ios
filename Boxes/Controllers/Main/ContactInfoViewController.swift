//
//  ContactInfoViewController.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/24/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ContactInfoViewController: BaseViewController {
    
    @IBOutlet weak var contactLbl: UILabel!
    @IBOutlet weak var contactNoView: UIView!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var submit: UIButton!
    
    var fName : String = ""
    var lName : String = ""
    var emailAdd : String = ""
    var phone : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPhoneLabel()
        //        self.setScreen()
        //        self.setNavigationBar()
    }
    
    func setPhoneLabel(){
        if let user = UserManager.getUserObjectFromUserDefaults() {
            self.fName = user.firstName
            self.lName = user.lastName
            self.emailAdd = user.email
            self.contactLbl.text = user.phone
            self.phone = user.phone
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setScreen()
        configureNavigationBar()
    }
    
    func configureNavigationBar(){
        
        let font = UIFont(name: "Oswald-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 25)
        self.title = "contact info".localized.uppercased()
        self.navigationController?.hideNavigationBar(isHidden: false)
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarAttribute(font: font)
        self.navigationController?.navigationBar.tintColor = .white
        // self.placeBackButton(selectorBack: #selector(self.back))
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    func setScreen() {
        firstName.layer.cornerRadius = 5
        firstName.text = fName
        lastName.text = lName
        email.text = emailAdd
        self.firstName.delegate = self
        self.lastName.delegate = self
        self.email.delegate = self
        self.firstName.keyboardType = .alphabet
        self.firstName.returnKeyType = .next
        self.lastName.keyboardType = .alphabet
        self.lastName.returnKeyType = .next
        self.email.keyboardType = .emailAddress
        self.email.returnKeyType = .next
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ContactInfoViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    func isValidate() -> Bool {
        
        guard let firstName = self.firstName.text, !firstName.isEmpty else{
            Messages.showErrorMessage(message: "Write your first name".localized)
            return false
        }
        
        guard let lastName = self.lastName.text, !lastName.isEmpty else{
            Messages.showErrorMessage(message: "Write your last name".localized)
            return false
        }
        
        guard let email = self.email.text, !email.isEmpty else{
            Messages.showErrorMessage(message: "Write your email".localized)
            return false
        }
        
        guard email.isValid(regex: .email) else {
            Messages.showErrorMessage(message: "Write your email in correct form".localized)
            return false
        }
        
        saveContactInfo(firstName: firstName, lastName: lastName, email: email, phone: self.phone)
        return true
    }
    
    func saveContactInfo(firstName: String,lastName: String,email: String,phone: String){
        CartManager.shared.contactInfo = [
            "first_name": firstName,
            "last_name": lastName,
            "email": email,
            "phone": phone
        ]
        print(CartManager.shared.contactInfo)
    }
    
    @IBAction func submit(_ sender: Any) {
        if !isValidate() {
            return
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension ContactInfoViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.firstName{
            textField.resignFirstResponder()
            self.lastName.becomeFirstResponder()
        }else if textField == self.lastName{
            textField.resignFirstResponder()
            self.email.becomeFirstResponder()
        }
        else if textField == self.email{
            textField.resignFirstResponder()
        }
        return true
    }
}
