//
//  FoodTopBarViewController.swift
//  Boxes
//
//  Created by macmin on 12/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Tabman
import Pageboy
import SwiftyJSON
import MOLH
class FoodTopBarViewController: TabmanViewController {
   
    
    
    @IBOutlet weak var searchBar: UIView!
    
    private var viewControllers : [(String, FoodListingViewController)] = [
        ("MULTI".localized, FoodListingViewController.instantiate(fromAppStoryboard: .Main)),
        ("SINGLE".localized, FoodListingViewController.instantiate(fromAppStoryboard: .Main)),
    ]
    
   
    private var data : [ListingModel] = [] {
        didSet {
            print("data : [ListingModel] ")
            self.viewControllers.first?.1.data = self.data
            self.viewControllers.first?.1.tableView?.reloadData()
            self.viewControllers.last?.1.data = self.data
            self.viewControllers.last?.1.tableView?.reloadData()
        }
    }
    
    private var mainData : [ListingModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewControllers.first?.1.type = .multiRestaurant
        self.viewControllers.last?.1.type = .singleRestaurant
        
        self.view.addSubview(self.searchBar)
        
        self.dataSource = self
        
        let bar = TMBar.ButtonBar()
        bar.layout.transitionStyle = .progressive
        bar.layout.contentMode = .fit
        bar.layout.contentInset = UIEdgeInsets(top: 45, left: 6.0, bottom: 0.0, right: 6.0)
        bar.backgroundView.style = .flat(color: .white)
        bar.indicator.backgroundColor = AppColor.primary
        bar.scrollMode = .interactive
        
        bar.buttons.customize { (button) in
            button.font = UIFont(name: AppFont.Oswald_Regular, size: 12) ?? UIFont.boldSystemFont(ofSize: 12)
            button.tintColor = AppColor.secondary
            button.selectedTintColor = AppColor.primary
            button.frame = CGRect(x: 0, y: 0, width: bar.bounds.width/2, height: bar.bounds.height)
        }
        addBar(bar, dataSource: self, at: .top)

        self.viewControllers.first?.1.tabsView = bar.backgroundView
        self.viewControllers.first?.1.multiView = bar.buttons.all.first
        self.viewControllers.first?.1.singleView = bar.buttons.all.last
        
        self.serviceCalltypes()
        self.serviceCall()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        print("Debug: FoodTopBarViewController -> viewDidAppear")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        self.tabBarController?.title = "FOOD"
        
        self.tabBarController?.navigationItem.rightBarButtonItem = nil
        print("Debug: FoodTopBarViewController -> viewWillAppear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("Debug: FoodTopBarViewController -> viewWillDisappear")
    }
    
    func serviceCalltypes (){
        //subscription_type
        let parameter = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
            "subscription_type": "1"
        ]
        NetworkRequest.shared.types(parameters: parameter) { (response, error) in
            
            if error != nil {
                
                return
            }
            let types = JSON(response).arrayValue.map({ TypeModel.init(fromJson: $0) })
            self.viewControllers.first?.1.typeID = types.first?.id
            self.viewControllers.last?.1.typeID = types.last?.id
            
        }
    }
    
    func serviceCall() {
        
        let parameter : [String:String] = ["language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
                                           "user_latitude" : "25.36562",
                                           "user_longitude" : "65.5442",
        ]
        
        NetworkRequest.shared.listings(parameters: parameter) { (response, error) in
            if error != nil {
                return
            }
            let types = JSON(response).arrayValue.map({ ListingModel.init(fromJson: $0) })
            self.data = types
            self.mainData = types
        }
    }
    
    func searchServiceCall(_ keyword : String) {
        print(keyword)
        guard !keyword.isEmpty else {
            self.data = self.mainData
            return
        }
        self.data = self.mainData.filter { (item) -> Bool in
            return item.listingName.lowercased().contains(keyword.lowercased()) || item.listingCaption.lowercased().contains(keyword.lowercased())
        }

//        let parameter : [String:String] = ["language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
//                                           "keyword" : "\(keyword)",
//        ]
//
//        NetworkRequest.shared.search(parameters: parameter) { (response, error) in
//            if error != nil {
//                return
//            }
//            let types = JSON(response).arrayValue.map({ ListingModel.init(fromJson: $0) })
//            self.data = types
//        }
    }
    
}


extension FoodTopBarViewController : PageboyViewControllerDataSource, TMBarDataSource {
    
    func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
        let barItem = TMBarItem(title: self.viewControllers[index].0)
        return barItem
    }
    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return viewControllers.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController,
                        at index: PageboyViewController.PageIndex) -> UIViewController? {
        return viewControllers[index].1
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
    
}

extension FoodTopBarViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.searchServiceCall(textField.text ?? "")
    }
    
    
    
}
