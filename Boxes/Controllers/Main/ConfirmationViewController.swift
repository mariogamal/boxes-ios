//
//  ConfirmationViewController.swift
//  Boxes
//
//  Created by macmin on 17/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import SwiftyJSON
import MOLH
class ConfirmationViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var checkoutButton: CustomButton!
    
    var typeID: String?
    var data : [ListingModel] = []
    var scrollViewDidScroll :((UIScrollView) -> Void)?
    var emptyListingName = ""
    var deleteAction: ((ListingModel) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "CONFIRMATION".localized
        self.checkoutButton.setTitle("Checkout by KWD : \(self.calculateTotal())".localized, for: .normal)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: ItemTableViewCell.identifier, bundle: .main), forCellReuseIdentifier: ItemTableViewCell.identifier)
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        
    }
    
    func calculateTotal() -> Double {
        var total = 0.0
        for item in data {
            if item.packages.first != nil {
                total += item.packages.first!.priceValue * Double(item.weekCount)
            } else {
                emptyListingName = item.listingName
                Messages.showErrorMessage(message: "\(emptyListingName) doesn't support multiple subscription")
                return 0.0
            }
        }
        return total
    }
    
    @IBAction func checkoutButtonAction(_ sender: UIButton) {
        if UserManager.getUserObjectFromUserDefaults() != nil {
            guard self.data.count > 1 else{
                Messages.showErrorMessage(message: "Please select at least two restaurants")
                sender.shake()
                return
            }
            
            guard self.calculateTotal() > 0.0 else{
                Messages.showErrorMessage(message: "\(emptyListingName) doesn't support multiple subscription".localized)
                sender.shake()
                return
            }
            self.checkOutService()
        }
        else{
            let vc = LoginViewController.instantiate(fromAppStoryboard: .User)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func checkOutService() {
        guard UserManager.getUserObjectFromUserDefaults() != nil else {
            let vc = LoginViewController.instantiate(fromAppStoryboard: .User)
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        
        guard UserManager.getUserObjectFromUserDefaults()?.firstName != "" else {
            let vc = CreateProfileViewController.instantiate(fromAppStoryboard: .User)
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        let ids = self.data.reduce("") { (result, model) -> String in
            guard let id = model.id else { return result }
            return "\(result),\(id)"
        }
        
        let weeks = self.data.reduce("") { (result, model) -> String in
            let week = model.weekCount
            return "\(result),\(week)"
        }
        
        let packageIds = self.data.reduce("") { (result, model) -> String in
            guard let packageId = model.packages.first?.id else { return result }
            return "\(result),\(packageId)"
        }
        
        guard let typeId = self.typeID else{
            Messages.showErrorMessage(message: "ID not available".localized)
            return
        }
        
        var parameter : [String:String] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
            "listing_id" : String(ids.dropFirst()),
            "weekly" : String(weeks.dropFirst()),
            "package_id" : String(packageIds.dropFirst()),
            "type_id" : typeId,
            "card_holder_name" : "Sample",
            "card_number" : "123456789",
            "expiry_date" : "123123",
            "cvv" : "123",
            "Is_save" : "1",
            "state": "start"
        ]
        parameter.merge(UserManager.shared.cardInfo) { (a, b) -> String in
            return b
        }
        NetworkRequest.shared.userSubscription(parameters: parameter) { (response, error) in
            if let error = error {
                Messages.showErrorMessage(message: error.message)
                return
            }
            let stringURL = response as! String
            self.openPayment(stringURL: stringURL, parameter: parameter)
        }
    }
    
    func openPayment(stringURL: String, parameter : [String:String]) {
        let vc = PaymentViewController.instantiate(fromAppStoryboard: .Main)
        vc.stringUrl = stringURL
        vc.parameter = parameter
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //
    //    func getPackage() {
    //        guard let typeId = self.typeID else{
    //            Messages.showErrorMessage(message: "ID not available")
    //            return
    //        }
    //        let parameter : [String:String] = [
    //            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
    //            "type_id" : typeId
    //        ]
    //        NetworkRequest.shared.packages(parameters: parameter) { (response, error) in
    //            if error != nil {
    //                return
    //            }
    //            let types = JSON(response).arrayValue.map({ PackageModel.init(fromJson: $0) })
    //            if let packageId = types.first?.id {
    //                self.checkOutService(packageId: packageId)
    //            }
    //        }
    //    }
    //
    
    
    //    func serviceCall() {
    //
    //        guard let item = self.item else{
    //            return
    //        }
    //        let parameter : [String:String] = [
    //            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
    //            "listing_id" : item.id
    //        ]
    //        NetworkRequest.shared.menu(parameters: parameter) { (response, error) in
    //
    //            if error != nil {
    //                return
    //            }
    //            let types = JSON(response).arrayValue.map({ MenuModel.init(fromJson: $0) })
    //            self.data = types
    //            self.tableView.reloadData()
    //        }
    //    }
    
}

extension ConfirmationViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.data[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: ItemTableViewCell.identifier, for: indexPath) as! ItemTableViewCell
        cell.imgView.imageUrl = BASEIMAGEURL + item.image
        cell.titleLabel.textAlignment = MOLHLanguage.isArabic() ? .right : .left
        cell.titleLabel.text = item.listingName
        cell.subTitleLabel.textAlignment = MOLHLanguage.isArabic() ? .right : .left
        cell.subTitleLabel.text = item.listingCaption
        cell.weeksLabel.textAlignment = MOLHLanguage.isArabic() ? .right : .left
        cell.weeksLabel.text = "Weeks: \(item.weekCount)"
        cell.deleteAction = {
            self.deleteAction?(item)
            self.data.remove(at: indexPath.row)
            tableView.reloadData()
        }
        return cell
    }
    
    
}

extension ConfirmationViewController : UITableViewDelegate, UIScrollViewDelegate {
    
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //        let vc = ItemDetailsViewController.instantiate(fromAppStoryboard: .Main)
    //        self.showVC(vc)
    //    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.scrollViewDidScroll?(scrollView)
    }
    
}
