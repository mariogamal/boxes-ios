//
//  ItemTopBarViewController.swift
//  Boxes
//
//  Created by macmin on 10/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Tabman
import Pageboy
import SwiftyJSON

class ItemTopBarViewController: TabmanViewController {
    
    @IBOutlet weak var topSlidingView: SlidingView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var topSlidingViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    private let bar = TMBar.ButtonBar()
    
    var item : ListingModel?
    private var data : [ListingProductsModel] = []
    private var vcs : [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(self.topSlidingView)
        self.serviceCall()
        
        self.dataSource = self
        
        bar.layout.transitionStyle = .progressive
        bar.layout.contentMode = .fit
        bar.layout.contentInset = UIEdgeInsets(top: 140, left: 6.0, bottom: 0.0, right: 6.0)
        bar.backgroundView.style = .flat(color: .white)
        bar.indicator.backgroundColor = AppColor.primary
        bar.scrollMode = .interactive
        
        bar.buttons.customize { (button) in
            button.font = UIFont(name: AppFont.Oswald_Regular, size: 12) ?? UIFont.boldSystemFont(ofSize: 12)
            button.tintColor = AppColor.secondary
            button.selectedTintColor = AppColor.primary
//            button.frame = CGRect(x: 0, y: 0, width: bar.bounds.width/2, height: bar.bounds.height)
        }
        addBar(bar, dataSource: self, at: .top)
        
        self.setupData()
        self.setupSlidingView()
        self.applyGrad()
//        self.addRightBarButtonWithCartImage()
        self.addBadgeRightBarButtonWithCartImage(itemvalue: "\(CartManager.shared.data.count)")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
//        CartManager.shared.itemUpdateAction = {
//            print("CartManager.shared.cartItemCount: \(CartManager.shared.cartItemCount)")
//            self.addBadgeRightBarButtonWithCartImage(itemvalue: "\(CartManager.shared.cartItemCount)")
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addBadgeRightBarButtonWithCartImage(itemvalue: "\(CartManager.shared.cartItemCount)")
    }
    
    func setupData(){
        guard let item = self.item else{
            return
        }
        self.imgView.imageUrl = BASEIMAGEURL + item.image
        self.titleLabel.text = item.listingName
        self.subTitleLabel.text = item.listingCaption
    }
    
    public func addBadgeRightBarButtonWithCartImage(itemvalue: String) {
        let bagButton = BadgeButton()
        bagButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        bagButton.tintColor = UIColor.darkGray
        bagButton.setImage(#imageLiteral(resourceName: "cart"), for: .normal)
        bagButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
        bagButton.badge = itemvalue
        bagButton.addTarget(self, action: #selector(self.openCart), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: bagButton)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    public func addRightBarButtonWithCartImage(_ buttonImage: UIImage = #imageLiteral(resourceName: "cart")) {
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: buttonImage, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openCart))
        self.navigationItem.rightBarButtonItem = leftButton
    }
    
    func applyGrad (){
        let blackColor = UIColor.black.withAlphaComponent(0.7)
        self.gradientView.applyGradient(colours: [blackColor, .clear], cornerRadius: 6, startPoint: CGPoint(x: 0, y: 1), endPoint: CGPoint(x: 1, y: 0))
    }
    
    func setupSlidingView(){
        
        self.topSlidingView.onViewUpdate = {
            self.view.layoutIfNeeded()
        }
        self.topSlidingView.onOpenAction = {
            self.topSlidingViewHeightConstraint.constant = 0
            self.bar.layout.contentInset = UIEdgeInsets(top: 0.0, left: 6.0, bottom: 0.0, right: 6.0)
            self.navigationController?.title = "Restaurant Name".localized
        }
        self.topSlidingView.onCloseAction = {
            self.topSlidingViewHeightConstraint.constant = 140
            self.bar.layout.contentInset = UIEdgeInsets(top: 140, left: 6.0, bottom: 0.0, right: 6.0)
            self.navigationController?.title = ""
        }
    }
    
    func serviceCall() {
        
        guard let item = self.item else{
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        let parameter : [String:String] = ["language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c", "listing_id": item.id]
        NetworkRequest.shared.listing_categories_with_products(parameters: parameter) { (response, error) in
            if error != nil {
                return
            }
            let types = JSON(response).arrayValue.map({ ListingProductsModel.init(fromJson: $0) })
            self.data = types
            self.vcs = types.map { (_) -> UIViewController in
                return ItemListingViewController.instantiate(fromAppStoryboard: .Main)
            }
            self.reloadData()
        }
    }
    
}


extension ItemTopBarViewController : PageboyViewControllerDataSource, TMBarDataSource {
    
    func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
        let barItem = TMBarItem(title: self.data[index].listingCatName)
        return barItem
    }
    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return self.data.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController,
                        at index: PageboyViewController.PageIndex) -> UIViewController? {
        let vc = self.vcs[index] as! ItemListingViewController
        vc.item = self.data[index]
        return vc
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
    
}
