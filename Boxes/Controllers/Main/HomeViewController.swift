//
//  HomeViewController.swift
//  Boxes
//
//  Created by MacAir on 03/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import SwiftyJSON

class HomeViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textField: UITextField!
    
    var item : TypeModel?
    private var data : [ListingModel] = []
    private var mainData : [ListingModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.register(UINib(nibName: HomeTableViewCell.identifier, bundle: .main), forCellReuseIdentifier: HomeTableViewCell.identifier)
        self.tableView.allowsMultipleSelection = false
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        
        self.serviceCall()
        self.textField.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        print("Debug: HomeViewController -> viewDidAppear")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.addBadgeRightBarButtonWithCartImage(itemvalue: "\(CartManager.shared.data.count)")
        CartManager.shared.itemUpdateAction = {
            print("CartManager.shared.cartItemCount: \(CartManager.shared.cartItemCount)")
            self.addBadgeRightBarButtonWithCartImage(itemvalue: "\(CartManager.shared.cartItemCount)")
        }
        
        print("Debug: HomeViewController -> viewWillAppear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("Debug: HomeViewController -> viewWillDisappear")
    }
    
    func filterData(with keyword : String){
        guard !keyword.isEmpty else {
            self.data = self.mainData
            self.tableView.reloadData()
            return
        }
        self.data = self.mainData.filter { (item) -> Bool in
            return item.listingName.lowercased().contains(keyword.lowercased()) || item.listingCaption.lowercased().contains(keyword.lowercased())
        }
        self.tableView.reloadData()
    }
    
    func serviceCall() {
        
        var parameter : [String:String] = ["language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
                                           "user_latitude" : "25.36562",
                                           "user_longitude" : "65.5442",
        ]
        if let item = self.item {
            parameter["type_id"] = item.id
        }
        
        NetworkRequest.shared.listings(parameters: parameter) { (response, error) in
            if error != nil {
                return
            }
            let types = JSON(response).arrayValue.map({ ListingModel.init(fromJson: $0) })
            self.data = types
            self.mainData = types
            self.tableView.reloadData()
        }
    }
    
}

extension HomeViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = data.count
        if count == 0 {
            tableView.addPlaceholderImage()
        }else {
            tableView.removePlaceholderImage()
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = self.data[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: HomeTableViewCell.identifier, for: indexPath) as! HomeTableViewCell
        cell.imgView.imageUrl = BASEIMAGEURL + item.image
        cell.titleLabel.text = item.listingName
        cell.subTitleLabel.text = item.listingCaption
        return cell
    }
    
}

extension HomeViewController : UITableViewDelegate, UIScrollViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = ItemTopBarViewController.instantiate(fromAppStoryboard: .Main)
        vc.item = self.data[indexPath.row]
        vc.title = (self.item?.typeName ?? "Listing").uppercased()
        self.showVC(vc)
        
    }
    
    //    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //        let offsetY = self.tableView.contentOffset.y
    //        for cell in self.tableView.visibleCells as! [HomeTableViewCell] {
    //            let x = cell.imgView.frame.origin.x
    //            let w = cell.imgView.bounds.width
    //            let h = cell.imgView.bounds.height
    //            let y = ((offsetY - cell.frame.origin.y) / h) * 25
    //            cell.imgView.frame = CGRect(x: x, y: y, width: w, height: h)
    //        }
    //    }
    
}

extension HomeViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.filterData(with: (textField.text ?? "") + string)
        print(string)
        print(textField.text ?? "nothing".localized)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.filterData(with: textField.text ?? "")
    }
    
    
}
