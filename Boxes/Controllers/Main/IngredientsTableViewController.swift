//
//  IngredientsTableViewController.swift
//  Boxes
//
//  Created by Mario Gamal on 10/20/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class IngredientsTableViewController: UITableViewController {

    var ingredients = [Ingredient]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Ingredients"
        self.tableView.tableFooterView = UIView()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ingredients.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = ingredients[indexPath.row].ingName
        return cell
    }

}
