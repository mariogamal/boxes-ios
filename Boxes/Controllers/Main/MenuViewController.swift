//
//  MenuViewController.swift
//  Boxes
//
//  Created by macmin on 15/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import SwiftyJSON
import ExpandableCell

class MenuViewController: BaseViewController {
    
    @IBOutlet weak var tableView: ExpandableTableView!
    
    var item : ListingModel?
    var data : [MenuModel] = []
    
    var scrollViewDidScroll :((UIScrollView) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        self.tableView.dataSource = self
//        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: FoodTableViewCell.identifier, bundle: .main), forCellReuseIdentifier: FoodTableViewCell.identifier)
        tableView.register(UINib(nibName: FAQTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: FAQTableViewCell.identifier)
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        
        tableView.expandableDelegate = self
        tableView.autoReleaseDelegate = false
        tableView.animation = .automatic
        tableView.expansionStyle = .multi
        
        
        self.serviceCall()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func serviceCall() {
            
        guard let item = self.item else{
            return
        }

        let parameter : [String:String] = [
                         "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
                         "listing_id" : item.id
        ]
        
        NetworkRequest.shared.showMenu(parameters: parameter) { (response, error) in
            
            if error != nil {
                return
            }
            let types = JSON(response).arrayValue.map({ MenuModel.init(fromJson: $0) })
            self.data = types.filter { (model) -> Bool in
                if !model.products.isEmpty {
                    return true
                }
                return false
            }
            self.tableView.reloadData()
        }
    }

}

extension MenuViewController : ExpandableDelegate {
    
    func numberOfSections(in tableView: ExpandableTableView) -> Int {
        return 1
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCellsForRowAt indexPath: IndexPath) -> [UITableViewCell]? {
        var cells = [FoodTableViewCell]()
        
        for item in self.data[indexPath.row].products {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: FoodTableViewCell.identifier, for: indexPath) as! FoodTableViewCell
            cell.imgView.imageUrl = BASEIMAGEURL + item.image
            cell.titleLabel.text = item.productName
            cell.subTitleLabel.text = item.captionName
            cell.caloriesLabel.text = "\(item.calories ?? 0)"
            cell.proteinLabel.text = "\(item.proteins ?? 0)"
            cell.carbsLabel.text = "\(item.carbohydrates ?? 0)"
            cell.ratingLabel.text = item.rating
            cell.ingredientsList = item.ingredients
            cells.append(cell)
        }
        return cells
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = expandableTableView.dequeueReusableCell(withIdentifier: FAQTableViewCell.identifier) as! FAQTableViewCell
        cell.titleLabel.text = self.data[indexPath.row].menuCatName.uppercased()
        cell.rightMargin = 32
        
        return cell
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
        
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightsForExpandedRowAt indexPath: IndexPath) -> [CGFloat]? {
        var heights = [CGFloat]()
        
        self.data[indexPath.row].products.forEach({ (_) in
            heights.append(UITableView.automaticDimension)
        })
        
        return heights
    }
    
    
    @objc(expandableTableView:didCloseRowAt:) func expandableTableView(_ expandableTableView: UITableView, didCloseRowAt indexPath: IndexPath) {
        let cell = expandableTableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
        cell?.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
    }
    
    func expandableTableView(_ expandableTableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func expandableTableView(_ expandableTableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        //        let cell = expandableTableView.cellForRow(at: indexPath)
        //        cell?.contentView.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        //        cell?.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
    }
    
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCell: UITableViewCell, didSelectExpandedRowAt indexPath: IndexPath) {
        let cell = expandedCell as! FoodTableViewCell
        let vc = IngredientsTableViewController.instantiate(fromAppStoryboard: .Main)
        vc.ingredients = cell.ingredientsList
        self.showVC(vc)

    }
    
    
}

//extension MenuViewController : UITableViewDataSource {
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.data[section].products.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let item = self.data[indexPath.section].products[indexPath.row]
//        let cell = tableView.dequeueReusableCell(withIdentifier: FoodTableViewCell.identifier, for: indexPath) as! FoodTableViewCell
//        cell.imgView.imageUrl = BASEIMAGEURL + item.image
//        cell.titleLabel.text = item.productName
//        cell.subTitleLabel.text = item.captionName
//        return cell
//    }
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return self.data.count
//    }
//
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return self.data[section].products.isEmpty ? nil : self.data[section].menuCatName.uppercased()
//    }
//}
//
//extension MenuViewController : UITableViewDelegate, UIScrollViewDelegate {
//
//
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        self.scrollViewDidScroll?(scrollView)
//    }
//
//}
