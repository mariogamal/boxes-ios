//
//  FoodListingViewController.swift
//  Boxes
//
//  Created by macmin on 17/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import SwiftyJSON
import MOLH
import Instructions

class FoodListingViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var checkoutButton: CustomButton!
    @IBOutlet weak var countView: UIView!
    
    var typeID : String?
    var type : FoodType = .singleRestaurant
    var data : [ListingModel] = []
    var weekCount = 0 {
        didSet {
            self.countLabel?.text = "\(self.weekCount)/4"
        }
    }
    var footer : TableViewFooterButton?
    var selectedItems = Set<ListingModel>()
    
    let coachMarksController = CoachMarksController()
    var tabsView: UIView?
    var multiView: UIView?
    var singleView: UIView?
    var itemCountView: UIView?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        if self.type == .multiRestaurant {
            self.tableView.register(UINib(nibName: MultiSubscriptionTableViewCell.identifier, bundle: .main), forCellReuseIdentifier: MultiSubscriptionTableViewCell.identifier)
//            self.tableView.register(UINib(nibName: TableViewFooterButton.identifier, bundle: .main), forHeaderFooterViewReuseIdentifier: TableViewFooterButton.identifier)
            self.tableView.allowsMultipleSelection = false
            self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        }else{
            self.tableView.register(UINib(nibName: HomeTableViewCell.identifier, bundle: .main), forCellReuseIdentifier: HomeTableViewCell.identifier)
            self.tableView.allowsMultipleSelection = false
            self.tableView.tableHeaderView = nil
            self.checkoutButton.isHidden = true
            self.countView.isHidden = true
        }
        initInstructions()
    }
    
    private func initInstructions() {
        self.coachMarksController.dataSource = self
        let skipView = CoachMarkSkipDefaultView()
        skipView.setTitle("Skip".localized, for: .normal)
        self.coachMarksController.skipView = skipView
        self.coachMarksController.statusBarVisibility  = .visible
        self.coachMarksController.overlay.isUserInteractionEnabled = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if SplashViewController.VCIndex == 0 && !UserManager.getInstructionShown() {
            self.coachMarksController.start(in: .window(over : self))
            UserManager.saveInstructionShown(instrctionsShown: true)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        if !UserManager.getInstructionShown() {
            self.coachMarksController.stop(immediately: true)
        }
    }
    
    @IBAction func checkoutButtonAction(_ sender: Any) {
        self.fillSelectedResturants()
        guard self.selectedItems.count > 1 else{
            Messages.showErrorMessage(message: "Please select at least two restaurants".localized)
            return
        }
        DispatchQueue.main.async {
            let vc = ConfirmationViewController.instantiate(fromAppStoryboard: .Main)
            vc.data = self.selectedItems.map({ $0 })
            vc.deleteAction = { item in
                if let index = self.data.firstIndex(of: item) {
                    if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? MultiSubscriptionTableViewCell {
                        cell.selectItem(selected: false)
                    }
                }
                self.selectedItems.remove(item)
            }
            vc.typeID = self.typeID
            self.showVC(vc)
        }
    }
    
    func fillSelectedResturants() {
        selectedItems.removeAll()
        for item in data {
            if item.weekCount > 0 {
                selectedItems.insert(item)
            }
        }
    }
    
}

extension FoodListingViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = data.count
        if count == 0 {
            tableView.addPlaceholderImage()
        }else {
            tableView.removePlaceholderImage()
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = self.data[indexPath.row]
        
        if self.type == .multiRestaurant {
            let cell = tableView.dequeueReusableCell(withIdentifier: MultiSubscriptionTableViewCell.identifier, for: indexPath) as! MultiSubscriptionTableViewCell
            cell.imgView.imageUrl = BASEIMAGEURL + item.image
            cell.titleLabel.text = item.listingName
            cell.subTitleLabel.text = item.listingCaption
            cell.countLabel.text = String(item.weekCount)
            cell.plusAction = {
                if self.weekCount < 4 {
                    self.data[indexPath.row].weekCount += 1
                    self.weekCount += 1
                    tableView.reloadData()
                }
            }
            cell.minusAction = {
                if self.weekCount > 0 && item.weekCount > 0{
                    self.data[indexPath.row].weekCount -= 1
                    self.weekCount -= 1
                    tableView.reloadData()
                }
            }
            
            if self.selectedItems.contains(item) {
                cell.selectItem(selected: true)
            }else{
                cell.selectItem(selected: false)
            }
            
            if itemCountView == nil {
                itemCountView = cell.countLabel
            }

            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: HomeTableViewCell.identifier, for: indexPath) as! HomeTableViewCell
        cell.imgView.imageUrl = BASEIMAGEURL + item.image
        cell.titleLabel.text = item.listingName
        cell.subTitleLabel.text = item.listingCaption
        
        return cell
    }
    
    private func showMenu(item: ListingModel) {
        DispatchQueue.main.async {
            let vc = RestaurantTopbarViewController.instantiate(fromAppStoryboard: .Main)
            vc.item = item
            vc.title = "MULTI".localized
            
            vc.showSubscription = false
            vc.typeID = self.typeID
            self.showVC(vc)
        }
    }
    
}

extension FoodListingViewController : UITableViewDelegate, UIScrollViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.type == .singleRestaurant {
            let vc = RestaurantTopbarViewController.instantiate(fromAppStoryboard: .Main)
            vc.item = self.data[indexPath.row]
            vc.title = "SINGLE".localized
                
            
          
        
            vc.typeID = self.typeID
            self.showVC(vc)
        }else{
            showMenu(item: self.data[indexPath.row])
//            tableView.deselectRow(at: indexPath, animated: true)
//            let item = self.data[indexPath.row]
//            guard let cell = tableView.cellForRow(at: indexPath) as? MultiSubscriptionTableViewCell else{ return }
//            guard self.selectedItems.contains(item) else {
//                guard self.selectedItems.count < 4 else{
//                    return
//                }
//                self.selectedItems.insert(item)
//                cell.selectItem(selected: true)
//                return
//            }
//            self.selectedItems.remove(item)
//            cell.selectItem(selected: false)
        }
    }

    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return ((tableView.indexPathsForSelectedRows?.count ?? 0) < 4) ? indexPath : nil
    }
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        if self.type == .multiRestaurant {
//
//            if self.footer == nil {
//                self.footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: TableViewFooterButton.identifier) as? TableViewFooterButton
//            }
//
//            if let footer = self.footer {
//                footer.button.setTitle("CONFIRM", for: .normal)
//                footer.action = {
//                    DispatchQueue.main.async {
//                        let vc = ConfirmationViewController.instantiate(fromAppStoryboard: .Main)
//                        vc.data = (tableView.indexPathsForSelectedRows ?? []).map { (indexPath) -> ListingModel in
//                            return self.data[indexPath.row]
//                        }
//                        self.showVC(vc)
//                    }
//                    print("footer")
//                }
//                return footer
//            }
//        }
//        return nil
//    }
//
//    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
//        return (self.type == .multiRestaurant) ? 50 : 0
//    }
    
}

extension FoodListingViewController:CoachMarksControllerDataSource{
    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkViewsAt index: Int, madeFrom coachMark: CoachMark) -> (bodyView: (UIView & CoachMarkBodyView), arrowView: (UIView & CoachMarkArrowView)?) {
        let coachViews = coachMarksController.helper.makeDefaultCoachViews(
            withArrow: true,
            arrowOrientation: coachMark.arrowOrientation
        )
        switch index {
        case 0:
            coachViews.bodyView.hintLabel.text = "You can choose your subcription type from here".localized
            coachViews.bodyView.nextLabel.text = "Next".localized
        case 1:
            coachViews.bodyView.hintLabel.text = "For multi subscription you can have one month from more than a resturant".localized
            coachViews.bodyView.nextLabel.text = "Next".localized
        case 2:
            coachViews.bodyView.hintLabel.text = "This is the weeks count that you can choose from this resturant".localized
            coachViews.bodyView.nextLabel.text = "Next".localized
        case 3:
            coachViews.bodyView.hintLabel.text = "Total number of weeks will be displayed here".localized
            coachViews.bodyView.nextLabel.text = "Next".localized
        case 4:
            coachViews.bodyView.hintLabel.text = "To subscribe for one resturant choose packages from here".localized
            coachViews.bodyView.nextLabel.text = "Done".localized
        default:
            coachViews.bodyView.hintLabel.text = "To subscribe for one resturant choose packages from here".localized
            coachViews.bodyView.nextLabel.text = "Next".localized
        }
        
        return (bodyView: coachViews.bodyView, arrowView: coachViews.arrowView)
    }
    
    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkAt index: Int) -> CoachMark {
        switch index {
        case 0:
            return coachMarksController.helper.makeCoachMark(for : tabsView)
        case 1:
            return coachMarksController.helper.makeCoachMark(for : multiView)
        case 2:
            return coachMarksController.helper.makeCoachMark(for : itemCountView)
        case 3:
            return coachMarksController.helper.makeCoachMark(for : countLabel)
        case 4:
            return coachMarksController.helper.makeCoachMark(for : singleView)
        default:
            return coachMarksController.helper.makeCoachMark(for : singleView)
        }
    }
    
    func numberOfCoachMarks(for coachMarksController: CoachMarksController) -> Int {
        return 5
    }
}
