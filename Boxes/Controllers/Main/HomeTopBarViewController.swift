//
//  HomeTopBarViewController.swift
//  Boxes
//
//  Created by macmin on 10/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Tabman
import Pageboy
import SwiftyJSON
import Kingfisher
import MOLH

enum FoodType {
    case singleRestaurant
    case multiRestaurant
}

class HomeTopBarViewController: TabmanViewController {
    
    private var data : [TypeModel] = []
    private var vcs : [UIViewController] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoadingOverlay.shared.showOverlay()
        self.serviceCall()
        
        //        let vc2 = HomeViewController.instantiate(fromAppStoryboard: .Main)
        //        let vc3 = HomeViewController.instantiate(fromAppStoryboard: .Main)
        //        let vc4 = HomeViewController.instantiate(fromAppStoryboard: .Main)
        //        let vc5 = HomeViewController.instantiate(fromAppStoryboard: .Main)
        //        let vc6 = HomeViewController.instantiate(fromAppStoryboard: .Main)
        
        //        self.data = [
        //            ("Flowers", #imageLiteral(resourceName: "restaurant-icon_active"), vc2),
        //            ("Groceries", #imageLiteral(resourceName: "restaurant-icon_active"), vc3),
        //            ("Health", #imageLiteral(resourceName: "restaurant-icon_active"), vc4),
        //            ("Cosmetics", #imageLiteral(resourceName: "restaurant-icon_active"), vc5),
        //            ("Pet", #imageLiteral(resourceName: "restaurant-icon_active"), vc6),
        //        ]
        self.dataSource = self
        self.delegate = self
        
        let bar = TMBar.TabBar()
        bar.layout.transitionStyle = .progressive
        bar.layout.contentInset = UIEdgeInsets(top: 0, left: 0.0, bottom: 4.0, right: 0.0)
        bar.backgroundView.style = .flat(color: .white)
        bar.indicator.backgroundColor = .clear
        //        bar.indicator. = TMLineBarIndicator.Weight.light
        
        bar.scrollMode = .interactive
        bar.borderWidth = 1
        bar.borderColor = UIColor.groupTableViewBackground
        
        bar.buttons.customize { (button) in
            button.font = UIFont(name: AppFont.Oswald_Regular, size: 12) ?? UIFont.boldSystemFont(ofSize: 12)
            button.tintColor = AppColor.secondary
            button.selectedTintColor = AppColor.primary
            //            button.shrinksImageWhenUnselected = false
            button.imageViewSize = CGSize(width: 40, height: 40)
            button.imageContentMode = .scaleAspectFill
            button.width = 50
        }
//        addBar(bar, dataSource: self, at: .top)
        
        self.tabBarController?.navigationItem.title = "BOXES"
        self.addLeftBarButtonWithMenuImage()
        
        self.tabBarController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
//        CartManager.shared.itemUpdateAction = {
//            print("CartManager.shared.cartItemCount: \(CartManager.shared.cartItemCount)")
//            self.addBadgeRightBarButtonWithCartImage(itemvalue: "\(CartManager.shared.cartItemCount)")
//        }
        
    }
    
    public func addBadgeRightBarButtonWithCartImage(itemvalue: String) {
        let bagButton = BadgeButton()
        bagButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        bagButton.tintColor = UIColor.darkGray
        bagButton.setImage(#imageLiteral(resourceName: "cart"), for: .normal)
        bagButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
        bagButton.badge = itemvalue
        bagButton.addTarget(self, action: #selector(self.openCart), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: bagButton)
        self.tabBarController?.navigationItem.rightBarButtonItem = barButton
    }
    
    public func addRightBarButtonWithCartImage(_ buttonImage: UIImage = #imageLiteral(resourceName: "cart")) {
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: buttonImage, style: UIBarButtonItem.Style.plain, target: self, action: #selector(super.openCart))
        self.tabBarController?.navigationItem.rightBarButtonItem = leftButton
    }
    
    public func addLeftBarButtonWithMenuImage(_ buttonImage: UIImage = #imageLiteral(resourceName: "menu_icon")) {
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: buttonImage, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openMenu))
        self.tabBarController?.navigationItem.leftBarButtonItem = leftButton
    }
    
    
    
    @objc public func openMenu() {
        if MOLHLanguage.currentAppleLanguage() == "en" {
            self.sideMenuController?.showLeftViewAnimated()
            
        }
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            self.sideMenuController?.showRightViewAnimated()
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.tabBarController?.title = "BOXES"
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 36, height: 36))
        let image = #imageLiteral(resourceName: "tab_menu").getRoundedImage(view: view, radius: 18, borderWidth: 0)
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        self.tabBarController?.navigationItem.titleView = imageView
        //        self.addRightBarButtonWithCartImage()
//        self.addBadgeRightBarButtonWithCartImage(itemvalue: "\(CartManager.shared.data.count)")

    }
    
    func serviceCall (){
        //subscription_type
        let parameter = ["language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"]
        NetworkRequest.shared.types(parameters: parameter) { (response, error) in
            
            if error != nil {
                
                return
            }
            let types = JSON(response).arrayValue.map({ TypeModel.init(fromJson: $0) })
            self.data = types
            self.vcs = types.map { (_) -> UIViewController in
                return HomeViewController.instantiate(fromAppStoryboard: .Main)
            }
            self.data.insert(TypeModel(fromJson: JSON([:])), at: 0)
            self.vcs.insert(FoodTopBarViewController.instantiate(fromAppStoryboard: .Main), at: 0)
            self.reloadData()
        }
    }
    
    func getImageFromURL(with urlString : String, completion : @escaping ((UIImage) -> Void)){
        DispatchQueue.main.async {
            guard let url = URL.init(string: BASEIMAGEURL + urlString) else {
                return
            }
            let resource = ImageResource(downloadURL: url)
            KingfisherManager.shared.retrieveImage(with: resource) { (result) in
                switch result {
                case .success(let value):
                    completion(value.image)
                    return
                case .failure(let error):
                    print("Error: \(error)")
                    completion(UIImage())
                    return
                }
            }
        }
    }
    
}


extension HomeTopBarViewController : PageboyViewControllerDataSource, TMBarDataSource {
    
    func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
        if index == 0 {
            let barItem = TMBarItem(title: "Food".localized)
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
            barItem.image = #imageLiteral(resourceName: "foodIcon").getRoundedImage(view: view, radius: 25, borderWidth: 0)
            return barItem
        }
        let item = self.data[index]
        let barItem = TMBarItem(title: item.typeName )
        self.getImageFromURL(with: item.image) { (image) in
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
            barItem.image = image.getRoundedImage(view: view, radius: 25, borderWidth: 0)
        }
        return barItem
    }
    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return self.data.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController,
                        at index: PageboyViewController.PageIndex) -> UIViewController? {
        print("Debug: index = \(index)")
        
        if index == 0 {
            let vc = self.vcs[index] as! FoodTopBarViewController
            return vc
        }
        let vc = self.vcs[index] as! HomeViewController
        vc.item = self.data[index]
        return vc
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return .first
    }
    
    
}
