//
//  OrderConfirmedViewController.swift
//  Boxes
//
//  Created by macmin on 11/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class OrderConfirmedViewController: BaseViewController {

    @IBOutlet weak var phoneNumber: UILabel!
    
    @IBOutlet weak var address: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let user = UserManager.getUserObjectFromUserDefaults()
        if let user = user {
            phoneNumber.text = user.phone
            address.text = user.address
        }
    }
    
    @IBAction func doneButtonAction(){
        self.dismiss(animated: true, completion: nil)
        self.openTabController()
    }

    func openTabController() {
        SplashViewController.VCIndex = 1
        CartManager.shared.getCartItems()
        let vc = BaseLeftMenuViewController.instantiate(fromAppStoryboard: .LeftMenu)
        vc.leftViewPresentationStyle = .slideBelow
        UIWINDOW?.rootViewController = vc
    }
}


