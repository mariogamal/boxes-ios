//
//  HomeTableViewCell.swift
//  Boxes
//
//  Created by macmin on 10/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit


class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var imgWrapperView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.applyGrad()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.applyGrad()
    }
    
    func applyGrad (){
        let blackColor = UIColor.black.withAlphaComponent(0.9)
        self.gradientView.applyGradient(colours: [blackColor, .clear], cornerRadius: 6, startPoint: CGPoint(x: 0, y: 1), endPoint: CGPoint(x: 1, y: 0))
    }
    
}
