//
//  MultiSubscriptionTableViewCell.swift
//  Boxes
//
//  Created by macmin on 12/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class MultiSubscriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var imgWrapperView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    var plusAction : (() -> Void)?
    var minusAction : (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.applyGrad()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func selectItem(selected: Bool) {
//        if selected {
//            self.imgWrapperView.borderColor = AppColor.primary
//            self.imgWrapperView.borderWidth = 6
//        }else{
//            self.imgWrapperView.borderWidth = 0
//        }
    }
    
    
    @IBAction func plusPressed(_ sender: UIButton) {
        self.plusAction?()
    }
    
    
    @IBAction func minusPressed(_ sender: UIButton) {
        self.minusAction?()
    }
    
    func applyGrad (){
        let blackColor = UIColor.black.withAlphaComponent(0.9)
        self.gradientView.applyGradient(colours: [blackColor, .clear], cornerRadius: 6, startPoint: CGPoint(x: 0, y: 1), endPoint: CGPoint(x: 1, y: 0))
    }

}
