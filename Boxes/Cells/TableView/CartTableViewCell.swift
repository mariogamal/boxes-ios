//
//  CartTableViewCell.swift
//  Boxes
//
//  Created by macmin on 24/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    var minusAction: ((Int) -> Void)?
    var plusAction: ((Int) -> Void)?
    var deleteAction: (() -> Void)?
    
    var item : CartItemModel? {
        didSet {
            self.setup(item: self.item)
        }
    }
    
    var count = 1 {
        didSet {
            self.countLabel.text = "\(self.count)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(item: CartItemModel?){
        priceLabel.text = item?.price ?? "N/A".localized
        nameLabel.text = item?.productName ?? "N/A".localized
        self.count = item?.quantity ?? 1
//        count = 1
    }
    
    @IBAction func minusButtonAction(_ sender: UIButton) {
        if count > 1 {
            count -= 1
            minusAction? (count)
        }else{
            self.deleteAction?()
        }
    }
    
    @IBAction func plusButtonAction(_ sender: UIButton) {
        if count < self.item?.stockCount ?? 99 {
            count += 1
            plusAction? (count)
        }
    }
}
