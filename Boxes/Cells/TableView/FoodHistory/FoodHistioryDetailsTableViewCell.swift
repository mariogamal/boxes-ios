//
//  FoodHistioryDetailsTableViewCell.swift
//  Boxes
//
//  Created by macmin on 26/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class FoodHistioryDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var carbsLabel: UILabel!
    @IBOutlet weak var proteinLabel: UILabel!

    var addReveiwAction : (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func addReview(_ sender: UIButton){
        self.addReveiwAction?()
    }
    
}
