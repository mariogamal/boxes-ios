//
//  FoodTableViewCell.swift
//  Boxes
//
//  Created by macmin on 31/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class FoodTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var carbsLabel: UILabel!
    @IBOutlet weak var proteinLabel: UILabel!
    @IBOutlet weak var selectionImageView: UIImageView!
    
    var ingredientsList =  [Ingredient]()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func selectItem(selected : Bool){
        
       if !self.selectionImageView.isHidden {
           self.selectionImageView.image = selected ? #imageLiteral(resourceName: "white_check_box_active") : #imageLiteral(resourceName: "white_check_box_unactive")
        }
    }
    
    func enableSelection(){
        self.selectionImageView.isHidden = false
    }
    
}
