//
//  ItemTableViewCell.swift
//  Boxes
//
//  Created by macmin on 15/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var selectionImageView: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var weeksLabel: UILabel!
    
    var deleteAction : (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

//        self.selectItem(selected: selected)
    }
    
    func selectItem(selected : Bool){
        if !self.selectionImageView.isHidden {
            self.selectionImageView.image = selected ? #imageLiteral(resourceName: "white_check_box_active") : #imageLiteral(resourceName: "white_check_box_unactive")
        }
    }
    
    func enableSelection(){
        self.selectionImageView.isHidden = false
    }
    
    func enableDeletion(){
        self.deleteButton.isHidden = false
    }
    
    @IBAction func deleteButtonAction(_ sender: UIButton) {
        self.deleteAction?()
    }
    
    
}
