//
//  PackageDetailsCell.swift
//  Boxes
//
//  Created by Mario Gamal on 26/01/2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class PackageDetailsCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var categoryValueLabel: UILabel!
    
    var itemId: String = "-1"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
}
