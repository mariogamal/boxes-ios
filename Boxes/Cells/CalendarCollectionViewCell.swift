//
//  CalendarCollectionViewCell.swift
//  Boxes
//
//  Created by macmin on 19/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import PieCharts

class CalendarCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var pieChart: PieChart!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var resturantImage: UIImageView! //by client change 16 March
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pieChart.delegate = self
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        titleLabel.text = ""
        subTitleLabel.text = ""
        resturantImage.image = nil
        pieChart.clear()
        pieChart.animDuration = 0
    }
    
    func createModels(selected: Int, total: Int){
        var models = [PieSliceModel]()
        if total == 0 {
//            models.append(PieSliceModel(value: 1, color: UIColor.black.withAlphaComponent(0.2) ))
            pieChart.backgroundColor = UIColor.black.withAlphaComponent(0.2)
            pieChart.models = []
            pieChart.clear()
            return
        }else{
            pieChart.backgroundColor = UIColor.clear
            for i in 1 ... total {
                models.append(PieSliceModel(value: 1, color: (i <= selected) ? AppColor.primary : UIColor.black.withAlphaComponent(0.2) ))
            }
        }
        pieChart.models = models
    }
    
    func setup(title: String, subTitle: String, image: String){ //by client change 16 March
        self.titleLabel.text = "   \(title)   "
        self.subTitleLabel.text = "   \(subTitle)   "
        self.resturantImage.imageUrl = BASEIMAGEURL + image //by client change 16 March
    }
    
    func setSelected(_ selected: Bool){
        if selected {
            self.titleLabel.backgroundColor = AppColor.primary
            self.subTitleLabel.backgroundColor = AppColor.primary
            self.titleLabel.textColor = UIColor.white
            self.subTitleLabel.textColor = UIColor.white
        }else {
            self.titleLabel.backgroundColor = UIColor.clear
            self.subTitleLabel.backgroundColor = UIColor.clear
            self.titleLabel.textColor = UIColor.lightGray
            self.subTitleLabel.textColor = UIColor.lightGray
        }
    }
    

}

extension CalendarCollectionViewCell : PieChartDelegate {
    
    func onSelected(slice: PieSlice, selected: Bool) {
        print(selected)
    }
    
    
}
