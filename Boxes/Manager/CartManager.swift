//
//  CartManager.swift
//  Boxes
//
//  Created by macmin on 25/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import SwiftyJSON

extension Notification.Name {
    static let cartLoaded = Notification.Name("CartNotification")
    static let cartItemAdded = Notification.Name("CartItemAdded")
    static let cartItemRemoved = Notification.Name("CartItemRemoved")
    static let orderPlaced = Notification.Name("OrderPlaced")
    static let voucherApplied = Notification.Name("VoucherApplied")
    
    
    static let cartItemAddedError = Notification.Name("CartItemAddedError")
    static let voucherAppliedError = Notification.Name("voucherAppliedError")
}

class CartManager {
    
    static var shared = CartManager()
    
    var item : CartModel?
    var data = [CartItemModel]()
    
//    var cartItems : [String] = []
    
    let cartNotification = NSNotification.Name("CartNotification")
    let cartItemAdded = NSNotification.Name("CartItemAdded")
    
    var deliveryInfo : [String:String] = [:]
    var contactInfo : [String:String] = [:]
    
    var itemUpdateAction : (() -> Void)?
    
    var cartItemCount : Int {
        return self.data.count
    }
    
    private init() {
        print("CartManager init")
    }
    
    deinit {
        print("CartManager deinit")
    }
    
    func getCartItems() {
        
        let parameter : [String:String] = ["language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",]
        NetworkRequest.shared.myCart(parameters: parameter) { (response, error) in
            if let err = error {
                print(err.message)
//                self.showError(error: err)
                return
            }
            let item = CartModel(fromJson: JSON(response))
            self.item = item
            self.data = item.cartItems
//            self.cartItems.removeAll()
            NotificationCenter.default.post(name: .cartLoaded, object: nil)
            self.itemUpdateAction?()
        }
    }
    
    func newAddToCart(id: String, quantity: Int) { // new
        let parameter : [String : Any] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
            "product_id": id,
            "quantity": quantity]
        
        NetworkRequest.shared.newAddCartItem(parameters: parameter) { (response, error) in
            if let err = error {
                print(err.message)
                if !err.userInfo.isEmpty {
                    if let vendorError = err.userInfo["vendor_error"] {
                        let alert = UIAlertController.init(title: nil, message: vendorError, preferredStyle: .alert)
                        let action1 = UIAlertAction.init(title: "YES".localized, style: .default, handler: { (action) in
                            self.clearCart()
                        })
                        action1.setValue(AppColor.primary, forKey: "titleTextColor")
                        alert.addAction(action1)
                        let action2 = UIAlertAction.init(title: "NO".localized, style: .default, handler: nil)
                        action2.setValue(AppColor.primary, forKey: "titleTextColor")
                        alert.addAction(action2)
                        alert.setValue(NSAttributedString(string: vendorError, attributes: [NSAttributedString.Key.font : UIFont.init(name: AppFont.Oswald_Regular, size: 15) ?? UIFont.systemFont(ofSize: 15), NSAttributedString.Key.foregroundColor : AppColor.secondary]), forKey: "attributedMessage")
                        (UIApplication.shared.delegate)?.window??.rootViewController?.present(alert, animated: true, completion: nil)
                        return
                    }
                }
                self.showError(error: err)
                NotificationCenter.default.post(name: .cartItemAddedError, object: nil)
                return
            }
//            self.cartItems.append(id)
            self.getCartItems()
            NotificationCenter.default.post(name: .cartItemAdded, object: nil)
        }
    }
    
    func addToCart(id: String, quantity: Int,opr: String? = nil){
        var parameter : [String : Any] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
            "product_id": id,
            "quantity": quantity]
        
        if let opr = opr {
            parameter["opr"] = opr
        }
        
        NetworkRequest.shared.addCartItem(parameters: parameter) { (response, error) in
            if let err = error {
                print(err.message)
                self.showError(error: err)
                NotificationCenter.default.post(name: .cartItemAddedError, object: nil)
                return
            }
//            self.cartItems.append(id)
            NotificationCenter.default.post(name: .cartItemAdded, object: nil)
        }
    }
    
    func removeFromCart(id: String){
        let parameter : [String : Any] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
            "product_id": id]
        NetworkRequest.shared.removeCartItem(parameters: parameter) { (response, error) in
            if let err = error {
                print(err.message)
                self.showError(error: err)
                return
            }
//            self.cartItems.append(id)
            self.getCartItems()
            self.data.removeAll { (item) -> Bool in
                return item.id == id
            }
            NotificationCenter.default.post(name: .cartItemRemoved, object: nil)
            NotificationCenter.default.post(name: .cartLoaded, object: nil)
        }
    }
    
    func clearCart(){
        let parameter : [String : Any] = ["language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c"]
        NetworkRequest.shared.clearCart(parameters: parameter) { (response, error) in
            if let err = error {
                self.showError(error: err)
                return
            }
            self.data.removeAll()
//            self.cartItems.removeAll()
            self.getCartItems()
//            NotificationCenter.default.post(name: .cartItemRemoved, object: nil)
            NotificationCenter.default.post(name: .cartLoaded, object: nil)
        }
    }
    
    func placeOrder(){
        
        guard let user = UserManager.getUserObjectFromUserDefaults() else {
            Messages.showErrorMessage(message: "User not logged in".localized)
            return
        }
        guard let firstName = contactInfo["first_name"] ?? user.firstName, !firstName.isEmpty else {
            Messages.showErrorMessage(message: "First name is not available".localized)
            return
        }
        guard let lastName = contactInfo["last_name"] ?? user.lastName, !lastName.isEmpty else {
            Messages.showErrorMessage(message: "Last name is not available".localized)
            return
        }
        guard let email = contactInfo["email"] ?? user.email, !email.isEmpty else {
            Messages.showErrorMessage(message: "Email is not available".localized)
            return
        }
        guard let phone =  contactInfo["phone"] ?? user.phone, !phone.isEmpty else {
            Messages.showErrorMessage(message: "Phone is not available".localized)
            return
        }
        
        var parameter : [String : Any] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
            "first_name": firstName,
            "last_name": lastName,
            "email": email,
            "phone": phone,
            "current_location": "",
        ]
        
        parameter.merge(deliveryInfo) { (a, b) -> Any in
            return a
        }
        NetworkRequest.shared.placeOrder(parameters: parameter) { (response, error) in
            if let err = error {
                print(err.message)
                self.showError(error: err)
                return
            }
            
            Messages.showSuccessMessage(message: "Order successfully placed")
            self.data.removeAll()
//            self.cartItems.removeAll()
            NotificationCenter.default.post(name: .cartLoaded, object: nil)
            NotificationCenter.default.post(name: .orderPlaced, object: nil)
        }
    }
    
    func applyVoucher(title: String){
        let parameter : [String : Any] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
            "voucher_name": title]
        NetworkRequest.shared.applyVoucher(parameters: parameter) { (response, error) in
            if let err = error {
                print(err.message)
                self.showError(error: err)
                NotificationCenter.default.post(name: .voucherAppliedError, object: nil)
                return
            }
            self.getCartItems()
            NotificationCenter.default.post(name: .voucherApplied, object: nil)
        }
    }
    
    func myOrders(title: String){
        let parameter : [String : Any] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
            "voucher_name": title]
        NetworkRequest.shared.myOrders(parameters: parameter) { (response, error) in
            if let err = error {
                print(err.message)
                self.showError(error: err)
                return
            }
        }
    }
    
    func myOrdersDetails(title: String){
        let parameter : [String : Any] = [
            "language_id": "73089eac-068f-4d4f-ae59-8fd478fefc1c",
            "voucher_name": title]
        NetworkRequest.shared.myOrderDetails(parameters: parameter) { (response, error) in
            if let err = error {
                print(err.message)
                self.showError(error: err)
                return
            }
        }
    }
    
    func getItemCount(id: String) -> Int{
        let model = self.data.first { (item) -> Bool in
            return item.id == id
        }
        return model?.quantity ?? 0
    }
    
    func cartItemExists(id: String) -> Bool {
        if !self.data.isEmpty {
            if self.data.contains(where: { (cItem) -> Bool in
                return cItem.id == id
            }) {
                return true
            }
        }
        return false
        
//        guard !self.cartItems.isEmpty else { return false }
//        return self.cartItems.contains(id)
    }
    
    func isCartEmpty() -> Bool {
//        guard self.data.isEmpty else { return false }
//        return self.cartItems.isEmpty
        return self.data.isEmpty
    }
    
    func showError(error: MyError){
        let title = error.message
        for message in error.userInfo {
            if !message.value.isEmpty {
                Messages.showErrorMessage(title: title, message: message.value)
                return
            }
        }
        guard !error.userInfo.isEmpty else {
            Messages.showErrorMessage(message: title)
            return
        }
    }
    
}
