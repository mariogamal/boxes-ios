//
//  UserManager.swift
//  Boxes
//
//  Created by MacAir on 03/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation

class UserManager {
    
    var cardInfo : [String:String] = [:]
    
    static var shared = UserManager()
    
    var isSubscribed : Bool = false // user subscribed to a specific food deal
    
    class var token : String {
        set {
            UserDefaults.standard.set(newValue, forKey: "TOKEN")
        }
        get {
            return UserDefaults.standard.string(forKey: "TOKEN") ?? ""
        }
    }
    
    static var fcmToken = ""
    
    class func saveUserObjectToUserDefaults(userResult: UserModel) {
        let archiveData = NSKeyedArchiver.archivedData(withRootObject: userResult)
        UserDefaults.standard.set(archiveData, forKey: userDataUserDefault)
    }
    
    class func saveIsUserSubscribed(isSubscribed: Bool) {
        UserDefaults.standard.set(isSubscribed, forKey: userIsSubscribed)
    }
    
    class func getIsUserSunscribed() -> Bool{
        return UserDefaults.standard.bool(forKey: userIsSubscribed)
    }
    
    class func removeUserObjectFromUserDefaults() {
        UserDefaults.standard.set(nil, forKey: userDataUserDefault)
        UserDefaults.standard.removeObject(forKey: userDataUserDefault)
        UserDefaults.standard.synchronize()
    }
    
    class func getUserObjectFromUserDefaults() -> UserModel? {
        let userData: NSData? = UserDefaults.standard.value(forKey: userDataUserDefault) as? NSData
        if userData != nil {
            let userDataObj = NSKeyedUnarchiver.unarchiveObject(with: userData! as Data) as? UserModel
            return userDataObj
        } else {
            return nil
        }
    }
    
    class func isUserLogin() -> Bool {
        if self.getUserObjectFromUserDefaults() != nil {
            return true
        } else {
            return false
        }
    }
    
    class func saveUserSelectedLanguage(languageId: String) {
        UserDefaults.standard.set(languageId, forKey: userSelectedLanguage)
    }
    
    class func getUserSelectedLanguage() -> String {
        return UserDefaults.standard.string(forKey: userSelectedLanguage) ?? "73089eac-068f-4d4f-ae59-8fd478fefc1c"
    }
    
    class func saveInstructionShown(instrctionsShown: Bool) {
        UserDefaults.standard.set(instrctionsShown, forKey: instructionShown)
    }
    
    class func getInstructionShown() -> Bool {
        return UserDefaults.standard.bool(forKey: instructionShown)
    }
}
