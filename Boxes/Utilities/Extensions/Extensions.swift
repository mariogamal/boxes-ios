//
//  Extensions.swift
//  Boxes
//
//  Created by MacAir on 03/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import Kingfisher

extension UIView {
    func getScreenRatio() -> CGFloat? {
        if let frame = (UIApplication.shared.delegate as? AppDelegate)?.window?.frame{
            let height = frame.height
            let width = frame.width
            return width/height
        }
        return nil
    }
    
    func addConstraintsWithFormatString(formate: String, views: UIView...) {
        
        var viewsDictionary = [String: UIView]()
        
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: formate, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    func makebox(borderColor: UIColor? = nil,borderWidth: CGFloat? = nil){
        
        self.layer.borderWidth = borderWidth ?? 1
        self.layer.borderColor = borderColor?.cgColor ?? UIColor.gray.cgColor
        
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]? = nil, cornerRadius : CGFloat = 0, startPoint : CGPoint = CGPoint(x: 0.5, y: 0), endPoint : CGPoint = CGPoint(x: 0.5, y: 1)) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.cornerRadius = cornerRadius
        gradient.startPoint = startPoint
        gradient.endPoint = endPoint
        if let gLayer = self.layer.sublayers?.first as? CAGradientLayer {
            self.layer.replaceSublayer(gLayer, with: gradient)
        }else{
            self.layer.insertSublayer(gradient, at: 0)
        }
    }
    
    func removeGradientLayer(){
        if let gLayer = self.layer.sublayers?.first as? CAGradientLayer {
            gLayer.removeFromSuperlayer()
        }
    }
}

extension UIViewController {
    
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
    
    class var identifier : String {
        return "\(self)"
    }
    
}

enum AppStoryboard : String {
    
    case User, Main, LeftMenu, RightMenu
    
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T : UIViewController>(viewControllerClass : T.Type, function : String = #function, line : Int = #line, file : String = #file) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).identifier
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
            fatalError("ViewController with identifier \(storyboardID), not found in \(self.rawValue) Storyboard.\nFile : \(file) \nLine Number : \(line) \nFunction : \(function)")
        }
        return scene
    }
    
    func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
}

extension UICollectionViewCell {
    class var identifier : String {
        return "\(self)"
    }
}

extension UITableViewCell {
    class var identifier : String {
        return "\(self)"
    }
}

extension UITableViewHeaderFooterView {
    class var identifier : String {
        return "\(self)"
    }
}

extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}

extension UINavigationController {
    
    func resetNavigationBar(barTintColor: UIColor = UIColor.init(rgb: THEME_COLOR)) {
        self.navigationBar.isHidden = false
        self.navigationBar.isTranslucent = false
        self.navigationBar.barTintColor = barTintColor
    }
    
    func setNavigationBarColor(color: UIColor) {
        self.navigationBar.barTintColor = color
    }
    
    func setGradient(colors: [UIColor],
                     startPoint: UINavigationBarGradientView.Point = .topLeft,
                     endPoint: UINavigationBarGradientView.Point = .bottomLeft,
                     locations: [NSNumber] = [0, 1]) {
        self.navigationBar.setGradientBackground(colors: colors,
                                                 startPoint: startPoint,
                                                 endPoint: endPoint,
                                                 locations: locations)
    }
    
    func plainNavigationBar() {
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
    }
    
    func transparentNavigationBar() {
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
        self.view.backgroundColor = .clear
    }
    
    func setNavigationBarTitle(color: UIColor = UIColor(rgb: THEME_COLOR)) {
        self.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: color
        ]
        
        self.navigationBar.largeTitleTextAttributes = [
            NSAttributedString.Key.foregroundColor: color
        ]
    }
    
    func setNavigationBarFont(font: UIFont) {
        self.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.font: font
        ]
    }
    
    func setNavigationBarAttribute(font: UIFont, color: UIColor = .white) {
        self.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.foregroundColor: color
        ]
    }
    
    func hideNavigationBar(isHidden: Bool = true) {
        self.navigationBar.isHidden = isHidden
    }
}

extension UINavigationBar {
    func setGradientBackground(colors: [UIColor],
                               startPoint: UINavigationBarGradientView.Point = .topLeft,
                               endPoint: UINavigationBarGradientView.Point = .bottomLeft,
                               locations: [NSNumber] = [0, 1]) {
        guard let backgroundView = value(forKey: "backgroundView") as? UIView else { return }
        guard let gradientView = backgroundView.subviews.first(where: { $0 is UINavigationBarGradientView }) as? UINavigationBarGradientView else {
            let gradientView = UINavigationBarGradientView(colors: colors, startPoint: startPoint,
                                                           endPoint: endPoint, locations: locations)
            backgroundView.addSubview(gradientView)
            gradientView.setupConstraints()
            return
        }
        gradientView.set(colors: colors, startPoint: startPoint, endPoint: endPoint, locations: locations)
    }
}

extension UILabel {
    
    func addTrailing(with trailingText: String, moreText: String, moreTextFont: UIFont, moreTextColor: UIColor) {
        let readMoreText: String = trailingText + moreText
        
        let lengthForVisibleString: Int = self.vissibleTextLength
        let mutableString: String = self.text!
        let trimmedString: String? = (mutableString as NSString).replacingCharacters(in: NSRange(location: lengthForVisibleString, length: ((self.text?.count)! - lengthForVisibleString)), with: "")
        let readMoreLength: Int = (readMoreText.count)
        let trimmedForReadMore: String = (trimmedString! as NSString).replacingCharacters(in: NSRange(location: ((trimmedString?.count ?? 0) - readMoreLength), length: readMoreLength), with: "") + trailingText
        let answerAttributed = NSMutableAttributedString(string: trimmedForReadMore, attributes: [NSAttributedString.Key.font: self.font!])
        let readMoreAttributed = NSMutableAttributedString(string: moreText, attributes: [NSAttributedString.Key.font: moreTextFont, NSAttributedString.Key.foregroundColor: moreTextColor])
        answerAttributed.append(readMoreAttributed)
        self.attributedText = answerAttributed
    }
    
    var vissibleTextLength: Int {
        let font: UIFont = self.font
        let mode: NSLineBreakMode = self.lineBreakMode
        let labelWidth: CGFloat = self.frame.size.width
        let labelHeight: CGFloat = self.frame.size.height
        let sizeConstraint = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)
        
        let attributes: [AnyHashable: Any] = [NSAttributedString.Key.font: font]
        let attributedText = NSAttributedString(string: self.text!, attributes: attributes as? [NSAttributedString.Key : Any])
        let boundingRect: CGRect = attributedText.boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, context: nil)
        
        if boundingRect.size.height > labelHeight {
            var index: Int = 0
            var prev: Int = 0
            let characterSet = CharacterSet.whitespacesAndNewlines
            repeat {
                prev = index
                if mode == NSLineBreakMode.byCharWrapping {
                    index += 1
                } else {
                    index = (self.text! as NSString).rangeOfCharacter(from: characterSet, options: [], range: NSRange(location: index + 1, length: self.text!.count - index - 1)).location
                }
            } while index != NSNotFound && index < self.text!.count && (self.text! as NSString).substring(to: index).boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, attributes: attributes as? [NSAttributedString.Key : Any], context: nil).size.height <= labelHeight
            return prev
        }
        return self.text!.count
    }
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font!], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}

extension UIColor {
    
    convenience init(rgb: UInt) {
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func rgbColor(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        
        return UIColor.init(red: red/255, green: green/255, blue: blue/255, alpha: 1.0)
    }
    
    static func colorFromHex(_ hex: String) -> UIColor {
        
        var hexString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if hexString.hasPrefix("#") {
            
            hexString.remove(at: hexString.startIndex)
        }
        
        if hexString.count != 6 {
            
            return UIColor.magenta
        }
        
        var rgb: UInt32 = 0
        Scanner.init(string: hexString).scanHexInt32(&rgb)
        
        return UIColor.init(red: CGFloat((rgb & 0xFF0000) >> 16)/255,
                            green: CGFloat((rgb & 0x00FF00) >> 8)/255,
                            blue: CGFloat(rgb & 0x0000FF)/255,
                            alpha: 1.0)
    }
}

extension CGSize {
    static func aspectFit(aspectRatio : CGSize, boundingSize: CGSize) -> CGSize {
        var boundingSize = boundingSize
        let mW = boundingSize.width / aspectRatio.width;
        let mH = boundingSize.height / aspectRatio.height;
        
        if( mH < mW ) {
            boundingSize.width = boundingSize.height / aspectRatio.height * aspectRatio.width;
        }
        else if( mW < mH ) {
            boundingSize.height = boundingSize.width / aspectRatio.width * aspectRatio.height;
        }
        
        return boundingSize;
    }
    
    static func aspectFill(aspectRatio :CGSize, minimumSize: CGSize) -> CGSize {
        var minimumSize = minimumSize
        let mW = minimumSize.width / aspectRatio.width;
        let mH = minimumSize.height / aspectRatio.height;
        
        if( mH > mW ) {
            minimumSize.width = minimumSize.height / aspectRatio.height * aspectRatio.width;
        }
        else if( mW > mH ) {
            minimumSize.height = minimumSize.width / aspectRatio.width * aspectRatio.height;
        }
        
        return minimumSize;
    }
}

extension Date {
    func format(format: String = "E, d MMM yyyy HH:mm:ss") -> Date {
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = format
        formatter.timeZone = TimeZone(identifier: "UTC")
        //        formatter.timeZone = TimeZone(identifier: "GMT")
        
        let myString = formatter.string(from: self) // string purpose I add here
        // convert your string to date
        return formatter.date(from: myString)!
    }
    
    func toString(format: String = "E, d MMM yyyy HH:mm:ss") -> String {
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: self) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = format
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        return myStringafd
    }
    
    func configureDateFormatter() -> String {
        let formatter = DateFormatter()
        switch true {
        case Calendar.current.isDateInToday(self) || Calendar.current.isDateInYesterday(self):
            formatter.doesRelativeDateFormatting = true
            formatter.dateStyle = .short
            formatter.timeStyle = .short
        case Calendar.current.isDate(self, equalTo: Date(), toGranularity: .weekOfYear):
            formatter.dateFormat = "EEEE"
        case Calendar.current.isDate(self, equalTo: Date(), toGranularity: .year):
            formatter.dateFormat = "E, d MMM, h:mm a"
        default:
            formatter.dateFormat = "MMM d, yyyy, h:mm a"
        }
        return formatter.string(from: self)
    }
    
    func addDay() -> Date {
        return Calendar.current.date(byAdding: DateComponents(day: 1), to: self)!
    }
    
    func addDay(days: Int) -> Date {
        return Calendar.current.date(byAdding: DateComponents(day: days), to: self)!
    }
    
    func previousMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: -1), to: self.startOfMonth())!
    }
    
    func nextMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1), to: self.startOfMonth())!
    }
    
    func previousYear() -> Date {
        return Calendar.current.date(byAdding: .year, value: -1, to: self.startOfMonth())!
    }
    
    func nextYear() -> Date {
        return Calendar.current.date(byAdding: .year, value: 1, to: self.startOfMonth())!
    }
    
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
}


extension String {
    
    func toDate(format: String = "yyyy'-'MM'-'dd'T'HH':'mm':'ssZZZ") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)
        return date ?? Date()
    }
}

extension GMSMapView {
    
    static var polyline: GMSPolyline!
    
    class func addMarker(coordinates: CLLocationCoordinate2D, with pin: UIImage) -> GMSMarker {
        
        let location = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude)
        let marker = GMSMarker()
        marker.position = location
        marker.icon = pin
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker.appearAnimation = .pop
        
        return marker
    }
    
    func addMarker(coordinates: CLLocationCoordinate2D, isSelected: Bool = false) -> GMSMarker {
        
        let location = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude)
        let marker = GMSMarker()
        marker.position = location
        marker.appearAnimation = .pop
        marker.map = self
        
        if isSelected {
            self.selectedMarker = marker
        }
        return marker
    }
    
    func addMarker(coordinates: CLLocationCoordinate2D, with pin: UIImage, isSelected: Bool = false) -> GMSMarker {
        
        let location = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude)
        let marker = GMSMarker()
        marker.position = location
        marker.icon = pin
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker.appearAnimation = .pop
        marker.map = self
        
        if isSelected {
            self.selectedMarker = marker
        }
        return marker
    }
    
    func addPulseMarker(coordinates: CLLocationCoordinate2D, with pin: UIImage) -> GMSMarker {
        
        let marker = GMSMarker()
        
        //custom marker image
        let pulseRingImg = UIImageView(frame: CGRect(x: -30, y: -30, width: 78, height: 78))
        pulseRingImg.image = pin
        pulseRingImg.isUserInteractionEnabled = false
        CATransaction.begin()
        CATransaction.setAnimationDuration(3.5)
        
        //transform scale animation
        var theAnimation: CABasicAnimation?
        theAnimation = CABasicAnimation(keyPath: "transform.scale.xy")
        theAnimation?.repeatCount = Float.infinity
        theAnimation?.autoreverses = false
        theAnimation?.fromValue = Float(0.0)
        theAnimation?.toValue = Float(2.0)
        theAnimation?.isRemovedOnCompletion = false
        
        pulseRingImg.layer.add(theAnimation!, forKey: "pulse")
        pulseRingImg.isUserInteractionEnabled = false
        CATransaction.setCompletionBlock({() -> Void in
            
            //alpha Animation for the image
            let animation = CAKeyframeAnimation(keyPath: "opacity")
            animation.duration = 3.5
            animation.repeatCount = Float.infinity
            animation.values = [Float(2.0), Float(0.0)]
            marker.iconView?.layer.add(animation, forKey: "opacity")
        })
        
        CATransaction.commit()
        marker.iconView = pulseRingImg
        marker.icon = pin
        marker.position = coordinates
        marker.layer.addSublayer(pulseRingImg.layer)
        marker.map = self
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        return marker
    }
    
    func updateMarker(marker: GMSMarker, coordinates: CLLocationCoordinate2D, degrees: CLLocationDegrees, duration: Double = 1.0, moveToLocation: Bool = false) {
        // Keep Rotation Short
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.5)
        marker.rotation = degrees
        CATransaction.commit()
        
        // Movement
        CATransaction.begin()
        CATransaction.setAnimationDuration(duration)
        marker.position = coordinates
        CATransaction.commit()
        
        if moveToLocation {
            CATransaction.begin()
            // Center Map View
            let camera = GMSCameraUpdate.setTarget(coordinates)
            self.animate(with: camera)
            
            CATransaction.commit()
        }
    }
    
    func updateMarker(marker: GMSMarker, image: UIImage, moveToLocation: Bool = false) {
        marker.icon = image
        
        if moveToLocation {
            CATransaction.begin()
            // Center Map View
            let camera = GMSCameraUpdate.setTarget(marker.position)
            self.animate(with: camera)
            
            CATransaction.commit()
        }
        
    }
    
    func animateToCoordinates(coordinates: CLLocationCoordinate2D) {
        let camera = GMSCameraUpdate.setTarget(coordinates)
        self.animate(with: camera)
    }
    
    func zoom(coordinates: CLLocationCoordinate2D, zoomLevel: Float) {
        let camera = GMSCameraPosition.camera(withTarget: coordinates, zoom: zoomLevel)
        self.animate(to: camera)
    }
    
    func getRoute() -> GMSPolyline? {
        return GMSMapView.polyline
    }
    
    func removeRoute() {
        if GMSMapView.polyline != nil {
            GMSMapView.polyline.map = nil
            GMSMapView.polyline = nil
        }
    }
    
    // Pass your source and destination coordinates in this method.
    func drawRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D) {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=false&mode=driving&key=AIzaSyADBVSBegINIgjltqURETGY63h7HRUe48A")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                do {
                    if let json: [String: Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {
                        
                        let routes = json["routes"] as? [Any]
                        if (routes?.count)! > 0 {
                            let firstIndex = routes?[0] as? [String: Any]
                            let overview_polyline = (firstIndex?["overview_polyline"] as? [String: Any])
                            let polyString = overview_polyline!["points"] as? String
                            self.showPath(polyStr: polyString!)
                        }
                    }
                    
                } catch {
                    print("error in JSONSerialization")
                }
            }
        })
        task.resume()
    }
    
    fileprivate func showPath(polyStr: String) {
        DispatchQueue.main.async {
            let path = GMSPath(fromEncodedPath: polyStr)
            if GMSMapView.polyline != nil {
                if GMSMapView.polyline.map == nil {
                    GMSMapView.polyline.map = self // Your map view
                }
                GMSMapView.polyline.path = path
            } else {
                GMSMapView.polyline = GMSPolyline(path: path)
                GMSMapView.polyline.strokeWidth = 3.0
//                GMSMapView.polyline.strokeColor = UIColor(rgb: 0xE43943)
                GMSMapView.polyline.map = self // Your map view
            }
        }
    }
}

extension Notification.Name {
    static let didReceiveRemainingTime = Notification.Name("didReceiveRemainingTime")
    static let didReceiveElapsedTime = Notification.Name("didReceiveElapsedTime")
    static let didStop = Notification.Name("didStop")
}


extension UICollectionView {
    
    func scrollToNearestVisibleCollectionViewCell() -> Int {
        self.decelerationRate = UIScrollView.DecelerationRate.fast
        let visibleCenterPositionOfScrollView = Float(self.contentOffset.x + (self.bounds.size.width / 2))
        var closestCellIndex = -1
        var closestDistance: Float = .greatestFiniteMagnitude
        for i in 0..<self.visibleCells.count {
            let cell = self.visibleCells[i]
            let cellWidth = cell.bounds.size.width
            let cellCenter = Float(cell.frame.origin.x + cellWidth / 2)
            
            // Now calculate closest cell
            let distance: Float = fabsf(visibleCenterPositionOfScrollView - cellCenter)
            if distance < closestDistance {
                closestDistance = distance
                closestCellIndex = self.indexPath(for: cell)!.row
            }
        }
        if closestCellIndex != -1 {
            self.scrollToItem(at: IndexPath(row: closestCellIndex, section: 0), at: .centeredHorizontally, animated: true)
        }
        
        return closestCellIndex
    }
}

extension UIButton {
    
    func shake() {
        self.transform = CGAffineTransform(translationX: 20, y: 0)
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    func bounce(){
        UIView.animate(withDuration: 0.6, animations: {
            self.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        },completion: { _ in
            UIView.animate(withDuration: 0.6) {
                self.transform = CGAffineTransform.identity
            }
        })
    }
    
}


extension UIImage {
    
    func getRoundedImage(view: UIView,radius:CGFloat,borderWidth:CGFloat) -> UIImage?{
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, 0)
        let path = UIBezierPath(roundedRect: view.bounds.insetBy(dx: borderWidth / 2, dy: borderWidth / 2), cornerRadius: radius)
        let context = UIGraphicsGetCurrentContext()
        context!.saveGState()
        path.addClip()
        self.draw(in: view.bounds)
        UIColor.gray.setStroke()
        path.lineWidth = borderWidth
        path.stroke()
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return roundedImage
    }
    
}


extension Dictionary where Key == String, Value == Any {
    func addLanguage() -> Dictionary<Key, Value> {
        var updatable = self
        updatable.updateValue(UserManager.getUserSelectedLanguage(), forKey: "language_id")
        return updatable
    }
}
