//
//  TableViewExtension.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/25/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func addPlaceholderImage(){
        if (self.viewWithTag(11) as? UIImageView) != nil {
            return
        }
        let image = UIImageView()
        image.tag = 11
        image.image = UIImage(named: "flower_screen_vector")
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        self.addSubview(image)
//        image.centerYAnchor.constraint(equalToSystemSpacingBelow: self.centerYAnchor, multiplier: 0.6).isActive = true
        image.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        image.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.6, constant: 0).isActive = true
        image.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        image.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
    }
    
    func removePlaceholderImage(){
        if let image = self.viewWithTag(11) as? UIImageView {
            image.removeFromSuperview()
        }
    }
}
