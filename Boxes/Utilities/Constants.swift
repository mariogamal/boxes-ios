//
//  Constants.swift
//  Boxes
//
//  Created by MacAir on 03/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import UIKit

let BASEURL = "http://boxes-app.com/api/"
let BASEIMAGEURL = "http://boxes-app.com/public/uploads/"

let userDataUserDefault         = "userDataUserDefault"
let userIsSubscribed            = "userIsSubscribed"
let userSelectedLanguage        = "userSelectedLanguage"
let instructionShown            = "instructionShown"

let WINDOW_FRAME                = UIScreen.main.bounds
let SCREEN_SIZE                 = UIScreen.main.bounds.size
let WINDOW_WIDTH                = UIScreen.main.bounds.size.width
let WINDOW_HIEGHT               = UIScreen.main.bounds.size.height

let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
let UIWINDOW                    = UIApplication.shared.delegate!.window!

let USER_DEFAULTS               = UserDefaults.standard

let THEME_COLOR: UInt           = 0x5DC04B
let THEME_COLOR_BLACK: UInt     = 0x000000
let THEME_COLOR_RED: UInt       = 0xC21538
let THEME_COLOR_GREEN: UInt     = 0x35C8A6

let THEME_COLOR_DARK_GREEN: UIColor = UIColor(named: "Theme Green") ?? .green
let THEME_COLOR_BLACK_HEX: UIColor = UIColor(hexString: "403F45")

let THEME_COLOR_GRAY_FONT: UIColor = UIColor(hexString: "727272")
let THEME_COLOR_GRAY_BORDER: UIColor = UIColor(hexString: "727272")


let LOGO_LIGHT                  = UIImage(named: "logo")


struct AppColor {
    
    static var primary = UIColor(named: "Theme Green") ?? UIColor.init(hexString: "#5FC049")
    static var secondary = UIColor(named: "Theme Dark Gray") ?? UIColor.init(hexString: "#403E45")
    
    static var deliver = UIColor(named: "Theme Deliver") ?? UIColor.init(hexString: "#426FDB")
    static var pause = UIColor(named: "Theme Pause") ?? UIColor.init(hexString: "#F00A12")
    
}


var Screen: UIScreen {
    return UIScreen.main
}

struct UserDefaultsKey {
    static let deviceId = "deviceId"
    static let IsFromSetting = "isFromSetting"
}

struct SPRING_STUFFS {
    
    struct ANIMATIONS {
        static let SHAKE =  "shake"
        static let POP =  "pop"
        static let MORPH =  "morph"
        static let SQUEEZE =  "squeeze"
        static let WOBBLE =  "wobble"
        static let SWING =  "swing"
        static let FLIPX =  "flipX"
        static let FLIPY =  "flipY"
        static let FALL =  "fall"
        static let SQUEEZE_LEFT =  "squeezeLeft"
        static let SQUEEZE_RIGHT =  "squeezeRight"
        static let SQUEEZE_DOWN =  "squeezeDown"
        static let SQUEEZE_UP =  "squeezeUp"
        static let SLIDE_LEFT =  "slideLeft"
        static let SLIDE_RIGHT =  "slideRight"
        static let SLIDE_DOWN =  "slideDown"
        static let SLIDE_UP =  "slideUp"
        static let FADE_IN =  "fadeIn"
        static let FADE_OUT =  "fadeOut"
        static let FADE_IN_LEFT =  "fadeInLeft"
        static let FADE_IN_RIGHT =  "fadeInRight"
        static let FADE_IN_DOWN =  "fadeInDown"
        static let FADE_IN_UP =  "fadeInUp"
        static let ZOOM_IN =  "zoomIn"
        static let ZOOM_OUT =  "zoomOut"
        static let FLASH =  "flash"
    }
    
    struct CURVE {
        static let SPRING = "spring"
        static let LINEAR = "linear"
        static let EASE_IN = "easeIn"
        static let EASE_OUT = "easeOut"
        static let EASE_IN_OUT = "easeInOut"
    }
    
    struct PROPERTIES {
        static let FORCE =  "force"
        static let DURATION =  "duration"
        static let DELAY =  "delay"
        static let DAMPING =  "damping"
        static let VELOCITY =  "velocity"
        static let REPEAT_COUNT =  "repeatCount"
        static let SCALE =  "scale"
        static let X =  "x"
        static let Y =  "y"
        static let ROTATE =  "rotate"
    }
}
