//
//  IBInspectables.swift
//  Med24
//
//  Created by MacAir on 12/11/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UITextView {
    
    @IBInspectable var TopTextSpaceUITextView: CGFloat {
        get {
            return self.textContainerInset.top
        }
        set {
            
            self.textContainerInset = UIEdgeInsets(top: newValue, left: self.textContainerInset.left, bottom: self.textContainerInset.bottom, right: self.textContainerInset.right)
        }
    }
    
    @IBInspectable var LeftTextSpaceUITextView: CGFloat {
        get {
            return self.textContainerInset.left
        }
        set {
            
            self.textContainerInset = UIEdgeInsets(top: self.textContainerInset.top, left: newValue, bottom: self.textContainerInset.bottom, right: self.textContainerInset.right)
        }
    }
    
    @IBInspectable var BottomTextSpaceUITextView: CGFloat {
        get {
            return self.textContainerInset.bottom
        }
        set {
            
            self.textContainerInset = UIEdgeInsets(top: self.textContainerInset.top, left: self.textContainerInset.left, bottom: newValue, right: self.textContainerInset.right)
        }
    }
    
    @IBInspectable var RightTextSpaceUITextView: CGFloat {
        get {
            return self.textContainerInset.right
        }
        set {
            
            self.textContainerInset = UIEdgeInsets(top: self.textContainerInset.top, left: self.textContainerInset.left, bottom: self.textContainerInset.bottom, right: newValue)
        }
    }
}

extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    @IBInspectable
    var scaleEnabled: Bool {
        get {
            return false
        }
        set {
            for constraint in self.constraints {
                print("before")
                print(constraint.constant, "")
                constraint.constant *= UIScreen.main.bounds.height/812
                print("after")
                print(constraint.constant, "")
            }
        }
    }
}


extension UIImageView {
    
    @IBInspectable
    var imageUrl: String {
        get {
            return ""
        }
        set {
            let url = URL(string: newValue)
            self.kf.setImage(with: url, placeholder: UIImage())
        }
    }
    
    @IBInspectable
    var imageColor: UIColor? {
        get {
            if let color = self.tintColor {
                return UIColor(cgColor: color.cgColor)
            }
            return nil
        }
        set {
            if let color = newValue {
                let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
                self.image = templateImage
                self.tintColor = color
            }
        }
    }
    
}
