//
//  NetworkLayer.swift
//  CBD Shops
//
//  Created by MacAir on 01/07/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import Alamofire

class MyError {
    var message : String
    var statusCode : Int
    var userInfo : [String : String]
    
//    init(message : String, statusCode : Int) {
//        self.message = message
//        self.statusCode = statusCode
//    }
    init(message : String, statusCode : Int, userInfo : [String : [String]] = [:]) {
        self.message = message
        self.statusCode = statusCode
        self.userInfo = [:]
        userInfo.forEach { (key, value) in
            self.userInfo[key] = value.first ?? ""
        }
    }
}

typealias DefaultAPIClosure = (AnyObject, MyError?) -> Void
typealias DefaultAPIFailureClosure = (Error) -> Void
typealias DefaultAPISuccessClosure = (AnyObject) -> Void
typealias DefaultBoolResultAPISuccesClosure = (Bool) -> Void

class NetworkLayer {
    
    static let shared = NetworkLayer()
    private init(){}
    
    //        func URLforRoute(route: String,params:[String: String]) -> URL? {
    //
    //            if let components: NSURLComponents  = NSURLComponents(string: (self.BaseURL+route)){
    //                var queryItems = [NSURLQueryItem]()
    //                for(key,value) in params{
    //                    queryItems.append(NSURLQueryItem(name:key,value: value))
    //                }
    //                components.queryItems = queryItems as [URLQueryItem]?
    //
    //                return components.url as NSURL?
    //            }
    //            return nil;
    //        }
    
    var BaseURL = BASEURL
    
    func getAuthorizationHeader() -> Dictionary<String,String>{
//        if UserManager.isUserLogin() {
        print(UserManager.token)
        return ["Content-Type": "application/json; charset=utf-8", "Authorization": UserManager.token ]
//        }
//        return [:]
    }
    
    func postRequestWith(route: Route,parameters: Parameters, result:@escaping DefaultAPIClosure){
        print("route \(route.rawValue) and paramters \(parameters)")
        
        guard self.checkInternet() else{
            result([:] as AnyObject, self.internetError())
            return
        }
        LoadingOverlay.shared.showOverlay()
        Alamofire.request(self.getURL(route), method: .post, parameters: parameters.addLanguage(), encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
            response in
            LoadingOverlay.shared.hideOverlayView()
            self.responseResult(response, result: { (object, error) in
                result(object, error)
            })
        }
        
        //        let data = parameters.queryString.data(using:String.Encoding.ascii, allowLossyConversion: false)
    }
    
    func getRequestWith(route: Route, parameters: Parameters, result:@escaping DefaultAPIClosure, showLoading: Bool = true){
        
        print("route \(route.rawValue) and paramters \(parameters)")

        guard self.checkInternet() else{
            result([:] as AnyObject, self.internetError())
            return
        }
        if showLoading {
            LoadingOverlay.shared.showOverlay()
        }
        let url = self.URLforRoute(route: route, params: parameters.addLanguage())

        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
            response in
            if showLoading {
                LoadingOverlay.shared.hideOverlayView()
            }
            self.responseResult(response, result: { (object, error) in
                result(object, error)
            })
        }
        
        //        let data = parameters.queryString.data(using:String.Encoding.ascii, allowLossyConversion: false)
    }
    
    func deleteRequestWith(route: Route, parameters: Parameters, result:@escaping DefaultAPIClosure){
        
        print("route \(route.rawValue) and paramters \(parameters)")

        guard checkInternet() else{
            result([:] as AnyObject, internetError())
            return
        }
        
        Alamofire.request(self.getURL(route), method: .delete, parameters: parameters.addLanguage(), encoding: URLEncoding.default, headers: getAuthorizationHeader()).responseJSON{
            response in
            
            self.responseResult(response, result: { (object, error) in
                result(object, error)
            })
            
        }
    }
    
    func postRequestWithMultipart(route: Route, parameters: Parameters, result:@escaping DefaultAPIClosure){
        
        print("route \(route.rawValue) and paramters \(parameters)")

        guard self.checkInternet() else{
            result([:] as AnyObject, self.internetError())
            return
        }
        
        let URLSTR = try! URLRequest(url: self.getURLString(route), method: HTTPMethod.post, headers: self.getAuthorizationHeader())
        
        Alamofire.upload(multipartFormData: {multipart in
            
            
            for (key , value) in parameters.addLanguage() {
                if let data:Data = value as? Data {
                    multipart.append(data, withName: key, fileName: "\(key).\(data.mimeTypeFileExtension)", mimeType: data.mimeType)
                } else {
                    multipart.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                }
            }
            
            
        }, with: URLSTR, encodingCompletion: {res in
//            LoadingOverlay.shared.hideOverlayView()
            switch res {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    self.responseResult(response, result: { (object, error) in
                        result(object, error)
                    })
                }
            case .failure(let error):
                let myError = MyError(message: error.localizedDescription, statusCode: 0)
                result([:] as AnyObject, myError)
            }
        })
    }
    
    func responseResult(_ response:DataResponse<Any>, result: @escaping DefaultAPIClosure) {
        print(response)
        switch response.result{
        case .success:
            if let dictData = response.result.value as? [String : Any] {
                if let resultData = dictData["status"] as? Int, resultData == 1 {
                    result(dictData["data"] as AnyObject, nil)
                }else if let resultData = dictData["status"] as? String, resultData == "1" {
                    result(dictData["data"] as AnyObject, nil)
                }else {
                    //Failure
                    let errorMessage: String = (dictData["message"] as? String) ?? "Unknown error";
                    let parameter = (dictData["parameters"] as? [String : [String]]) ?? [:]
                    let myError = MyError(message: errorMessage, statusCode: (dictData["APIstatus"] as? Int) ?? 0, userInfo: parameter )
                    result([:] as AnyObject, myError)
                }
            } else {
                //Failure
                let errorMessage: String = "Response not in dictionary";
                let myError = MyError(message: errorMessage, statusCode: 0)
                result([:] as AnyObject, myError)
            }
            
        case .failure(let error):
            let myError = MyError(message: error.localizedDescription, statusCode: response.response?.statusCode ?? 0)
            result([:] as AnyObject, myError)
        }
    }
    
    func URLforRoute(route: Route, params: Parameters ) -> URL {
        var components = URLComponents(string: self.getURL(route).absoluteString)
        var queryItems = [URLQueryItem]()
        for(key,value) in params{
            queryItems.append(URLQueryItem(name:key,value: "\(value)"))
        }
        components?.queryItems = queryItems
        return components?.url ?? self.getURL(route)
    }
    
    func checkInternet() -> Bool{
        return NetworkReachabilityManager()?.isReachable ?? false
    }
    
    func internetError() -> MyError?{
        let myError = MyError(message: "Internet Not Responding", statusCode: 503)
        return myError
    }
    
    func getURL(_ route : Route) -> URL {
        let url = URL(string: "\(self.BaseURL)\(route.rawValue)")!
        return url
    }
    
    func getURLString(_ route : Route) -> String {
        return "\(self.BaseURL)\(route.rawValue)"
    }
    
}


public extension Data {
    var mimeType:String {
        get {
            var c = [UInt32](repeating: 0, count: 1)
            (self as NSData).getBytes(&c, length: 1)
            switch (c[0]) {
            case 0xFF:
                return "image/jpg";
            case 0x89:
                return "image/png";
            case 0x47:
                return "image/gif";
            case 0x49, 0x4D:
                return "image/tiff";
            case 0x25:
                return "application/pdf";
            case 0xD0:
                return "application/vnd";
            case 0x46:
                return "text/plain";
            default:
                print("mimeType for \(c[0]) in available");
                return "image/jpg";
            }
        }
    }
    
    var mimeTypeFileExtension:String {
        get {
            var c = [UInt32](repeating: 0, count: 1)
            (self as NSData).getBytes(&c, length: 1)
            switch (c[0]) {
            case 0xFF:
                return "jpg";
            case 0x89:
                return "png";
            case 0x47:
                return "gif";
            case 0x49, 0x4D:
                return "tiff";
            case 0x25:
                return "pdf";
            case 0xD0:
                return "vnd";
            case 0x46:
                return "txt";
            default:
                print("mimeType for \(c[0]) in available");
                return "jpg";
            }
        }
    }
    
}
