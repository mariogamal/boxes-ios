//
//  Network.swift
//  CBD Shops
//
//  Created by MacAir on 27/06/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum Route : String {
    
    //listing API's
    case languages                                      = "v1/languages"
    case types                                          = "v1/types"
    case search                                         = "v1/search"
    case listings                                       = "v1/listings"
    case listing_categories_with_products               = "v1/listing-categories-with-products"
    case packages                                       = "v1/packages"
    case menu                                           = "v1/auth/menu"
    case showMenu                                       = "v1/menu"
    
    //Auth
    case login                                          = "v2/login"
    case verifyOpt                                      = "v1/verify-otp"
    case storeSurvey                                    = "v1/auth/store-survey"
    case survey_details                                 = "v1/auth/survey-details" //get
    case logout                                         = "v1/auth/logout"
    case changeNumber                                   = "v1/auth/change-number"
    case user                                           = "v1/auth/user" //get
    case storeProfile                                   = "v1/auth/store-profile"
    case storeCardInfo                                  = "v1/auth/store-card-info"
    
    //Subscription API's
    case userSubscription                               = "v1/auth/user-subscription"
    case myPackages                                     = "v1/auth/my-packages"
    case myCurrentPackage                               = "v1/auth/my-current-package"
    
    //Meals API's
    case storeUserMeal                                  = "v1/auth/store-user-meal"
    case updateUserMeal                                 = "v1/auth/update-user-meal"
    case deleteUserMeal                                 = "v1/auth/delete-meal"
    case pauseMeal                                      = "v1/auth/pause-meal"
    case calendarDetail                                 = "v1/auth/calendar-detail"
    case mealsHistory                                   = "v1/auth/meals-history"
    case deliveryDatesBySubscription                    = "v1/auth/delivery-dates-by-subscription"
    case deliveryDateDetails                            = "v1/auth/delivery-date-details"
    
    //Cart API's
    case new_add_cart_item                              = "v1/auth/cart/add-new-cart-item"
    case add_cart_item                                  = "v1/auth/cart/add-cart-item"
    case cart_remove_cart_item                          = "v1/auth/cart/remove-cart-item"
    case myCart                                         = "v1/auth/cart/my-cart"
    case clearCart                                      = "v1/auth/cart/clear-cart"
    case placeOrder                                     = "v1/auth/cart/place-order"
    case applyVoucher                                   = "v1/auth/cart/apply-voucher" //not autorized
    case myOrders                                       = "v1/auth/cart/my-orders"
    case myOrderDetails                                 = "v1/auth/cart/my-order-details"
    
    //Miscellaneous API's
    case allergiesOrDiet                                 = "v1/allergies-or-diet"
    case submitQuery                                     = "v1/contact/submit-query"
    case page                                            = "v1/page"
    case rateMeal                                        = "v1/auth/rate-meal"
    case rateDetails                                     = "v1/auth/rate-details"
    case notifications                                   = "v1/auth/notifications"
    
    //Ingredients API
    case ingredients                                     = "v1/ingredients"
    
    //Address
    case cities                                          = "v1/cities"
    case areas                                           = "v1/areas"
    case storeAddress                                    = "v1/auth/store-address"
    case addressDetails                                  = "v1/auth/address-details"

}

class NetworkRequest {
    
    static var shared = NetworkRequest()
    
    private init(){
        
    }
    
    //get
    
    func ingredients(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .ingredients, parameters: parameters, result: result)
    }

    func languages(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .languages, parameters: parameters, result: result)
    }
    
    func types(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .types, parameters: parameters, result: result, showLoading: false)
    }
    
    func search(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .search, parameters: parameters, result: result)
    }
    
    func listings(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .listings, parameters: parameters, result: result)
    }
    
    func listing_categories_with_products(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .listing_categories_with_products, parameters: parameters, result: result)
    }
    
    func packages(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .packages, parameters: parameters, result: result)
    }
    
    func menu(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .menu, parameters: parameters, result: result)
    }
    
    func showMenu(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .showMenu, parameters: parameters, result: result)
    }
    
    func survey_details(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .survey_details, parameters: parameters, result: result)
    }
    
    func user(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .user, parameters: parameters, result: result)
    }
    
    func myPackages(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .myPackages, parameters: parameters, result: result)
    }
    
    func myCurrentPackage(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .myCurrentPackage, parameters: parameters, result: result)
    }
    
    func calendarDetail(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .calendarDetail, parameters: parameters, result: result)
    }
    
    func mealsHistory(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .mealsHistory, parameters: parameters, result: result)
    }
    
    func deliveryDatesBySubscription(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .deliveryDatesBySubscription, parameters: parameters, result: result)
    }
    
    func deliveryDateDetails(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .deliveryDateDetails, parameters: parameters, result: result)
    }
    
    func myCart(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .myCart, parameters: parameters, result: result, showLoading: false)
    }
    
    func applyVoucher(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .applyVoucher, parameters: parameters, result: result)
    }
    
    func myOrders(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .myOrders, parameters: parameters, result: result)
    }
    
    func myOrderDetails(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .myOrderDetails, parameters: parameters, result: result)
    }
    
    func allergiesOrDiet(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .allergiesOrDiet, parameters: parameters, result: result)
    }
    
    func page(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .page, parameters: parameters, result: result)
    }
    
    func notifications(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .notifications, parameters: parameters, result: result)
    }
    
    func cities(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .cities, parameters: parameters, result: result)
    }
    
    func areas(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .areas, parameters: parameters, result: result)
    }
    
    func addressDetails(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .addressDetails, parameters: parameters, result: result)
    }
    
    func rateDetails(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.getRequestWith(route: .rateDetails, parameters: parameters, result: result)
    }
    
    //post
    func login(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .login, parameters: parameters, result: result)
    }
    
    func verifyOpt(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .verifyOpt, parameters: parameters, result: result)
    }
    
    func storeSurvey(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .storeSurvey, parameters: parameters, result: result)
    }
    
    func logout(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .logout, parameters: parameters, result: result)
    }
    
    func changeNumber(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .changeNumber, parameters: parameters, result: result)
    }
    
    func storeProfile(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWithMultipart(route: .storeProfile, parameters: parameters, result: result)
    }
    
    func storeCardInfo(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .storeCardInfo, parameters: parameters, result: result)
    }
    
    func userSubscription(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .userSubscription, parameters: parameters, result: result)
    }
    
    func storeUserMeal(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .storeUserMeal, parameters: parameters, result: result)
    }
    
    func updateUserMeal(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .updateUserMeal, parameters: parameters, result: result)
    }
    
    func deleteUserMeal(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .deleteUserMeal, parameters: parameters, result: result)
    }
    
    func pauseMeal(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .pauseMeal, parameters: parameters, result: result)
    }
    
    func addCartItem(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .add_cart_item, parameters: parameters, result: result)
    }
    
    func newAddCartItem(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .new_add_cart_item, parameters: parameters, result: result)
    }
    
    func removeCartItem(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .cart_remove_cart_item, parameters: parameters, result: result)
    }
    
    func clearCart(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .clearCart, parameters: parameters, result: result)
    }
    
    func placeOrder(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .placeOrder, parameters: parameters, result: result)
    }
    
    func submitQuery(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .submitQuery, parameters: parameters, result: result)
    }
    
    func rateMeal(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .rateMeal, parameters: parameters, result: result)
    }
    
    func storeAddress(parameters: Parameters, result:@escaping DefaultAPIClosure){
        NetworkLayer.shared.postRequestWith(route: .storeAddress, parameters: parameters, result: result)
    }
}


class PostData: NSObject {
    
    var value: Data!
    var type: DOCUMENTTYPE!
    
    init(value: Data, type: DOCUMENTTYPE) {
        super.init()
        
        self.value = value
        self.type = type
    }
}

enum DOCUMENTTYPE: String {
    case IMAGE
    case VIDEO
    case PDF
    case TEXT
}
