//
//  AppDelegate.swift
//  Boxes
//
//  Created by MacAir on 03/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GooglePlaces
import GooglePlacePicker
import GoogleMaps
import Reachability
import Firebase
import MOLH
import OneSignal
import CustomerlySDK
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , MOLHResetable {
    
    var window: UIWindow?
    
    var reachability : Reachability!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        GMSServices.provideAPIKey("AIzaSyADBVSBegINIgjltqURETGY63h7HRUe48A")
        GMSPlacesClient.provideAPIKey("AIzaSyADBVSBegINIgjltqURETGY63h7HRUe48A")
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
        IQKeyboardManager.shared.toolbarTintColor = AppColor.primary
        
        do {
            self.reachability = try Reachability()
            reachability?.whenReachable = { reachability in
                if reachability.connection == .wifi {
                    print("Debug: Reachable via WiFi")
                } else {
                    print("Debug: Reachable via Cellular")
                }
                Messages.hideMessage()
            }
            reachability?.whenUnreachable = { _ in
                print("Debug: Not reachable")
                Messages.showErrorMessage(title: "Internet Connection", message: "Your internet connection seems to be not working", time: .forever)
            }
        }catch{
            print("Error: \(error.localizedDescription )")
        }
        FirebaseApp.configure()
        MOLH.shared.activate(true)
        
        // Remove this method to stop OneSignal Debugging
        OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
        OneSignal.initWithLaunchOptions(launchOptions)
        OneSignal.setAppId("0db1f147-7f55-4ca1-9e86-8e459b90eb82")
        OneSignal.promptForPushNotifications(userResponse: { accepted in
          print("User accepted notifications: \(accepted)")
        })
        Customerly.sharedInstance.configure(appId: "64d36bb1",widgetColor: AppColor.primary)
        return true
    }
    func reset() {
        let isFromSetting = UserDefaults.standard.bool(forKey: UserDefaultsKey.IsFromSetting)
              let vc = BaseNavigationViewController()
              if isFromSetting {
                  vc.viewControllers = [SplashViewController.instantiate(fromAppStoryboard: .Main)]
              } else {
                  vc.viewControllers = [LoginViewController.instantiate(fromAppStoryboard: .User)]
              }
              UIWINDOW?.rootViewController = vc
        }
      
     //  UIApplication.shared.windows.first?.rootViewController = rootVC
     // UIApplication.shared.windows.first?.makeKeyAndVisible()
    // print("resetted")}
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        reachability?.stopNotifier()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        reachability?.stopNotifier()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        do {
            try reachability?.startNotifier()
        } catch {
            print("Error: Unable to start notifier")
        }
        Customerly.sharedInstance.activateApp()
        Customerly.sharedInstance.verboseLogging = true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        reachability?.stopNotifier()
    }
    
    
}

