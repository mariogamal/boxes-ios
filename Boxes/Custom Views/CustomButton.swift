//
//  CustomButton.swift
//  Boxes
//
//  Created by macmin on 11/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Material
import NVActivityIndicatorView

class CustomButton : FlatButton {
    
    @IBInspectable var isHollow : Bool = false
    @IBInspectable var applyGradient : Bool = false
    @IBInspectable var gradient1 : UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    @IBInspectable var gradient2 : UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    
    @IBInspectable var ratio : CGFloat = 20/134
    
    lazy var loadingView = NVActivityIndicatorView(frame: CGRect(x: (self.bounds.width/2) - 20, y: self.bounds.height/2, width: 40, height: self.bounds.height))
    
    override var intrinsicContentSize: CGSize{
        let width = (UIApplication.shared.delegate as? AppDelegate)?.window?.frame.width ?? 300
        return CGSize(width: self.bounds.width, height: (width * 0.9) * ratio)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cornerRadius = 8
        self.clipsToBounds = true
        self.shadowColor = (self.shadowColor != nil) ? self.shadowColor : UIColor.black
        self.shadowRadius = 14
        self.shadowOpacity = (self.shadowOpacity != 0.0) ? self.shadowOpacity : 0.15
        self.shadowOffset = CGSize(width: 0, height: 3)
        self.titleLabel?.font = UIFont(name: AppFont.PTSansbold, size: 15) ?? UIFont.systemFont(ofSize: 16)
        
        if self.isHollow {
            self.makeHollow()
        }else{
            self.makeOpaque()
        }
        
        self.addLoadingView()
        
    }
    
    func makeHollow (){
        self.backgroundColor = UIColor.clear
        self.borderWidth = 1
        self.borderColor = AppColor.primary
        self.setTitleColor(AppColor.primary, for: .normal)
    }
    
    func makeOpaque (){
        self.backgroundColor = AppColor.primary
        self.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        self.borderWidth = 0
    }
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if self.applyGradient {
            if let width = (UIApplication.shared.delegate as? AppDelegate)?.window?.frame.width {
                self.applyGradient(colours: [AppColor.primary, AppColor.secondary], locations: [0.0, 1.0], cornerRadius: ((width * 0.9) * ratio)/2, startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1.0, y: 0.5))
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
//        guard self.isTouchInside else {
//            print("isTouchInside not inside")
//            return
//        }
        
//        if self.loadingView.isAnimating {
//            self.stopLoading()
//        } else {
//            self.showLoading()
//            DispatchQueue.main.asyncAfter(deadline: (DispatchTime.now() + 40.0), execute: {
//                self.stopLoading()
//            })
//        }
    }
    
    
    
    func addLoadingView(){
//        guard !self.subviews.contains(self.loadingView) else { return }
//        loadingView.type = .ballPulse
//        loadingView.contentMode = .center
//        self.addSubview(loadingView)
//        self.insertSubview(loadingView, at: 0)
    }
    
    func showLoading(){
//        self.setTitleColor(UIColor.clear, for: .normal)
//        loadingView.startAnimating()
//        DispatchQueue.main.asyncAfter(deadline: (DispatchTime.now() + 40.0), execute: {
//            self.stopLoading()
//        })
    }
    
    func stopLoading(){
//        self.setTitleColor(UIColor.white, for: .normal)
//        loadingView.stopAnimating()
    }
    
    func isLoading() -> Bool{
//        return self.loadingView.isAnimating
        return false
    }
    
}






        
//        if let width = (UIApplication.shared.delegate as? AppDelegate)?.window?.frame.width {
//            let new = (width * 0.9) * ratio
//            self.heightAnchor.constraint(equalToConstant: new).isActive = true
//            self.cornerRadius = ((width * 0.9) * ratio)/2
//        }
        //        self.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: 22/131).isActive = true
        
