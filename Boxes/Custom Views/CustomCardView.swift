//
//  CustomCardView.swift
//  Boxes
//
//  Created by macmin on 11/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class CustomCardView: UIView {


    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cornerRadius = 8
        self.shadowRadius = (shadowRadius != 0) ? self.shadowRadius : 16
        self.shadowColor = (shadowColor != nil) ? self.shadowColor : UIColor.black
        self.shadowOpacity = (shadowOpacity != 0) ? self.shadowOpacity : 0.1
        self.shadowOffset = CGSize(width: 0, height: 4)
        
    }
    
}
