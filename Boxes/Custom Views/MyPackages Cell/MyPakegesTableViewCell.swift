//
//  MyPakegesCell.swift
//  Boxes
//
//  Created by Bilal on 05/08/2019.
//  Copyright © 2019 Bilal. All rights reserved.
//

import UIKit

class MyPakegesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var lblTypeRest: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var plainName: UILabel!
    @IBOutlet weak var endDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(with model: MyPackagesModel) {
            
        self.plainName.text = model.packageName
        self.startDate.text = model.createdAt
        self.endDate.text = model.validity
        self.lblTypeRest.text = model.typeName
    }
    
    
    
}

