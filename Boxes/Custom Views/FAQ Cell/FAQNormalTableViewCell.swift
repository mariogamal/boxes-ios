//
//  FAQNormalTableViewCell.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/8/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class FAQNormalTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    static let ID = "FAQNormalTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
