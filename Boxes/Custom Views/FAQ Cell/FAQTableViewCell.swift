//
//  FAQTableViewCell.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/5/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import ExpandableCell

class FAQTableViewCell: ExpandableCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    static let ID = "FAQTableViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

class ExpandableCell2: ExpandableCell {
    static let ID = "ExpandableCell"
}
