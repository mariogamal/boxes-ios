//
//  FoodOrderHistoryell.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 12/26/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class FoodOrderHistoryell: UITableViewCell {

    @IBOutlet weak var fromDateLabel: UILabel!
    @IBOutlet weak var toDateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(with model: MealsHistoryModel) {
        
        self.fromDateLabel.text = model.createdAt
        self.toDateLabel.text = model.validity
        self.titleLabel.text = model.packageName
    }
    
}
