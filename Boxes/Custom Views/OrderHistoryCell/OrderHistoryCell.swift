//
//  OrderHistoryCell.swift
//  Boxes
//
//  Created by Bilal on 05/08/2019.
//  Copyright © 2019 Bilal. All rights reserved.
//

import UIKit

class OrderHistoryCell: UITableViewCell {
    @IBOutlet weak var orderDate: UILabel!
    @IBOutlet weak var orderRestaurant: UILabel!
    @IBOutlet weak var dropDownButton: UIButton!
    @IBOutlet weak var price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    public func configure(with model: MyOrdersModel) {
        
        self.orderDate.text = model.createdAt
        self.orderRestaurant.text = model.listingName
        self.price.text = model.totalPrice
    }
}
