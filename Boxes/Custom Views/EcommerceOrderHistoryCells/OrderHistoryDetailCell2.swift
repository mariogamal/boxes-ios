//
//  OrderHistoryDetailCell2.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 11/10/19.
//  Copyright © 2019 Bilal. All rights reserved.
//

import UIKit

class OrderHistoryDetailCell2: UITableViewCell {

    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var subtotal: UILabel!
    @IBOutlet weak var deliveryLabel: UILabel!
    @IBOutlet weak var delivery: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var total: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(with model: TotalModel){
        self.subtotal.text = String(model.subTotal)
        self.delivery.text = String(model.deliveryCharges)
        self.discount.text = String(model.totalDiscount)
        self.total.text = String(model.totalPrice)
    }
    

}
