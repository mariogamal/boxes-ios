//
//  OrderHistoryDetailCell3.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 11/10/19.
//  Copyright © 2019 Bilal. All rights reserved.
//

import UIKit

class OrderHistoryDetailCell3: UITableViewCell {
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
}
