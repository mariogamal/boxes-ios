//
//  OrderHistoryDetailCell1.swift
//  Boxes
//
//  Created by Muhammad Imran Shah on 11/10/19.
//  Copyright © 2019 Bilal. All rights reserved.
//

import UIKit

class OrderHistoryDetailCell1: UITableViewCell {

    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(with model: OrderDetailModel){
        self.quantity.text = String(model.quantity)
        self.name.text = String(model.productName)
        self.price.text = String(model.price)
    }
    
}
