
import UIKit

class SurveyBox: UIView {
    
    var view: UIView!
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var textField: BaseTextField!
    
    
    @IBOutlet weak var uiview: UIView!
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
        self.textField.autocorrectionType = .no
        
    }
    
    
    
    
    func xibSetup() {
        
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        
        
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "SurveyBox", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    
    func setTextFieldPlaceHolder(string: String , placeHolderColor: UIColor = THEME_COLOR_GRAY_FONT, labelTop : String = ""){
        
        self.textField.placeholder = string
        self.topLabel.text = labelTop
        let color = THEME_COLOR_GRAY_FONT
        textField.attributedPlaceholder = NSAttributedString(string: textField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : color])
        
        uiview.layer.borderWidth = 1
        self.uiview.layer.cornerRadius = 5
        uiview.layer.borderColor = THEME_COLOR_GRAY_BORDER.cgColor
        
        self.textField.textColor = THEME_COLOR_GRAY_FONT
        
        
    }
 
    
}

