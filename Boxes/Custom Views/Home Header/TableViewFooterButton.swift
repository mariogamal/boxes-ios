//
//  TableViewFooterButton.swift
//  Boxes
//
//  Created by macmin on 12/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class TableViewFooterButton: UITableViewHeaderFooterView {
    
    @IBOutlet weak var button : UIButton!
    @IBOutlet weak var countLabel: UILabel!
    
    var action : (() -> Void)?
    
    
    @IBAction func buttonAction(_ sender : UIButton ){
        self.action?()
    }
    
}
