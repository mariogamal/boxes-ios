//
//  SlidingView.swift
//  Load Runr
//
//  Created by Umer Jabbar on 10/10/2019.
//  Copyright © 2019 Umer Jabbar. All rights reserved.
//

import UIKit

enum State {
    case closed
    case open
}

extension State {
    var opposite: State {
        switch self {
        case .open: return .closed
        case .closed: return .open
        }
    }
}


class SlidingView : UIView {
    
    @IBInspectable var isPanEnabled : Bool = false
    
    var currentState: State = .closed
    var runningAnimators = [UIViewPropertyAnimator]()
    var animationProgress = [CGFloat]()
    
    lazy var panRecognizer: InstantPanGestureRecognizer = {
        let recognizer = InstantPanGestureRecognizer()
        recognizer.addTarget(self, action: #selector(popupViewPanned(recognizer:)))
        return recognizer
    }()
    
    var onOpenAction : (() -> Void)?
    var onCloseAction : (() -> Void)?
    var onViewUpdate : (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if self.isPanEnabled {
            self.addGestureRecognizer(panRecognizer)
        }
    }

    func animateTransitionIfNeeded(to state: State, duration: TimeInterval) {
        
        guard runningAnimators.isEmpty else { return }
        
        let transitionAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1, animations: {
            switch state {
            case .open:
                self.onOpenAction?()
            case .closed:
                self.onCloseAction?()
            }
            self.onViewUpdate?()
        })
        
        transitionAnimator.addCompletion { position in
            switch position {
            case .start:
                self.currentState = state.opposite
            case .end:
                self.currentState = state
            case .current:
                ()
            @unknown default:
                ()
            }
            switch self.currentState {
            case .open:
                self.onOpenAction?()
            case .closed:
                self.onCloseAction?()
            }
            self.runningAnimators.removeAll()
        }
        transitionAnimator.startAnimation()
        runningAnimators.append(transitionAnimator)
    }
    
    @objc func popupViewPanned(recognizer: UIPanGestureRecognizer) {
        
        switch recognizer.state {
        case .began:
            self.beganPanGesture(recognizer: recognizer)
            
        case .changed:
            self.changedPanGesture(recognizer: recognizer)
            
        case .ended:
            self.endedPanGesture(recognizer: recognizer)
            
        default:
            ()
        }
    }
    
    func beganPanGesture(recognizer: UIPanGestureRecognizer){
        animateTransitionIfNeeded(to: currentState.opposite, duration: 0.4)
        runningAnimators.forEach { $0.pauseAnimation() }
        animationProgress = runningAnimators.map { $0.fractionComplete }
    }
    
    func changedPanGesture(recognizer: UIPanGestureRecognizer){
        let translation = recognizer.translation(in: self)
        var fraction = -translation.y / self.bounds.height
        if currentState == .open { fraction *= -1 }
        guard runningAnimators.first != nil else{return}
        if runningAnimators[0].isReversed { fraction *= -1 }
        for (index, animator) in runningAnimators.enumerated() {
            animator.fractionComplete = fraction + animationProgress[index]
        }
    }
    
    func endedPanGesture(recognizer: UIPanGestureRecognizer){
        let yVelocity = recognizer.velocity(in: self).y
        let shouldClose = yVelocity > 0
        if yVelocity == 0 {
            runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
            return
        }
        guard runningAnimators.first != nil else{return}
        switch currentState {
        case .open:
            if !shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
            if shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
        case .closed:
            if shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
            if !shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
        }
        runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
    }
    
    func closePopup(){
        switch currentState {
        case .open:
            animateTransitionIfNeeded(to: currentState.opposite, duration: 0.4)
        case .closed:
            break
        }
    }
    
    func openPopup(){
        switch currentState {
        case .open:
            break
        case .closed:
            animateTransitionIfNeeded(to: currentState.opposite, duration: 0.4)
            break
        }
    }
    
    func togglePopup(){
        animateTransitionIfNeeded(to: currentState.opposite, duration: 0.4)
    }
    
}

class InstantPanGestureRecognizer: UIPanGestureRecognizer {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesBegan(touches, with: event)
        //        if (self.state == UIGestureRecognizer.State.began) { return }
        //        super.touchesBegan(touches, with: event)
        //        self.state = UIGestureRecognizer.State.began
    }
    
}
