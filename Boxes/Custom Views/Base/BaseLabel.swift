//
//  BaseLabel.swift
//  Boxes
//
//  Created by mac on 1/10/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import MOLH

class BaseLabel: UILabel {

 
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textAlignment = MOLHLanguage.isArabic() ? .right : .left
    }

}
