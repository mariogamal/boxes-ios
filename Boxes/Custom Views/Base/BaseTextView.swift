//
//  BaseTextView.swift
//  Boxes
//
//  Created by mac on 1/11/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import MOLH

class BaseTextView: UITextView {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.textAlignment = MOLHLanguage.isArabic() ? .right : .left
     }

}
