//
//  CenterLabel.swift
//  Boxes
//
//  Created by mac on 1/13/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class CenterLabel: UILabel {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textAlignment = .center
     }
}
