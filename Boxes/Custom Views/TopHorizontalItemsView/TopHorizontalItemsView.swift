//
//  TopHorizontalItemsView.swift
//  Boxes
//
//  Created by macmin on 09/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

protocol TopHorizontalItemsViewDelegate: class {
    func topHorizontalItemsDidSelecrIndex(index: Int)
}

class TopHorizontalItemsView: UIView {
    
    var data = [(String,String)]() {
        didSet {
            guard !data.isEmpty else {
                return
            }
            collectionView.reloadData()
            collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .left)
        }
    }
    
    var delegate: TopHorizontalItemsViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addSubview(collectionView)
    }

    lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: bounds, collectionViewLayout: flowLayout)
        collectionView.backgroundColor = .clear
        collectionView.register(UINib(nibName: TopHorizontalItemCollectionViewCell.identifier, bundle: .main), forCellWithReuseIdentifier: TopHorizontalItemCollectionViewCell.identifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.alwaysBounceHorizontal = true
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 30)
        return collectionView
    }()
    
    lazy private var flowLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        return layout
    }()
    
}

    // MARK: - UICollectionViewDelegate

extension TopHorizontalItemsView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.topHorizontalItemsDidSelecrIndex(index: indexPath.row)
        collectionView.scrollToItem(at: indexPath, at: .left, animated: true)
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
//
//    }
}

    // MARK: - UICollectionViewDataSource

extension TopHorizontalItemsView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TopHorizontalItemCollectionViewCell.identifier, for: indexPath) as! TopHorizontalItemCollectionViewCell
        cell.titleLabel.text = data[indexPath.row].1.localizedUppercase
//        cell.imgView.imageUrl = data[indexPath.row].0
        return cell
    }
}

    // MARK: - UICollectionViewCdlegateFlowLayout

extension TopHorizontalItemsView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = bounds.height * 0.8
        return CGSize(width: width, height: bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
