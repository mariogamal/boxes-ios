//
//  TopHorizontalItemCollectionViewCell.swift
//  Boxes
//
//  Created by macmin on 09/12/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class TopHorizontalItemCollectionViewCell: UICollectionViewCell {

    
    override var isSelected: Bool {
        didSet {
            self.imgView?.imageColor = self.isSelected ? AppColor.primary : AppColor.secondary
            self.titleLabel?.textColor = self.isSelected ? AppColor.primary : AppColor.secondary
        }
    }
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
