//
//	DeliveryDate.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class DeliveryDateModel : NSObject, NSCoding{

	var deliveryDate : String!
	var orderDetailId : String!
	var status : String!
	var userMeals : [UserMealModel]!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		deliveryDate = json["delivery_date"].stringValue
		orderDetailId = json["order_detail_id"].stringValue
		status = json["status"].stringValue
		userMeals = [UserMealModel]()
		let userMealsArray = json["user_meals"].arrayValue
		for userMealsJson in userMealsArray{
			let value = UserMealModel(fromJson: userMealsJson)
			userMeals.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if deliveryDate != nil{
			dictionary["delivery_date"] = deliveryDate
		}
		if orderDetailId != nil{
			dictionary["order_detail_id"] = orderDetailId
		}
		if status != nil{
			dictionary["status"] = status
		}
		if userMeals != nil{
			var dictionaryElements = [[String:Any]]()
			for userMealsElement in userMeals {
				dictionaryElements.append(userMealsElement.toDictionary())
			}
			dictionary["user_meals"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         deliveryDate = aDecoder.decodeObject(forKey: "delivery_date") as? String
         orderDetailId = aDecoder.decodeObject(forKey: "order_detail_id") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         userMeals = aDecoder.decodeObject(forKey: "user_meals") as? [UserMealModel]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if deliveryDate != nil{
			aCoder.encode(deliveryDate, forKey: "delivery_date")
		}
		if orderDetailId != nil{
			aCoder.encode(orderDetailId, forKey: "order_detail_id")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if userMeals != nil{
			aCoder.encode(userMeals, forKey: "user_meals")
		}

	}

}
