//
//	ListingModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class ListingModel : NSObject, NSCoding{

	var deliveryTime : String!
	var distance : Double!
	var id : String!
	var image : String!
	var listingCaption : String!
	var listingName : String!
	var typeId : String!
    var isSelected : Bool = false
    var weekCount: Int = 0
    var packages: [PackageModel]!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		deliveryTime = json["delivery_time"].stringValue
		distance = json["distance"].doubleValue
		id = json["id"].stringValue
		image = json["image"].stringValue
		listingCaption = json["listing_caption"].stringValue
		listingName = json["listing_name"].stringValue
		typeId = json["type_id"].stringValue
        
        packages = [PackageModel]()
        let packagesJson = json["packages"].arrayValue
        for packageJson in packagesJson {
            let value = PackageModel.init(fromJson: packageJson)
            packages.append(value)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if deliveryTime != nil{
			dictionary["delivery_time"] = deliveryTime
		}
		if distance != nil{
			dictionary["distance"] = distance
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if listingCaption != nil{
			dictionary["listing_caption"] = listingCaption
		}
		if listingName != nil{
			dictionary["listing_name"] = listingName
		}
		if typeId != nil{
			dictionary["type_id"] = typeId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         deliveryTime = aDecoder.decodeObject(forKey: "delivery_time") as? String
         distance = aDecoder.decodeObject(forKey: "distance") as? Double
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         listingCaption = aDecoder.decodeObject(forKey: "listing_caption") as? String
         listingName = aDecoder.decodeObject(forKey: "listing_name") as? String
         typeId = aDecoder.decodeObject(forKey: "type_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if deliveryTime != nil{
			aCoder.encode(deliveryTime, forKey: "delivery_time")
		}
		if distance != nil{
			aCoder.encode(distance, forKey: "distance")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if listingCaption != nil{
			aCoder.encode(listingCaption, forKey: "listing_caption")
		}
		if listingName != nil{
			aCoder.encode(listingName, forKey: "listing_name")
		}
		if typeId != nil{
			aCoder.encode(typeId, forKey: "type_id")
		}

	}

}
