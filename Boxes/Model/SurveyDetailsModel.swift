//
//	SurveyDetailsModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SurveyDetailsModel : NSObject, NSCoding{

	var additionalPreferences : String!
	var age : String?
    var gender : String?
	var allergy : AllergyModel?
	var allergyId : String?
	var bmi : String!
	var diet : DietModel?
	var dietId : String?
	var feet : String!
	var height : String!
	var inches : String!
	var weight : String!
    var allergyIngredients : [Ingredient]!
    var nonPreferredIngredients : [Ingredient]!
    var allergyIngOther : String!
    var nonPreferredIngOther : String!

    override init() {
        additionalPreferences = ""
        age  = ""
        gender  = ""
        allergy = nil
        allergyId  = ""
        bmi  = ""
        diet = nil
        dietId  = ""
        feet  = ""
        height = ""
        inches  = ""
        weight  = ""
        allergyIngOther = ""
        nonPreferredIngOther = ""
        allergyIngredients = [Ingredient]()
        nonPreferredIngredients = [Ingredient]()
    }

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		additionalPreferences = json["additional_preferences"].stringValue
		age = json["age"].stringValue
        gender = json["gender"].stringValue
		let allergyJson = json["allergy"]
		if !allergyJson.isEmpty{
			allergy = AllergyModel(fromJson: allergyJson)
		}
		allergyId = json["allergy_id"].stringValue
		bmi = json["bmi"].stringValue
		let dietJson = json["diet"]
		if !dietJson.isEmpty{
			diet = DietModel(fromJson: dietJson)
		}
		dietId = json["diet_id"].stringValue
		feet = json["feet"].stringValue
		height = json["height"].stringValue
		inches = json["inches"].stringValue
		weight = json["weight"].stringValue
        
        allergyIngOther = json["other_allergy_ingredients"].stringValue
        nonPreferredIngOther = json["other_non_preferred_ingredients"].stringValue

        allergyIngredients = [Ingredient]()
        let ingredientsArray = json["allergy_ingredients"].arrayValue
        for ingredientJson in ingredientsArray{
            let value = Ingredient(fromJson: ingredientJson)
            allergyIngredients.append(value)
        }
        
        nonPreferredIngredients = [Ingredient]()
        let nonIngredientsArray = json["non_preferred_ingredients"].arrayValue
        for nonIngredientJson in nonIngredientsArray{
            let value = Ingredient(fromJson: nonIngredientJson)
            nonPreferredIngredients.append(value)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if additionalPreferences != nil{
			dictionary["additional_preferences"] = additionalPreferences
		}
		if age != nil{
			dictionary["age"] = age
		}
		if allergy != nil{
            dictionary["allergy"] = allergy?.toDictionary()
		}
		if allergyId != nil{
			dictionary["allergy_id"] = allergyId
		}
		if bmi != nil{
			dictionary["bmi"] = bmi
		}
		if diet != nil{
            dictionary["diet"] = diet?.toDictionary()
		}
		if dietId != nil{
			dictionary["diet_id"] = dietId
		}
		if feet != nil{
			dictionary["feet"] = feet
		}
		if height != nil{
			dictionary["height"] = height
		}
		if inches != nil{
			dictionary["inches"] = inches
		}
		if weight != nil{
			dictionary["weight"] = weight
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         additionalPreferences = aDecoder.decodeObject(forKey: "additional_preferences") as? String
         age = aDecoder.decodeObject(forKey: "age") as? String
         allergy = aDecoder.decodeObject(forKey: "allergy") as? AllergyModel
         allergyId = aDecoder.decodeObject(forKey: "allergy_id") as? String
         bmi = aDecoder.decodeObject(forKey: "bmi") as? String
         diet = aDecoder.decodeObject(forKey: "diet") as? DietModel
         dietId = aDecoder.decodeObject(forKey: "diet_id") as? String
         feet = aDecoder.decodeObject(forKey: "feet") as? String
         height = aDecoder.decodeObject(forKey: "height") as? String
         inches = aDecoder.decodeObject(forKey: "inches") as? String
         weight = aDecoder.decodeObject(forKey: "weight") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if additionalPreferences != nil{
			aCoder.encode(additionalPreferences, forKey: "additional_preferences")
		}
		if age != nil{
			aCoder.encode(age, forKey: "age")
		}
		if allergy != nil{
			aCoder.encode(allergy, forKey: "allergy")
		}
		if allergyId != nil{
			aCoder.encode(allergyId, forKey: "allergy_id")
		}
		if bmi != nil{
			aCoder.encode(bmi, forKey: "bmi")
		}
		if diet != nil{
			aCoder.encode(diet, forKey: "diet")
		}
		if dietId != nil{
			aCoder.encode(dietId, forKey: "diet_id")
		}
		if feet != nil{
			aCoder.encode(feet, forKey: "feet")
		}
		if height != nil{
			aCoder.encode(height, forKey: "height")
		}
		if inches != nil{
			aCoder.encode(inches, forKey: "inches")
		}
		if weight != nil{
			aCoder.encode(weight, forKey: "weight")
		}

	}

}
