//
//	UserSubscription.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class UserSubscriptionModel : NSObject, NSCoding{

	var id : String!
	var mealSelectionCase : String!
	var packageName : String!
	var price : Int!
	var typeId : String!
	var userSubscribedListing : [UserSubscribedListingModel]!
	var validity : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		id = json["id"].stringValue
		mealSelectionCase = json["meal_selection_case"].stringValue
		packageName = json["package_name"].stringValue
		price = json["price"].intValue
		typeId = json["type_id"].stringValue
		userSubscribedListing = [UserSubscribedListingModel]()
		let userSubscribedListingArray = json["user_subscribed_listing"].arrayValue
		for userSubscribedListingJson in userSubscribedListingArray{
			let value = UserSubscribedListingModel(fromJson: userSubscribedListingJson)
			userSubscribedListing.append(value)
		}
		validity = json["validity"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if id != nil{
			dictionary["id"] = id
		}
		if mealSelectionCase != nil{
			dictionary["meal_selection_case"] = mealSelectionCase
		}
		if packageName != nil{
			dictionary["package_name"] = packageName
		}
		if price != nil{
			dictionary["price"] = price
		}
		if typeId != nil{
			dictionary["type_id"] = typeId
		}
		if userSubscribedListing != nil{
			var dictionaryElements = [[String:Any]]()
			for userSubscribedListingElement in userSubscribedListing {
				dictionaryElements.append(userSubscribedListingElement.toDictionary())
			}
			dictionary["user_subscribed_listing"] = dictionaryElements
		}
		if validity != nil{
			dictionary["validity"] = validity
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? String
         mealSelectionCase = aDecoder.decodeObject(forKey: "meal_selection_case") as? String
         packageName = aDecoder.decodeObject(forKey: "package_name") as? String
         price = aDecoder.decodeObject(forKey: "price") as? Int
         typeId = aDecoder.decodeObject(forKey: "type_id") as? String
         userSubscribedListing = aDecoder.decodeObject(forKey: "user_subscribed_listing") as? [UserSubscribedListingModel]
         validity = aDecoder.decodeObject(forKey: "validity") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if mealSelectionCase != nil{
			aCoder.encode(mealSelectionCase, forKey: "meal_selection_case")
		}
		if packageName != nil{
			aCoder.encode(packageName, forKey: "package_name")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if typeId != nil{
			aCoder.encode(typeId, forKey: "type_id")
		}
		if userSubscribedListing != nil{
			aCoder.encode(userSubscribedListing, forKey: "user_subscribed_listing")
		}
		if validity != nil{
			aCoder.encode(validity, forKey: "validity")
		}

	}

}
