//
//	UserMeal.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class UserMealModel : NSObject, NSCoding{

	var calories : Int!
	var captionName : String!
	var descriptionName : String!
	var id : String!
	var image : String!
	var menuCategory : MenuCategoryModel!
	var menuCategoryId : String!
	var productName : String!
	var proteins : Int!
	var rating : String!
	var stock : Int!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		calories = json["calories"].intValue
		captionName = json["caption_name"].stringValue
		descriptionName = json["description_name"].stringValue
		id = json["id"].stringValue
		image = json["image"].stringValue
		let menuCategoryJson = json["menu_category"]
		if !menuCategoryJson.isEmpty{
			menuCategory = MenuCategoryModel(fromJson: menuCategoryJson)
		}
		menuCategoryId = json["menu_category_id"].stringValue
		productName = json["product_name"].stringValue
		proteins = json["proteins"].intValue
		rating = json["rating"].stringValue
		stock = json["stock"].intValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if calories != nil{
			dictionary["calories"] = calories
		}
		if captionName != nil{
			dictionary["caption_name"] = captionName
		}
		if descriptionName != nil{
			dictionary["description_name"] = descriptionName
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if menuCategory != nil{
			dictionary["menu_category"] = menuCategory.toDictionary()
		}
		if menuCategoryId != nil{
			dictionary["menu_category_id"] = menuCategoryId
		}
		if productName != nil{
			dictionary["product_name"] = productName
		}
		if proteins != nil{
			dictionary["proteins"] = proteins
		}
		if rating != nil{
			dictionary["rating"] = rating
		}
		if stock != nil{
			dictionary["stock"] = stock
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         calories = aDecoder.decodeObject(forKey: "calories") as? Int
         captionName = aDecoder.decodeObject(forKey: "caption_name") as? String
         descriptionName = aDecoder.decodeObject(forKey: "description_name") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         menuCategory = aDecoder.decodeObject(forKey: "menu_category") as? MenuCategoryModel
         menuCategoryId = aDecoder.decodeObject(forKey: "menu_category_id") as? String
         productName = aDecoder.decodeObject(forKey: "product_name") as? String
         proteins = aDecoder.decodeObject(forKey: "proteins") as? Int
         rating = aDecoder.decodeObject(forKey: "rating") as? String
         stock = aDecoder.decodeObject(forKey: "stock") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if calories != nil{
			aCoder.encode(calories, forKey: "calories")
		}
		if captionName != nil{
			aCoder.encode(captionName, forKey: "caption_name")
		}
		if descriptionName != nil{
			aCoder.encode(descriptionName, forKey: "description_name")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if menuCategory != nil{
			aCoder.encode(menuCategory, forKey: "menu_category")
		}
		if menuCategoryId != nil{
			aCoder.encode(menuCategoryId, forKey: "menu_category_id")
		}
		if productName != nil{
			aCoder.encode(productName, forKey: "product_name")
		}
		if proteins != nil{
			aCoder.encode(proteins, forKey: "proteins")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}
		if stock != nil{
			aCoder.encode(stock, forKey: "stock")
		}

	}

}
