//
//	CartItem.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class CartItemModel : NSObject, NSCoding{

	var discount : Int!
	var discountPrice : Int!
	var id : String!
	var image : String!
	var listingId : String!
	var price : String!
	var productName : String!
	var quantity : Int!
	var rating : String!
	var stockCount : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		discount = json["discount"].intValue
		discountPrice = json["discount_price"].intValue
		id = json["id"].stringValue
		image = json["image"].stringValue
		listingId = json["listing_id"].stringValue
		price = json["price"].stringValue
		productName = json["product_name"].stringValue
		quantity = json["quantity"].intValue
		rating = json["rating"].stringValue
		stockCount = json["stock_count"].intValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if discount != nil{
			dictionary["discount"] = discount
		}
		if discountPrice != nil{
			dictionary["discount_price"] = discountPrice
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if listingId != nil{
			dictionary["listing_id"] = listingId
		}
		if price != nil{
			dictionary["price"] = price
		}
		if productName != nil{
			dictionary["product_name"] = productName
		}
		if quantity != nil{
			dictionary["quantity"] = quantity
		}
		if rating != nil{
			dictionary["rating"] = rating
		}
		if stockCount != nil{
			dictionary["stock_count"] = stockCount
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         discount = aDecoder.decodeObject(forKey: "discount") as? Int
         discountPrice = aDecoder.decodeObject(forKey: "discount_price") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         listingId = aDecoder.decodeObject(forKey: "listing_id") as? String
         price = aDecoder.decodeObject(forKey: "price") as? String
         productName = aDecoder.decodeObject(forKey: "product_name") as? String
         quantity = aDecoder.decodeObject(forKey: "quantity") as? Int
         rating = aDecoder.decodeObject(forKey: "rating") as? String
         stockCount = aDecoder.decodeObject(forKey: "stock_count") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if discount != nil{
			aCoder.encode(discount, forKey: "discount")
		}
		if discountPrice != nil{
			aCoder.encode(discountPrice, forKey: "discount_price")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if listingId != nil{
			aCoder.encode(listingId, forKey: "listing_id")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if productName != nil{
			aCoder.encode(productName, forKey: "product_name")
		}
		if quantity != nil{
			aCoder.encode(quantity, forKey: "quantity")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}
		if stockCount != nil{
			aCoder.encode(stockCount, forKey: "stock_count")
		}

	}

}
