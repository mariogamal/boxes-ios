//
//	CalenderDatailModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class CalenderDatailModel : NSObject, NSCoding{

	var consumedMeals : Int!
	var id : String!
	var image : String!
	var listingName : String!
	var outOfMeals : Int!
	var thresholdInDays : Int!
    var subscription : Int!
    var used : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		consumedMeals = json["consumed_meals"].intValue
		id = json["id"].stringValue
		image = json["image"].stringValue
		listingName = json["listing_name"].stringValue
		outOfMeals = json["out_of_meals"].intValue
		thresholdInDays = json["threshold_in_days"].intValue
        subscription = json["subscription"].intValue
        used = json["used"].intValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if consumedMeals != nil{
			dictionary["consumed_meals"] = consumedMeals
		}

        if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if listingName != nil{
			dictionary["listing_name"] = listingName
		}
		if outOfMeals != nil{
			dictionary["out_of_meals"] = outOfMeals
		}
		if thresholdInDays != nil{
			dictionary["threshold_in_days"] = thresholdInDays
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         consumedMeals = aDecoder.decodeObject(forKey: "consumed_meals") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         listingName = aDecoder.decodeObject(forKey: "listing_name") as? String
         outOfMeals = aDecoder.decodeObject(forKey: "out_of_meals") as? Int
         thresholdInDays = aDecoder.decodeObject(forKey: "threshold_in_days") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if consumedMeals != nil{
			aCoder.encode(consumedMeals, forKey: "consumed_meals")
		}
	
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if listingName != nil{
			aCoder.encode(listingName, forKey: "listing_name")
		}
		if outOfMeals != nil{
			aCoder.encode(outOfMeals, forKey: "out_of_meals")
		}
		if thresholdInDays != nil{
			aCoder.encode(thresholdInDays, forKey: "threshold_in_days")
		}

	}

}
