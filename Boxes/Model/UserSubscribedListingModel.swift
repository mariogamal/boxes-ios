//
//	UserSubscribedListing.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class UserSubscribedListingModel : NSObject, NSCoding{

	var consumedMeals : Int!
	var id : String!
	var listing : UserSubscribedListingListingModel!
	var listingId : String!
	var outOfMeals : Int!
	var subscriptionId : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		consumedMeals = json["consumed_meals"].intValue
		id = json["id"].stringValue
		let listingJson = json["listing"]
		if !listingJson.isEmpty{
			listing = UserSubscribedListingListingModel(fromJson: listingJson)
		}
		listingId = json["listing_id"].stringValue
		outOfMeals = json["out_of_meals"].intValue
		subscriptionId = json["subscription_id"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if consumedMeals != nil{
			dictionary["consumed_meals"] = consumedMeals
		}
		if id != nil{
			dictionary["id"] = id
		}
		if listing != nil{
			dictionary["listing"] = listing.toDictionary()
		}
		if listingId != nil{
			dictionary["listing_id"] = listingId
		}
		if outOfMeals != nil{
			dictionary["out_of_meals"] = outOfMeals
		}
		if subscriptionId != nil{
			dictionary["subscription_id"] = subscriptionId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         consumedMeals = aDecoder.decodeObject(forKey: "consumed_meals") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? String
         listing = aDecoder.decodeObject(forKey: "listing") as? UserSubscribedListingListingModel
         listingId = aDecoder.decodeObject(forKey: "listing_id") as? String
         outOfMeals = aDecoder.decodeObject(forKey: "out_of_meals") as? Int
         subscriptionId = aDecoder.decodeObject(forKey: "subscription_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if consumedMeals != nil{
			aCoder.encode(consumedMeals, forKey: "consumed_meals")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if listing != nil{
			aCoder.encode(listing, forKey: "listing")
		}
		if listingId != nil{
			aCoder.encode(listingId, forKey: "listing_id")
		}
		if outOfMeals != nil{
			aCoder.encode(outOfMeals, forKey: "out_of_meals")
		}
		if subscriptionId != nil{
			aCoder.encode(subscriptionId, forKey: "subscription_id")
		}

	}

}
