//
//	MenuProduct.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class MenuProduct : NSObject, NSCoding{

	var calories : Int!
	var captionName : String!
	var carbohydrates : Int!
	var discount : Int!
	var discountUnit : String!
	var id : String!
	var image : String!
	var listingId : String!
	var menuCategoryId : String!
	var price : Int!
	var productDescription : String!
	var productName : String!
	var proteins : Int!
	var rating : String!
    var ingredients : [Ingredient]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		calories = json["calories"].intValue
		captionName = json["caption_name"].stringValue
		carbohydrates = json["carbohydrates"].intValue
		discount = json["discount"].intValue
		discountUnit = json["discount_unit"].stringValue
		id = json["id"].stringValue
		image = json["image"].stringValue
		listingId = json["listing_id"].stringValue
		menuCategoryId = json["menu_category_id"].stringValue
		price = json["price"].intValue
		productDescription = json["product_description"].stringValue
		productName = json["product_name"].stringValue
		proteins = json["proteins"].intValue
		rating = json["rating"].stringValue
        
        ingredients = [Ingredient]()
        let ingredientsArray = json["product_ingredients"].arrayValue
        for ingredientJson in ingredientsArray{
            let value = Ingredient(fromJson: ingredientJson)
            ingredients.append(value)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if calories != nil{
			dictionary["calories"] = calories
		}
		if captionName != nil{
			dictionary["caption_name"] = captionName
		}
		if carbohydrates != nil{
			dictionary["carbohydrates"] = carbohydrates
		}
		if discount != nil{
			dictionary["discount"] = discount
		}
		if discountUnit != nil{
			dictionary["discount_unit"] = discountUnit
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if listingId != nil{
			dictionary["listing_id"] = listingId
		}
		if menuCategoryId != nil{
			dictionary["menu_category_id"] = menuCategoryId
		}
		if price != nil{
			dictionary["price"] = price
		}
		if productDescription != nil{
			dictionary["product_description"] = productDescription
		}
		if productName != nil{
			dictionary["product_name"] = productName
		}
		if proteins != nil{
			dictionary["proteins"] = proteins
		}
		if rating != nil{
			dictionary["rating"] = rating
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         calories = aDecoder.decodeObject(forKey: "calories") as? Int
         captionName = aDecoder.decodeObject(forKey: "caption_name") as? String
         carbohydrates = aDecoder.decodeObject(forKey: "carbohydrates") as? Int
         discount = aDecoder.decodeObject(forKey: "discount") as? Int
         discountUnit = aDecoder.decodeObject(forKey: "discount_unit") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         listingId = aDecoder.decodeObject(forKey: "listing_id") as? String
         menuCategoryId = aDecoder.decodeObject(forKey: "menu_category_id") as? String
         price = aDecoder.decodeObject(forKey: "price") as? Int
         productDescription = aDecoder.decodeObject(forKey: "product_description") as? String
         productName = aDecoder.decodeObject(forKey: "product_name") as? String
         proteins = aDecoder.decodeObject(forKey: "proteins") as? Int
         rating = aDecoder.decodeObject(forKey: "rating") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if calories != nil{
			aCoder.encode(calories, forKey: "calories")
		}
		if captionName != nil{
			aCoder.encode(captionName, forKey: "caption_name")
		}
		if carbohydrates != nil{
			aCoder.encode(carbohydrates, forKey: "carbohydrates")
		}
		if discount != nil{
			aCoder.encode(discount, forKey: "discount")
		}
		if discountUnit != nil{
			aCoder.encode(discountUnit, forKey: "discount_unit")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if listingId != nil{
			aCoder.encode(listingId, forKey: "listing_id")
		}
		if menuCategoryId != nil{
			aCoder.encode(menuCategoryId, forKey: "menu_category_id")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if productDescription != nil{
			aCoder.encode(productDescription, forKey: "product_description")
		}
		if productName != nil{
			aCoder.encode(productName, forKey: "product_name")
		}
		if proteins != nil{
			aCoder.encode(proteins, forKey: "proteins")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}

	}

}
