//
//	TypesModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class TypeModel : NSObject, NSCoding{

	var createdAt : String!
	var defaultField : Int!
	var id : String!
	var image : String!
	var isFood : Int!
	var subType : [SubTypeModel]!
	var typeName : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		createdAt = json["created_at"].stringValue
		defaultField = json["default"].intValue
		id = json["id"].stringValue
		image = json["image"].stringValue
		isFood = json["is_food"].intValue
		subType = [SubTypeModel]()
		let subTypeArray = json["sub_type"].arrayValue
		for subTypeJson in subTypeArray{
			let value = SubTypeModel(fromJson: subTypeJson)
			subType.append(value)
		}
		typeName = json["type_name"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if defaultField != nil{
			dictionary["default"] = defaultField
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if isFood != nil{
			dictionary["is_food"] = isFood
		}
		if subType != nil{
			var dictionaryElements = [[String:Any]]()
			for subTypeElement in subType {
				dictionaryElements.append(subTypeElement.toDictionary())
			}
			dictionary["sub_type"] = dictionaryElements
		}
		if typeName != nil{
			dictionary["type_name"] = typeName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         defaultField = aDecoder.decodeObject(forKey: "default") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         isFood = aDecoder.decodeObject(forKey: "is_food") as? Int
         subType = aDecoder.decodeObject(forKey: "sub_type") as? [SubTypeModel]
         typeName = aDecoder.decodeObject(forKey: "type_name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if defaultField != nil{
			aCoder.encode(defaultField, forKey: "default")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if isFood != nil{
			aCoder.encode(isFood, forKey: "is_food")
		}
		if subType != nil{
			aCoder.encode(subType, forKey: "sub_type")
		}
		if typeName != nil{
			aCoder.encode(typeName, forKey: "type_name")
		}

	}

}
