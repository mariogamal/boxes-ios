//
//  Ingredient.swift
//  Boxes
//
//  Created by Mario Gamal on 10/20/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import SwiftyJSON

class Ingredient : NSObject, NSCoding {

    var createdAt : String!
    var id : String!
    var image : String!
    var ingName : String!
    var isSelected = false

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        createdAt = json["created_at"].stringValue
        id = json["id"].stringValue
        image = json["image"].stringValue
        ingName = json["ing_name"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if id != nil{
            dictionary["id"] = id
        }
        if image != nil{
            dictionary["image"] = image
        }
        if ingName != nil{
            dictionary["ing_name"] = ingName
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         ingName = aDecoder.decodeObject(forKey: "ing_name") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if ingName != nil{
            aCoder.encode(ingName, forKey: "ing_name")
        }

    }

}
