//
//	MealsHistoryModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class MealsHistoryModel : NSObject, NSCoding{

	var createdAt : String!
	var id : String!
	var listings : String!
	var mealSelectionCase : String!
	var meals : Int!
	var packageName : String!
	var price : Int!
	var validity : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		createdAt = json["created_at"].stringValue
		id = json["id"].stringValue
		listings = json["listings"].stringValue
		mealSelectionCase = json["meal_selection_case"].stringValue
		meals = json["meals"].intValue
		packageName = json["package_name"].stringValue
		price = json["price"].intValue
		validity = json["validity"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if id != nil{
			dictionary["id"] = id
		}
		if listings != nil{
			dictionary["listings"] = listings
		}
		if mealSelectionCase != nil{
			dictionary["meal_selection_case"] = mealSelectionCase
		}
		if meals != nil{
			dictionary["meals"] = meals
		}
		if packageName != nil{
			dictionary["package_name"] = packageName
		}
		if price != nil{
			dictionary["price"] = price
		}
		if validity != nil{
			dictionary["validity"] = validity
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         listings = aDecoder.decodeObject(forKey: "listings") as? String
         mealSelectionCase = aDecoder.decodeObject(forKey: "meal_selection_case") as? String
         meals = aDecoder.decodeObject(forKey: "meals") as? Int
         packageName = aDecoder.decodeObject(forKey: "package_name") as? String
         price = aDecoder.decodeObject(forKey: "price") as? Int
         validity = aDecoder.decodeObject(forKey: "validity") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if listings != nil{
			aCoder.encode(listings, forKey: "listings")
		}
		if mealSelectionCase != nil{
			aCoder.encode(mealSelectionCase, forKey: "meal_selection_case")
		}
		if meals != nil{
			aCoder.encode(meals, forKey: "meals")
		}
		if packageName != nil{
			aCoder.encode(packageName, forKey: "package_name")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if validity != nil{
			aCoder.encode(validity, forKey: "validity")
		}

	}

}