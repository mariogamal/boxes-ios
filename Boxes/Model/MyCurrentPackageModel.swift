//
//	MyCurrentPackageModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class MyCurrentPackageModel : NSObject, NSCoding{

	var id : String!
	var restaurantType : String!
	var typeName : String!
	var userSubscription : [UserSubscriptionModel]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson jsonArr: JSON!){
		if jsonArr.isEmpty{
			return
		}
        let json = jsonArr.arrayValue.first!
		id = json["id"].stringValue
		restaurantType = json["restaurant_type"].stringValue
		typeName = json["type_name"].stringValue
        let userSubscriptionArray = json["user_subscription_many"].arrayValue
        userSubscription = [UserSubscriptionModel]()
        for userSubscriptionJson in userSubscriptionArray {
            let value = UserSubscriptionModel(fromJson: userSubscriptionJson)
            userSubscription.append(value)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if id != nil{
			dictionary["id"] = id
		}
		if restaurantType != nil{
			dictionary["restaurant_type"] = restaurantType
		}
		if typeName != nil{
			dictionary["type_name"] = typeName
		}
		if userSubscription != nil{
//			dictionary["user_subscription"] = userSubscription.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? String
         restaurantType = aDecoder.decodeObject(forKey: "restaurant_type") as? String
         typeName = aDecoder.decodeObject(forKey: "type_name") as? String
//         userSubscription = aDecoder.decodeObject(forKey: "user_subscription") as? UserSubscriptionModel

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if restaurantType != nil{
			aCoder.encode(restaurantType, forKey: "restaurant_type")
		}
		if typeName != nil{
			aCoder.encode(typeName, forKey: "type_name")
		}
		if userSubscription != nil{
			aCoder.encode(userSubscription, forKey: "user_subscription")
		}

	}

}
