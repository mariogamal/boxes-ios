//
//	OrderDetail.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class OrderDetailModel : NSObject, NSCoding{

	var captionName : String!
	var discount : Int!
	var id : String!
	var image : String!
	var price : Int!
	var productName : String!
	var quantity : Int!
	var totalPrice : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		captionName = json["caption_name"].stringValue
		discount = json["discount"].intValue
		id = json["id"].stringValue
		image = json["image"].stringValue
		price = json["price"].intValue
		productName = json["product_name"].stringValue
		quantity = json["quantity"].intValue
		totalPrice = json["total_price"].intValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if captionName != nil{
			dictionary["caption_name"] = captionName
		}
		if discount != nil{
			dictionary["discount"] = discount
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if price != nil{
			dictionary["price"] = price
		}
		if productName != nil{
			dictionary["product_name"] = productName
		}
		if quantity != nil{
			dictionary["quantity"] = quantity
		}
		if totalPrice != nil{
			dictionary["total_price"] = totalPrice
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         captionName = aDecoder.decodeObject(forKey: "caption_name") as? String
         discount = aDecoder.decodeObject(forKey: "discount") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         price = aDecoder.decodeObject(forKey: "price") as? Int
         productName = aDecoder.decodeObject(forKey: "product_name") as? String
         quantity = aDecoder.decodeObject(forKey: "quantity") as? Int
         totalPrice = aDecoder.decodeObject(forKey: "total_price") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if captionName != nil{
			aCoder.encode(captionName, forKey: "caption_name")
		}
		if discount != nil{
			aCoder.encode(discount, forKey: "discount")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if productName != nil{
			aCoder.encode(productName, forKey: "product_name")
		}
		if quantity != nil{
			aCoder.encode(quantity, forKey: "quantity")
		}
		if totalPrice != nil{
			aCoder.encode(totalPrice, forKey: "total_price")
		}

	}

}
