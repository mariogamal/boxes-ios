//
//	StoreProfileModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class StoreProfileModel : NSObject, NSCoding{

	var address : String!
	var createdAt : String!
	var createdBy : String!
	var deletedAt : String!
	var deviceToken : String!
	var deviceType : String!
	var email : String!
	var firstName : String!
	var id : String!
	var image : String!
	var isActive : Int!
	var languageId : String!
	var lastName : String!
	var latitude : String!
	var longitude : String!
	var otpCode : String!
	var phone : String!
	var token : String!
	var updatedAt : String!
	var updatedBy : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		address = json["address"].stringValue
		createdAt = json["created_at"].stringValue
		createdBy = json["created_by"].stringValue
		deletedAt = json["deleted_at"].stringValue
		deviceToken = json["device_token"].stringValue
		deviceType = json["device_type"].stringValue
		email = json["email"].stringValue
		firstName = json["first_name"].stringValue
		id = json["id"].stringValue
		image = json["image"].stringValue
		isActive = json["is_active"].intValue
		languageId = json["language_id"].stringValue
		lastName = json["last_name"].stringValue
		latitude = json["latitude"].stringValue
		longitude = json["longitude"].stringValue
		otpCode = json["otp_code"].stringValue
		phone = json["phone"].stringValue
		token = json["token"].stringValue
		updatedAt = json["updated_at"].stringValue
		updatedBy = json["updated_by"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if address != nil{
			dictionary["address"] = address
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if createdBy != nil{
			dictionary["created_by"] = createdBy
		}
		if deletedAt != nil{
			dictionary["deleted_at"] = deletedAt
		}
		if deviceToken != nil{
			dictionary["device_token"] = deviceToken
		}
		if deviceType != nil{
			dictionary["device_type"] = deviceType
		}
		if email != nil{
			dictionary["email"] = email
		}
		if firstName != nil{
			dictionary["first_name"] = firstName
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if isActive != nil{
			dictionary["is_active"] = isActive
		}
		if languageId != nil{
			dictionary["language_id"] = languageId
		}
		if lastName != nil{
			dictionary["last_name"] = lastName
		}
		if latitude != nil{
			dictionary["latitude"] = latitude
		}
		if longitude != nil{
			dictionary["longitude"] = longitude
		}
		if otpCode != nil{
			dictionary["otp_code"] = otpCode
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if token != nil{
			dictionary["token"] = token
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		if updatedBy != nil{
			dictionary["updated_by"] = updatedBy
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
         deletedAt = aDecoder.decodeObject(forKey: "deleted_at") as? String
         deviceToken = aDecoder.decodeObject(forKey: "device_token") as? String
         deviceType = aDecoder.decodeObject(forKey: "device_type") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         firstName = aDecoder.decodeObject(forKey: "first_name") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         isActive = aDecoder.decodeObject(forKey: "is_active") as? Int
         languageId = aDecoder.decodeObject(forKey: "language_id") as? String
         lastName = aDecoder.decodeObject(forKey: "last_name") as? String
         latitude = aDecoder.decodeObject(forKey: "latitude") as? String
         longitude = aDecoder.decodeObject(forKey: "longitude") as? String
         otpCode = aDecoder.decodeObject(forKey: "otp_code") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         token = aDecoder.decodeObject(forKey: "token") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
         updatedBy = aDecoder.decodeObject(forKey: "updated_by") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "created_by")
		}
		if deletedAt != nil{
			aCoder.encode(deletedAt, forKey: "deleted_at")
		}
		if deviceToken != nil{
			aCoder.encode(deviceToken, forKey: "device_token")
		}
		if deviceType != nil{
			aCoder.encode(deviceType, forKey: "device_type")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if firstName != nil{
			aCoder.encode(firstName, forKey: "first_name")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if isActive != nil{
			aCoder.encode(isActive, forKey: "is_active")
		}
		if languageId != nil{
			aCoder.encode(languageId, forKey: "language_id")
		}
		if lastName != nil{
			aCoder.encode(lastName, forKey: "last_name")
		}
		if latitude != nil{
			aCoder.encode(latitude, forKey: "latitude")
		}
		if longitude != nil{
			aCoder.encode(longitude, forKey: "longitude")
		}
		if otpCode != nil{
			aCoder.encode(otpCode, forKey: "otp_code")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if token != nil{
			aCoder.encode(token, forKey: "token")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}
		if updatedBy != nil{
			aCoder.encode(updatedBy, forKey: "updated_by")
		}

	}

}
