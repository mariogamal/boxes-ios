//
//	CartModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class CartModel : NSObject, NSCoding{

	var cartItems : [CartItemModel]!
	var deliveryCharges : Int!
	var id : String!
	var total : CartTotalModel!
	var userId : String!
	var voucher : VoucherModel!
	var voucherId : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		cartItems = [CartItemModel]()
		let cartItemsArray = json["cart_items"].arrayValue
		for cartItemsJson in cartItemsArray{
			let value = CartItemModel(fromJson: cartItemsJson)
			cartItems.append(value)
		}
		deliveryCharges = json["delivery_charges"].intValue
		id = json["id"].stringValue
		let totalJson = json["total"]
		if !totalJson.isEmpty{
			total = CartTotalModel(fromJson: totalJson)
		}
		userId = json["user_id"].stringValue
		let voucherJson = json["voucher"]
		if !voucherJson.isEmpty{
			voucher = VoucherModel(fromJson: voucherJson)
		}
		voucherId = json["voucher_id"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if cartItems != nil{
			var dictionaryElements = [[String:Any]]()
			for cartItemsElement in cartItems {
				dictionaryElements.append(cartItemsElement.toDictionary())
			}
			dictionary["cart_items"] = dictionaryElements
		}
		if deliveryCharges != nil{
			dictionary["delivery_charges"] = deliveryCharges
		}
		if id != nil{
			dictionary["id"] = id
		}
		if total != nil{
			dictionary["total"] = total.toDictionary()
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		if voucher != nil{
			dictionary["voucher"] = voucher.toDictionary()
		}
		if voucherId != nil{
			dictionary["voucher_id"] = voucherId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cartItems = aDecoder.decodeObject(forKey: "cart_items") as? [CartItemModel]
         deliveryCharges = aDecoder.decodeObject(forKey: "delivery_charges") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? String
         total = aDecoder.decodeObject(forKey: "total") as? CartTotalModel
         userId = aDecoder.decodeObject(forKey: "user_id") as? String
         voucher = aDecoder.decodeObject(forKey: "voucher") as? VoucherModel
         voucherId = aDecoder.decodeObject(forKey: "voucher_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if cartItems != nil{
			aCoder.encode(cartItems, forKey: "cart_items")
		}
		if deliveryCharges != nil{
			aCoder.encode(deliveryCharges, forKey: "delivery_charges")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if total != nil{
			aCoder.encode(total, forKey: "total")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}
		if voucher != nil{
			aCoder.encode(voucher, forKey: "voucher")
		}
		if voucherId != nil{
			aCoder.encode(voucherId, forKey: "voucher_id")
		}

	}

}
