//
//  Address.swift
//  Boxes
//
//  Created by Mario Gamal on 03/02/2021.
//  Copyright © 2021 Apple. All rights reserved.
//

//
//    Data.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class AddressModel : NSObject, NSCoding{

    var apartment : String!
    var area : Area!
    var areaId : String!
    var block : String!
    var building : String!
    var city : City!
    var cityId : String!
    var floor : String!
    var id : String!
    var landmarks : String!
    var latitude : String!
    var longitude : String!
    var street : String!


    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        apartment = json["apartment"].stringValue
        let areaJson = json["area"]
        if !areaJson.isEmpty{
            area = Area(fromJson: areaJson)
        }
        areaId = json["area_id"].stringValue
        block = json["block"].stringValue
        building = json["building"].stringValue
        let cityJson = json["city"]
        if !cityJson.isEmpty{
            city = City(fromJson: cityJson)
        }
        cityId = json["city_id"].stringValue
        floor = json["floor"].stringValue
        id = json["id"].stringValue
        landmarks = json["landmarks"].stringValue
        latitude = json["latitude"].stringValue
        longitude = json["longitude"].stringValue
        street = json["street"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if apartment != nil{
            dictionary["apartment"] = apartment
        }
        if area != nil{
            dictionary["area"] = area.toDictionary()
        }
        if areaId != nil{
            dictionary["area_id"] = areaId
        }
        if block != nil{
            dictionary["block"] = block
        }
        if building != nil{
            dictionary["building"] = building
        }
        if city != nil{
            dictionary["city"] = city.toDictionary()
        }
        if cityId != nil{
            dictionary["city_id"] = cityId
        }
        if floor != nil{
            dictionary["floor"] = floor
        }
        if id != nil{
            dictionary["id"] = id
        }
        if landmarks != nil{
            dictionary["landmarks"] = landmarks
        }
        if latitude != nil{
            dictionary["latitude"] = latitude
        }
        if longitude != nil{
            dictionary["longitude"] = longitude
        }
        if street != nil{
            dictionary["street"] = street
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         apartment = aDecoder.decodeObject(forKey: "apartment") as? String
         area = aDecoder.decodeObject(forKey: "area") as? Area
         areaId = aDecoder.decodeObject(forKey: "area_id") as? String
         block = aDecoder.decodeObject(forKey: "block") as? String
         building = aDecoder.decodeObject(forKey: "building") as? String
         city = aDecoder.decodeObject(forKey: "city") as? City
         cityId = aDecoder.decodeObject(forKey: "city_id") as? String
         floor = aDecoder.decodeObject(forKey: "floor") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         landmarks = aDecoder.decodeObject(forKey: "landmarks") as? String
         latitude = aDecoder.decodeObject(forKey: "latitude") as? String
         longitude = aDecoder.decodeObject(forKey: "longitude") as? String
         street = aDecoder.decodeObject(forKey: "street") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if apartment != nil{
            aCoder.encode(apartment, forKey: "apartment")
        }
        if area != nil{
            aCoder.encode(area, forKey: "area")
        }
        if areaId != nil{
            aCoder.encode(areaId, forKey: "area_id")
        }
        if block != nil{
            aCoder.encode(block, forKey: "block")
        }
        if building != nil{
            aCoder.encode(building, forKey: "building")
        }
        if city != nil{
            aCoder.encode(city, forKey: "city")
        }
        if cityId != nil{
            aCoder.encode(cityId, forKey: "city_id")
        }
        if floor != nil{
            aCoder.encode(floor, forKey: "floor")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if landmarks != nil{
            aCoder.encode(landmarks, forKey: "landmarks")
        }
        if latitude != nil{
            aCoder.encode(latitude, forKey: "latitude")
        }
        if longitude != nil{
            aCoder.encode(longitude, forKey: "longitude")
        }
        if street != nil{
            aCoder.encode(street, forKey: "street")
        }

    }

}
