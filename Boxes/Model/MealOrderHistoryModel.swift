//
//	MealOrderHistoryModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class MealOrderHistoryModel : NSObject, NSCoding{

	var deliveryDate : String!
	var mealOrderDetailId : String!
	var status : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		deliveryDate = json["delivery_date"].stringValue
		mealOrderDetailId = json["meal_order_detail_id"].stringValue
		status = json["status"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if deliveryDate != nil{
			dictionary["delivery_date"] = deliveryDate
		}
		if mealOrderDetailId != nil{
			dictionary["meal_order_detail_id"] = mealOrderDetailId
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         deliveryDate = aDecoder.decodeObject(forKey: "delivery_date") as? String
         mealOrderDetailId = aDecoder.decodeObject(forKey: "meal_order_detail_id") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if deliveryDate != nil{
			aCoder.encode(deliveryDate, forKey: "delivery_date")
		}
		if mealOrderDetailId != nil{
			aCoder.encode(mealOrderDetailId, forKey: "meal_order_detail_id")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}