//
//	MyOrdersModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class MyOrdersModel : NSObject, NSCoding{

	var createdAt : String!
	var id : String!
	var image : String!
	var listingName : String!
	var products : String!
	var totalPrice : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		createdAt = json["created_at"].stringValue
		id = json["id"].stringValue
		image = json["image"].stringValue
		listingName = json["listing_name"].stringValue
		products = json["products"].stringValue
		totalPrice = json["total_price"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if listingName != nil{
			dictionary["listing_name"] = listingName
		}
		if products != nil{
			dictionary["products"] = products
		}
		if totalPrice != nil{
			dictionary["total_price"] = totalPrice
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         listingName = aDecoder.decodeObject(forKey: "listing_name") as? String
         products = aDecoder.decodeObject(forKey: "products") as? String
         totalPrice = aDecoder.decodeObject(forKey: "total_price") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if listingName != nil{
			aCoder.encode(listingName, forKey: "listing_name")
		}
		if products != nil{
			aCoder.encode(products, forKey: "products")
		}
		if totalPrice != nil{
			aCoder.encode(totalPrice, forKey: "total_price")
		}

	}

}