//
//	Product.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class ProductModel : NSObject, NSCoding{

	var calories : Int!
	var captionName : String!
	var carbohydrates : Int!
	var discount : String!
	var id : String!
	var image : String!
	var listingCatId : String!
	var newPrice : String!
	var price : String!
	var productDescription : String!
	var productName : String!
	var proteins : Int!
	var rating : String!
	var stock : String!
	var stockCount : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		calories = json["calories"].intValue
		captionName = json["caption_name"].stringValue
		carbohydrates = json["carbohydrates"].intValue
		discount = json["discount"].stringValue
		id = json["id"].stringValue
		image = json["image"].stringValue
		listingCatId = json["listing_cat_id"].stringValue
		newPrice = json["new_price"].stringValue
		price = json["price"].stringValue
		productDescription = json["product_description"].stringValue
		productName = json["product_name"].stringValue
		proteins = json["proteins"].intValue
		rating = json["rating"].stringValue
		stock = json["stock"].stringValue
		stockCount = json["stock_count"].intValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if calories != nil{
			dictionary["calories"] = calories
		}
		if captionName != nil{
			dictionary["caption_name"] = captionName
		}
		if carbohydrates != nil{
			dictionary["carbohydrates"] = carbohydrates
		}
		if discount != nil{
			dictionary["discount"] = discount
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if listingCatId != nil{
			dictionary["listing_cat_id"] = listingCatId
		}
		if newPrice != nil{
			dictionary["new_price"] = newPrice
		}
		if price != nil{
			dictionary["price"] = price
		}
		if productDescription != nil{
			dictionary["product_description"] = productDescription
		}
		if productName != nil{
			dictionary["product_name"] = productName
		}
		if proteins != nil{
			dictionary["proteins"] = proteins
		}
		if rating != nil{
			dictionary["rating"] = rating
		}
		if stock != nil{
			dictionary["stock"] = stock
		}
		if stockCount != nil{
			dictionary["stock_count"] = stockCount
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         calories = aDecoder.decodeObject(forKey: "calories") as? Int
         captionName = aDecoder.decodeObject(forKey: "caption_name") as? String
         carbohydrates = aDecoder.decodeObject(forKey: "carbohydrates") as? Int
         discount = aDecoder.decodeObject(forKey: "discount") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         listingCatId = aDecoder.decodeObject(forKey: "listing_cat_id") as? String
         newPrice = aDecoder.decodeObject(forKey: "new_price") as? String
         price = aDecoder.decodeObject(forKey: "price") as? String
         productDescription = aDecoder.decodeObject(forKey: "product_description") as? String
         productName = aDecoder.decodeObject(forKey: "product_name") as? String
         proteins = aDecoder.decodeObject(forKey: "proteins") as? Int
         rating = aDecoder.decodeObject(forKey: "rating") as? String
         stock = aDecoder.decodeObject(forKey: "stock") as? String
         stockCount = aDecoder.decodeObject(forKey: "stock_count") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if calories != nil{
			aCoder.encode(calories, forKey: "calories")
		}
		if captionName != nil{
			aCoder.encode(captionName, forKey: "caption_name")
		}
		if carbohydrates != nil{
			aCoder.encode(carbohydrates, forKey: "carbohydrates")
		}
		if discount != nil{
			aCoder.encode(discount, forKey: "discount")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if listingCatId != nil{
			aCoder.encode(listingCatId, forKey: "listing_cat_id")
		}
		if newPrice != nil{
			aCoder.encode(newPrice, forKey: "new_price")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if productDescription != nil{
			aCoder.encode(productDescription, forKey: "product_description")
		}
		if productName != nil{
			aCoder.encode(productName, forKey: "product_name")
		}
		if proteins != nil{
			aCoder.encode(proteins, forKey: "proteins")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}
		if stock != nil{
			aCoder.encode(stock, forKey: "stock")
		}
		if stockCount != nil{
			aCoder.encode(stockCount, forKey: "stock_count")
		}

	}

}
