//
//	MyOrderDetailsModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class MyOrderDetailsModel : NSObject, NSCoding{

	var orderDetails : [OrderDetailModel]!
	var total : TotalModel!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		orderDetails = [OrderDetailModel]()
		let orderDetailsArray = json["order_details"].arrayValue
		for orderDetailsJson in orderDetailsArray{
			let value = OrderDetailModel(fromJson: orderDetailsJson)
			orderDetails.append(value)
		}
		let totalJson = json["total"]
		if !totalJson.isEmpty{
			total = TotalModel(fromJson: totalJson)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if orderDetails != nil{
			var dictionaryElements = [[String:Any]]()
			for orderDetailsElement in orderDetails {
				dictionaryElements.append(orderDetailsElement.toDictionary())
			}
			dictionary["order_details"] = dictionaryElements
		}
		if total != nil{
			dictionary["total"] = total.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         orderDetails = aDecoder.decodeObject(forKey: "order_details") as? [OrderDetailModel]
         total = aDecoder.decodeObject(forKey: "total") as? TotalModel

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if orderDetails != nil{
			aCoder.encode(orderDetails, forKey: "order_details")
		}
		if total != nil{
			aCoder.encode(total, forKey: "total")
		}

	}

}
