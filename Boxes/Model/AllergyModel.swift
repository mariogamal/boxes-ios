//
//	Allergy.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class AllergyModel : NSObject, NSCoding{

	var allergyName : String!
	var id : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		allergyName = json["allergy_name"].stringValue
		id = json["id"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if allergyName != nil{
			dictionary["allergy_name"] = allergyName
		}
		if id != nil{
			dictionary["id"] = id
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         allergyName = aDecoder.decodeObject(forKey: "allergy_name") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if allergyName != nil{
			aCoder.encode(allergyName, forKey: "allergy_name")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}

	}

}
