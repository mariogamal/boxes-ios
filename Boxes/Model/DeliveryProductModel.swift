//
//	DeliveryProduct.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class DeliveryProductModel : NSObject, NSCoding{

	var calories : Int!
	var caption : String!
	var carbohydrates : Int!
	var id : String!
	var image : String!
	var name : String!
	var proteins : Int!
	var rating : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		calories = json["calories"].intValue
		caption = json["caption"].stringValue
		carbohydrates = json["carbohydrates"].intValue
		id = json["id"].stringValue
		image = json["image"].stringValue
		name = json["name"].stringValue
		proteins = json["proteins"].intValue
		rating = json["rating"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if calories != nil{
			dictionary["calories"] = calories
		}
		if caption != nil{
			dictionary["caption"] = caption
		}
		if carbohydrates != nil{
			dictionary["carbohydrates"] = carbohydrates
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if name != nil{
			dictionary["name"] = name
		}
		if proteins != nil{
			dictionary["proteins"] = proteins
		}
		if rating != nil{
			dictionary["rating"] = rating
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         calories = aDecoder.decodeObject(forKey: "calories") as? Int
         caption = aDecoder.decodeObject(forKey: "caption") as? String
         carbohydrates = aDecoder.decodeObject(forKey: "carbohydrates") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         proteins = aDecoder.decodeObject(forKey: "proteins") as? Int
         rating = aDecoder.decodeObject(forKey: "rating") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if calories != nil{
			aCoder.encode(calories, forKey: "calories")
		}
		if caption != nil{
			aCoder.encode(caption, forKey: "caption")
		}
		if carbohydrates != nil{
			aCoder.encode(carbohydrates, forKey: "carbohydrates")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if proteins != nil{
			aCoder.encode(proteins, forKey: "proteins")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}

	}

}
