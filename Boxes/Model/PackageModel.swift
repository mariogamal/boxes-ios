//
//	PackageModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class PackageModel : NSObject, NSCoding{

	var id : String!
	var meals : Int!
	var packageName : String!
	var price : String!
    var priceValue : Double!
	var type : TypeModel!
	var typeId : String!
    var details : String!
    var calories : String!
    var settings : [Setting]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		id = json["id"].stringValue
		meals = json["meals"].intValue
		packageName = json["package_name"].stringValue
		price = json["price"].stringValue
        priceValue = json["price2"].doubleValue
		let typeJson = json["type"]
		if !typeJson.isEmpty{
			type = TypeModel(fromJson: typeJson)
		}
		typeId = json["type_id"].stringValue
        details = json["description"].stringValue
        calories = json["calories"].stringValue
        settings = [Setting]()
        let settingsArray = json["settings"].arrayValue
        for settingsJson in settingsArray{
            let value = Setting(fromJson: settingsJson)
            settings.append(value)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if id != nil{
			dictionary["id"] = id
		}
		if meals != nil{
			dictionary["meals"] = meals
		}
		if packageName != nil{
			dictionary["package_name"] = packageName
		}
		if price != nil{
			dictionary["price"] = price
		}
		if type != nil{
			dictionary["type"] = type.toDictionary()
		}
		if typeId != nil{
			dictionary["type_id"] = typeId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? String
         meals = aDecoder.decodeObject(forKey: "meals") as? Int
         packageName = aDecoder.decodeObject(forKey: "package_name") as? String
         price = aDecoder.decodeObject(forKey: "price") as? String
         type = aDecoder.decodeObject(forKey: "type") as? TypeModel
         typeId = aDecoder.decodeObject(forKey: "type_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if meals != nil{
			aCoder.encode(meals, forKey: "meals")
		}
		if packageName != nil{
			aCoder.encode(packageName, forKey: "package_name")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
		if typeId != nil{
			aCoder.encode(typeId, forKey: "type_id")
		}

	}

}
