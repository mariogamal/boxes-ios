//
//	SubType.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SubTypeModel : NSObject, NSCoding{

	var id : String!
	var parentId : String!
	var restaurantType : String!
	var subTypeName : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		id = json["id"].stringValue
		parentId = json["parent_id"].stringValue
		restaurantType = json["restaurant_type"].stringValue
		subTypeName = json["sub_type_name"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if id != nil{
			dictionary["id"] = id
		}
		if parentId != nil{
			dictionary["parent_id"] = parentId
		}
		if restaurantType != nil{
			dictionary["restaurant_type"] = restaurantType
		}
		if subTypeName != nil{
			dictionary["sub_type_name"] = subTypeName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? String
         parentId = aDecoder.decodeObject(forKey: "parent_id") as? String
         restaurantType = aDecoder.decodeObject(forKey: "restaurant_type") as? String
         subTypeName = aDecoder.decodeObject(forKey: "sub_type_name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if parentId != nil{
			aCoder.encode(parentId, forKey: "parent_id")
		}
		if restaurantType != nil{
			aCoder.encode(restaurantType, forKey: "restaurant_type")
		}
		if subTypeName != nil{
			aCoder.encode(subTypeName, forKey: "sub_type_name")
		}

	}

}
