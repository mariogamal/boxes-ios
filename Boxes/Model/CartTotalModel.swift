//
//	Total.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class CartTotalModel : NSObject, NSCoding{

	var cartCount : Int!
	var deliveryCharges : String!
	var subTotal : String!
	var tax : String!
	var total : String!
	var totalDiscount : String!
	var voucherDiscount : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		cartCount = json["cart_count"].intValue
		deliveryCharges = json["delivery_charges"].stringValue
		subTotal = json["sub_total"].stringValue
		tax = json["tax"].stringValue
		total = json["total"].stringValue
		totalDiscount = json["total_discount"].stringValue
		voucherDiscount = json["voucher_discount"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if cartCount != nil{
			dictionary["cart_count"] = cartCount
		}
		if deliveryCharges != nil{
			dictionary["delivery_charges"] = deliveryCharges
		}
		if subTotal != nil{
			dictionary["sub_total"] = subTotal
		}
		if tax != nil{
			dictionary["tax"] = tax
		}
		if total != nil{
			dictionary["total"] = total
		}
		if totalDiscount != nil{
			dictionary["total_discount"] = totalDiscount
		}
		if voucherDiscount != nil{
			dictionary["voucher_discount"] = voucherDiscount
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cartCount = aDecoder.decodeObject(forKey: "cart_count") as? Int
         deliveryCharges = aDecoder.decodeObject(forKey: "delivery_charges") as? String
         subTotal = aDecoder.decodeObject(forKey: "sub_total") as? String
         tax = aDecoder.decodeObject(forKey: "tax") as? String
         total = aDecoder.decodeObject(forKey: "total") as? String
         totalDiscount = aDecoder.decodeObject(forKey: "total_discount") as? String
         voucherDiscount = aDecoder.decodeObject(forKey: "voucher_discount") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if cartCount != nil{
			aCoder.encode(cartCount, forKey: "cart_count")
		}
		if deliveryCharges != nil{
			aCoder.encode(deliveryCharges, forKey: "delivery_charges")
		}
		if subTotal != nil{
			aCoder.encode(subTotal, forKey: "sub_total")
		}
		if tax != nil{
			aCoder.encode(tax, forKey: "tax")
		}
		if total != nil{
			aCoder.encode(total, forKey: "total")
		}
		if totalDiscount != nil{
			aCoder.encode(totalDiscount, forKey: "total_discount")
		}
		if voucherDiscount != nil{
			aCoder.encode(voucherDiscount, forKey: "voucher_discount")
		}

	}

}
