//
//    Setting.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON


class Setting : NSObject, NSCoding{

    var categoryName : String!
    var consumption : String!
    var id : String!


    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        categoryName = json["category_name"].stringValue
        consumption = json["consumption"].stringValue
        id = json["id"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if categoryName != nil{
            dictionary["category_name"] = categoryName
        }
        if consumption != nil{
            dictionary["consumption"] = consumption
        }
        if id != nil{
            dictionary["id"] = id
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         categoryName = aDecoder.decodeObject(forKey: "category_name") as? String
         consumption = aDecoder.decodeObject(forKey: "consumption") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if categoryName != nil{
            aCoder.encode(categoryName, forKey: "category_name")
        }
        if consumption != nil{
            aCoder.encode(consumption, forKey: "consumption")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }

    }

}
