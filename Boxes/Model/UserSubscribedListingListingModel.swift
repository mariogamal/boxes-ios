//
//	Listing.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class UserSubscribedListingListingModel : NSObject, NSCoding{

	var captionName : String!
	var id : String!
	var image : String!
	var listingName : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		captionName = json["caption_name"].stringValue
		id = json["id"].stringValue
		image = json["image"].stringValue
		listingName = json["listing_name"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if captionName != nil{
			dictionary["caption_name"] = captionName
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if listingName != nil{
			dictionary["listing_name"] = listingName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         captionName = aDecoder.decodeObject(forKey: "caption_name") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         listingName = aDecoder.decodeObject(forKey: "listing_name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if captionName != nil{
			aCoder.encode(captionName, forKey: "caption_name")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if listingName != nil{
			aCoder.encode(listingName, forKey: "listing_name")
		}

	}

}
