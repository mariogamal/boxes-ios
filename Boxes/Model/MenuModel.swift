//
//	MenuModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class MenuModel : NSObject, NSCoding{
    
    var id : String!
    var menuCatName : String!
    var mealConsumption: Int!
    var products : [MenuProduct]!

    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].stringValue
        menuCatName = json["menu_cat_name"].stringValue
        mealConsumption = json["meal_consumption"].intValue
        products = [MenuProduct]()
        let productsArray = json["products"].arrayValue
        for productsJson in productsArray{
            let value = MenuProduct(fromJson: productsJson)
            products.append(value)
        }

    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if menuCatName != nil{
            dictionary["menu_cat_name"] = menuCatName
        }
        if mealConsumption != nil{
            dictionary["meal_consumption"] = mealConsumption
        }
        if products != nil{
            var dictionaryElements = [[String:Any]]()
            for productsElement in products {
                dictionaryElements.append(productsElement.toDictionary())
            }
            dictionary["products"] = dictionaryElements
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? String
        menuCatName = aDecoder.decodeObject(forKey: "menu_cat_name") as? String
        mealConsumption = aDecoder.decodeObject(forKey: "meal_consumption") as? Int
        products = aDecoder.decodeObject(forKey: "products") as? [MenuProduct]
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if menuCatName != nil{
            aCoder.encode(menuCatName, forKey: "menu_cat_name")
        }
        if mealConsumption != nil{
            aCoder.encode(mealConsumption, forKey: "meal_consumption")
        }
        if products != nil{
            aCoder.encode(products, forKey: "products")
        }
        
    }
    
}
