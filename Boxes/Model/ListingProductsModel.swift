//
//	ListingProductsModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class ListingProductsModel : NSObject, NSCoding{

	var id : String!
	var listingCatName : String!
	var listingId : String!
	var products : [ProductModel]!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		id = json["id"].stringValue
		listingCatName = json["listing_cat_name"].stringValue
		listingId = json["listing_id"].stringValue
		products = [ProductModel]()
		let productsArray = json["products"].arrayValue
		for productsJson in productsArray{
			let value = ProductModel(fromJson: productsJson)
			products.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if id != nil{
			dictionary["id"] = id
		}
		if listingCatName != nil{
			dictionary["listing_cat_name"] = listingCatName
		}
		if listingId != nil{
			dictionary["listing_id"] = listingId
		}
		if products != nil{
			var dictionaryElements = [[String:Any]]()
			for productsElement in products {
				dictionaryElements.append(productsElement.toDictionary())
			}
			dictionary["products"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? String
         listingCatName = aDecoder.decodeObject(forKey: "listing_cat_name") as? String
         listingId = aDecoder.decodeObject(forKey: "listing_id") as? String
         products = aDecoder.decodeObject(forKey: "products") as? [ProductModel]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if listingCatName != nil{
			aCoder.encode(listingCatName, forKey: "listing_cat_name")
		}
		if listingId != nil{
			aCoder.encode(listingId, forKey: "listing_id")
		}
		if products != nil{
			aCoder.encode(products, forKey: "products")
		}

	}

}
