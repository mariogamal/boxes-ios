//
//	VerifyOTPModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class VerifyOTPModel : NSObject, NSCoding{

	var accessToken : String!
	var tokenType : String!
	var user : UserModel!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		accessToken = json["access_token"].stringValue
		tokenType = json["token_type"].stringValue
		let userJson = json["user"]
		if !userJson.isEmpty{
			user = UserModel(fromJson: userJson)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if accessToken != nil{
			dictionary["access_token"] = accessToken
		}
		if tokenType != nil{
			dictionary["token_type"] = tokenType
		}
		if user != nil{
			dictionary["user"] = user.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         accessToken = aDecoder.decodeObject(forKey: "access_token") as? String
         tokenType = aDecoder.decodeObject(forKey: "token_type") as? String
         user = aDecoder.decodeObject(forKey: "user") as? UserModel

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if accessToken != nil{
			aCoder.encode(accessToken, forKey: "access_token")
		}
		if tokenType != nil{
			aCoder.encode(tokenType, forKey: "token_type")
		}
		if user != nil{
			aCoder.encode(user, forKey: "user")
		}

	}

}
