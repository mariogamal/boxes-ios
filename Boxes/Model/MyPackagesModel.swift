//
//	MyPackagesModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class MyPackagesModel : NSObject, NSCoding{

	var createdAt : String!
	var expiredAt : String!
	var packageName : String!
	var typeName : String!
	var validity : String!

    /**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		createdAt = json["created_at"].stringValue
		expiredAt = json["expired_at"].stringValue
		packageName = json["package_name"].stringValue
		typeName = json["type_name"].stringValue
		validity = json["validity"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if expiredAt != nil{
			dictionary["expired_at"] = expiredAt
		}
		if packageName != nil{
			dictionary["package_name"] = packageName
		}
		if typeName != nil{
			dictionary["type_name"] = typeName
		}
		if validity != nil{
			dictionary["validity"] = validity
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         expiredAt = aDecoder.decodeObject(forKey: "expired_at") as? String
         packageName = aDecoder.decodeObject(forKey: "package_name") as? String
         typeName = aDecoder.decodeObject(forKey: "type_name") as? String
         validity = aDecoder.decodeObject(forKey: "validity") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if expiredAt != nil{
			aCoder.encode(expiredAt, forKey: "expired_at")
		}
		if packageName != nil{
			aCoder.encode(packageName, forKey: "package_name")
		}
		if typeName != nil{
			aCoder.encode(typeName, forKey: "type_name")
		}
		if validity != nil{
			aCoder.encode(validity, forKey: "validity")
		}

	}

}
