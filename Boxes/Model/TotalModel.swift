//
//	Total.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class TotalModel : NSObject, NSCoding{

	var deliveryCharges : Int!
	var subTotal : Int!
	var totalDiscount : Int!
	var totalPrice : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		deliveryCharges = json["delivery_charges"].intValue
		subTotal = json["sub_total"].intValue
		totalDiscount = json["total_discount"].intValue
		totalPrice = json["total_price"].intValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if deliveryCharges != nil{
			dictionary["delivery_charges"] = deliveryCharges
		}
		if subTotal != nil{
			dictionary["sub_total"] = subTotal
		}
		if totalDiscount != nil{
			dictionary["total_discount"] = totalDiscount
		}
		if totalPrice != nil{
			dictionary["total_price"] = totalPrice
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         deliveryCharges = aDecoder.decodeObject(forKey: "delivery_charges") as? Int
         subTotal = aDecoder.decodeObject(forKey: "sub_total") as? Int
         totalDiscount = aDecoder.decodeObject(forKey: "total_discount") as? Int
         totalPrice = aDecoder.decodeObject(forKey: "total_price") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if deliveryCharges != nil{
			aCoder.encode(deliveryCharges, forKey: "delivery_charges")
		}
		if subTotal != nil{
			aCoder.encode(subTotal, forKey: "sub_total")
		}
		if totalDiscount != nil{
			aCoder.encode(totalDiscount, forKey: "total_discount")
		}
		if totalPrice != nil{
			aCoder.encode(totalPrice, forKey: "total_price")
		}

	}

}
