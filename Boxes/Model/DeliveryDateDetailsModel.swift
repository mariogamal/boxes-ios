//
//	DeliveryDateDetailsModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class DeliveryDateDetailsModel : NSObject, NSCoding{

	var id : String!
	var listingName : String!
	var menu : [DeliveryMenuModel]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		id = json["id"].stringValue
		listingName = json["listing_name"].stringValue
		menu = [DeliveryMenuModel]()
		let menuArray = json["menu"].arrayValue
		for menuJson in menuArray{
			let value = DeliveryMenuModel(fromJson: menuJson)
			menu.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if id != nil{
			dictionary["id"] = id
		}
		if listingName != nil{
			dictionary["listing_name"] = listingName
		}
		if menu != nil{
			var dictionaryElements = [[String:Any]]()
			for menuElement in menu {
				dictionaryElements.append(menuElement.toDictionary())
			}
			dictionary["menu"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? String
         listingName = aDecoder.decodeObject(forKey: "listing_name") as? String
         menu = aDecoder.decodeObject(forKey: "menu") as? [DeliveryMenuModel]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if listingName != nil{
			aCoder.encode(listingName, forKey: "listing_name")
		}
		if menu != nil{
			aCoder.encode(menu, forKey: "menu")
		}

	}

}
