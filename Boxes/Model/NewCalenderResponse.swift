//
//  NewCalenderResponse.swift
//  Boxes
//
//  Created by Mario Gamal on 11/13/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

import Foundation
import SwiftyJSON

class NewCalenderResponse : NSObject, NSCoding{
    
    var vendors: [CalenderDatailModel]!
    var deliveryDates: [DeliveryDateModel]!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        deliveryDates = [DeliveryDateModel]()
        let deliveryDatesArray = json["delivery_dates"].arrayValue
        for deliveryDatesJson in deliveryDatesArray{
            let value = DeliveryDateModel(fromJson: deliveryDatesJson)
            deliveryDates.append(value)
        }
        vendors = [CalenderDatailModel]()
        let vendorsArray = json["vendors"].arrayValue
        for vendorsJson in vendorsArray{
            let value = CalenderDatailModel(fromJson: vendorsJson)
            vendors.append(value)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if deliveryDates != nil{
            var dictionaryElements = [[String:Any]]()
            for deliveryDatesElement in deliveryDates {
                dictionaryElements.append(deliveryDatesElement.toDictionary())
            }
            dictionary["delivery_dates"] = dictionaryElements
        }
        if vendors != nil{
            var dictionaryElements = [[String:Any]]()
            for vendorsElement in vendors {
                dictionaryElements.append(vendorsElement.toDictionary())
            }
            dictionary["vendors"] = dictionaryElements
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        deliveryDates = aDecoder.decodeObject(forKey: "delivery_dates") as? [DeliveryDateModel]
        vendors = aDecoder.decodeObject(forKey: "vendors") as? [CalenderDatailModel]
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if deliveryDates != nil{
            aCoder.encode(deliveryDates, forKey: "delivery_dates")
        }
        if vendors != nil{
            aCoder.encode(vendors, forKey: "vendors")
        }
        
    }
    
}
