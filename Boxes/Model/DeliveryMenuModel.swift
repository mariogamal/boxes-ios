//
//	DeliveryMenu.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class DeliveryMenuModel : NSObject, NSCoding{

	var categoryName : String!
	var product : DeliveryProductModel!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		categoryName = json["category_name"].stringValue
		let productJson = json["product"]
		if !productJson.isEmpty{
			product = DeliveryProductModel(fromJson: productJson)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if categoryName != nil{
			dictionary["category_name"] = categoryName
		}
		if product != nil{
			dictionary["product"] = product.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         categoryName = aDecoder.decodeObject(forKey: "category_name") as? String
         product = aDecoder.decodeObject(forKey: "product") as? DeliveryProductModel

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if categoryName != nil{
			aCoder.encode(categoryName, forKey: "category_name")
		}
		if product != nil{
			aCoder.encode(product, forKey: "product")
		}

	}

}
