//
//	FAQModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class FAQModel : NSObject, NSCoding{

	var answerDesc : String!
	var questionDesc : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		answerDesc = json["answer_desc"].stringValue
		questionDesc = json["question_desc"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if answerDesc != nil{
			dictionary["answer_desc"] = answerDesc
		}
		if questionDesc != nil{
			dictionary["question_desc"] = questionDesc
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         answerDesc = aDecoder.decodeObject(forKey: "answer_desc") as? String
         questionDesc = aDecoder.decodeObject(forKey: "question_desc") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if answerDesc != nil{
			aCoder.encode(answerDesc, forKey: "answer_desc")
		}
		if questionDesc != nil{
			aCoder.encode(questionDesc, forKey: "question_desc")
		}

	}

}