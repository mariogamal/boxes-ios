//
//  File.swift
//  CBD Shops
//
//  Created by MacAir on 28/06/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class Messages {
    
    private var myname = "Umer"
    fileprivate var mynicknName = "Umer"
    
    class func showSuccessMessage(message:String?){
        
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        //        config.duration = .seconds(seconds: 1.0)
        let messageView = MessageView.viewFromNib(layout: .cardView)
        messageView.configureTheme(.success)
        
        messageView.configureContent(title: "", body: message ?? "" )
        messageView.button?.isHidden = true
        SwiftMessages.show(config: config, view: messageView)
    }
    
    class func showErrorMessage(message:String?){
        
        DispatchQueue.main.async {
            
            var config = SwiftMessages.Config()
            config.presentationStyle = .top
            //        config.duration = .seconds(seconds: 1.0)
            let messageView = MessageView.viewFromNib(layout: .cardView)
            messageView.configureTheme(.error)
            
            messageView.configureContent(title: "", body: message ?? "" )
            messageView.button?.isHidden = true
            SwiftMessages.show(config: config, view: messageView)
        }
        
    }
    
    class func showErrorMessage(title: String? = "", message:String? = ""){
        
        DispatchQueue.main.async {
            
            var config = SwiftMessages.Config()
            config.presentationStyle = .top
            //        config.duration = .seconds(seconds: 1.0)
            let messageView = MessageView.viewFromNib(layout: .cardView)
            messageView.configureTheme(.error)
            
            messageView.configureContent(title: title ?? "", body: message ?? "" )
            messageView.button?.isHidden = true
            SwiftMessages.show(config: config, view: messageView)
        }
        
    }
    
    class func showErrorMessage(title: String? = "", message:String? = "", time: SwiftMessages.Duration = .automatic ){
        
        DispatchQueue.main.async {
            
            var config = SwiftMessages.Config()
            config.presentationStyle = .top
            config.duration = time
            //            if time == .forever {
            //                config.dimMode = .gray(interactive: true)
            //            }
            let messageView = MessageView.viewFromNib(layout: .cardView)
            messageView.configureTheme(.error)
            
            messageView.configureContent(title: title ?? "", body: message ?? "" )
            messageView.button?.isHidden = true
            SwiftMessages.show(config: config, view: messageView)
        }
        
    }
    
    class func showParmenantSuccessMessage(message:String){
        
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.interactiveHide = false
        config.duration = .forever
        let messageView = MessageView.viewFromNib(layout: .cardView)
        let iconStyle:IconStyle = .default
        let iconImage = iconStyle.image(theme: .success)
        messageView.configureTheme(backgroundColor: UIColor.red, foregroundColor: .white, iconImage: iconImage)
        messageView.configureContent(title: "", body: message)
        messageView.button?.isHidden = true
        SwiftMessages.show(config: config, view: messageView)
        
    }
    
    class func showSuccessWithMessage(_ message:String, layout:MessageView.Layout) {
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.preferredStatusBarStyle = .lightContent
        let messageView = MessageView.viewFromNib(layout: layout)
        messageView.configureTheme(.success)
        messageView.configureContent(title: "", body: message)
        messageView.button?.isHidden = true
        SwiftMessages.show(config: config, view: messageView)
    }
    
    class func showErrorWith(message: String?){
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        let error = MessageView.viewFromNib(layout: .centeredView)
        error.configureContent(title: "Error".localized, body: message ?? "Something went wrong".localized)
        let iconStyle:IconStyle = .default
        let iconImage = iconStyle.image(theme: .error)
        error.configureTheme(backgroundColor: UIColor(named: "BC4C46") ?? #colorLiteral(red: 0.8959212303, green: 0.2223513722, blue: 0.2608771324, alpha: 1), foregroundColor: .white, iconImage: iconImage)
        error.button?.isHidden = true
        SwiftMessages.show(config: config, view: error)
    }
    
    class func showErrorBar(message: String?){
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        let error = MessageView.viewFromNib(layout: .statusLine)
        error.configureTheme(.error)
        error.configureContent(title: "", body: message ?? "Something went wrong".localized)
        error.button?.isHidden = true
        SwiftMessages.show(config: config, view: error)
    }
    
    class func hideMessage(){
        SwiftMessages.hide()
    }
    
}

class Audio {
    
    func sd(){
        
    }
    
    
}
