
import UIKit

class BoxesTextBox: UIView {
    
    var view: UIView!
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet var uiview: UIView!
    
//    var isFilled: Bool {
//        get {
//
//            if textField.text! == "" &&  ! Utilities.shared.validateEmail(email: textField.text!) {
//                self.shakeView()
//                return false
//            } else {
//                return true
//            }
//        }
//    }
    
    func shakeView() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: view.center.x - 10, y: view.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: view.center.x + 10, y: view.center.y))
        view.layer.add(animation, forKey: "position")
    }
    
   
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
        self.textField.autocorrectionType = .no

    }
    
    func xibSetup() {
        
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
      
        

        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "BoxesTextBox", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
  
    
    func setTextFieldPlaceHolder(string: String, placeHolderColor: UIColor = .white){
        self.textField.placeholder = string
        let color = UIColor.white
        textField.textColor = color
        textField.attributedPlaceholder = NSAttributedString(string: textField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : color])
        uiview.layer.borderWidth = 1
        self.uiview.layer.cornerRadius = 5
        uiview.layer.borderColor = UIColor.gray.cgColor
      
    }
    func setTextFieldPlaceHolderGrey(string: String, placeHolderColor: UIColor = .white){
        self.textField.placeholder = string
        let color = UIColor.gray
        textField.textColor = color
        textField.attributedPlaceholder = NSAttributedString(string: textField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : color])
        uiview.layer.borderWidth = 1
        uiview.layer.borderColor = UIColor.gray.cgColor
        
        
    }
    
}

