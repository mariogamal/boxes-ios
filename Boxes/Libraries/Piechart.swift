import UIKit

//extension CGFloat {
//    
//    func radians() -> CGFloat {
//        let b = CGFloat(Double.pi) * (self/180)
//        return b
//    }
//    
//}
//
//extension UIBezierPath {
//    
//    convenience init(circleSegmentCenter center:CGPoint, radius:CGFloat, startAngle:CGFloat, endAngle:CGFloat){
//        self.init()
//        self.move(to: center)
//        self.addArc(withCenter: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
//        self.close()
//        
//    }
//
//}

//extension UIView {
//
//    func pieChart(colors: [UIColor]) {
//
//        let radius : CGFloat = 100
//        let angle = 360/colors.count
//
//        for i in 0 ..< colors.count {
//            let bezier = UIBezierPath(circleSegmentCenter: CGPoint(x: self.bounds.midX, y: self.bounds.midY), radius: radius, startAngle: CGFloat(i * angle), endAngle: CGFloat((i + 2) * angle))
//            let layer = CAShapeLayer()
//            layer.path = bezier.cgPath
//            layer.fillColor = colors[i].cgColor
//            layer.strokeColor = UIColor.black.cgColor
//            self.layer.addSublayer(layer)
//        }
//    }
//
//    func segmented(){
//
//        let circlePath = UIBezierPath(ovalIn: CGRect(x: 200, y: 200, width: 150, height: 150))
//        var segments: [CAShapeLayer] = []
//        let segmentAngle: CGFloat = (360 * 0.125) / 360
//
//        for i in 0 ..< 8 {
//            let circleLayer = CAShapeLayer()
//            circleLayer.path = circlePath.cgPath
//
//            // start angle is number of segments * the segment angle
//            circleLayer.strokeStart = segmentAngle * CGFloat(i)
//
//            // end angle is the start plus one segment, minus a little to make a gap
//            // you'll have to play with this value to get it to look right at the size you need
//            let gapSize: CGFloat = 0.008
//            circleLayer.strokeEnd = circleLayer.strokeStart + segmentAngle - gapSize
//
//            circleLayer.lineWidth = 10
//            circleLayer.strokeColor = UIColor(red:0,  green:0.004,  blue:0.549, alpha:1).cgColor
//            circleLayer.fillColor = UIColor.clear.cgColor
//
//            // add the segment to the segments array and to the view
//            segments.insert(circleLayer, at: i)
//            self.layer.addSublayer(segments[i])
//        }
//
//    }
//
//}

